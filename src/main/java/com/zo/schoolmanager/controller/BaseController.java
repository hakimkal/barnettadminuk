package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 14/12/2015.
 */
public class BaseController {

    @Autowired
    private UserRepository userRepository;

    public ApiResponse success() {
      return new ApiResponse("success", "", "");
    }

    public ApiResponse failure() {
        return new ApiResponse("failure", "", "");
    }

    public User loggedInUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User appUser = userRepository.findOneByUsername(user.getUsername());
        appUser.setPassword("");

        return appUser;
    }
}
