package com.zo.schoolmanager.classes;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.Clazz;
import com.zo.schoolmanager.repository.ClassRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 22/01/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ClassTests {

    @Autowired
    private ClassRepository classRepository;

    @Before
    public void cleanUpClassesDB() {
        classRepository.deleteAll();
    }

    @Test
    public void testCanCreateClass() {

        List<Clazz> classes = new ArrayList();

        Clazz basic7a = new Clazz("Basic 7A");
        Clazz basic7b = new Clazz("Basic 7B");
        Clazz basic8a = new Clazz("Basic 8A");
        Clazz basic8b = new Clazz("Basic 8B");
        Clazz basic9a = new Clazz("Basic 9A");
        Clazz basic9b = new Clazz("Basic 9B");

        Clazz ss1a = new Clazz("SS1A");
        Clazz ss1b = new Clazz("SS1B");
        Clazz ss2a = new Clazz("SS2A");
        Clazz ss2b = new Clazz("SS2B");
        Clazz ss3a = new Clazz("SS3A");
        Clazz ss3b = new Clazz("SS3B");

        classes.add(basic7a);
        classes.add(basic7b);
        classes.add(basic8a);
        classes.add(basic8b);
        classes.add(basic9a);
        classes.add(basic9b);

        classes.add(ss1a);
        classes.add(ss1b);
        classes.add(ss2a);
        classes.add(ss2b);
        classes.add(ss3a);
        classes.add(ss3b);


        classRepository.save(classes);

        List<Clazz> savedClasses = classRepository.findAll();

        Assert.assertEquals(classes.size(), savedClasses.size());

    }
}
