package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.users.Parent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by ozo on 06/11/15.
 */

public interface StudentRepository extends MongoRepository<Student, String> {

    List<Student> findByParentsFirstName(String firstName);
    Student findOneByParentsUsername(String username);
    // Method used to get Students whose results should be processed. Exempt
    List<Student> findByClazzAndSchoolIdAndExemptFromResultProcessing(String clazz, String schoolId, boolean exemptFromResultProcessing);
    @Query("{parentIds: ?0}")
    List<Student> findByParentId(String userId);
    List<Student> findByClazzAndSchoolId(String clazz, String schoolId);

    //For test
    void deleteAll();


}
