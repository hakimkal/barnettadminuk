package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ozo on 06/11/15.
 */

@RestController
public class ParentController extends BaseController {

    private StudentRepository studentRepository;


    @Autowired
    public ParentController(StudentRepository studentRepository) {
        this.studentRepository= studentRepository;
    }

    @RequestMapping("/parents")
    Collection<Parent> parents() {

        return studentRepository.findAll().stream()
                .map(s -> s.getParents()).findFirst().orElse(new ArrayList<>());

    }

    @RequestMapping("/parents/{id}")
    List<Parent> parents(@PathVariable("id") String studentId) {
        Student student = studentRepository.findOne(studentId);
        if (student != null) {
            if (student.getParents() != null) {
                return student.getParents();
            }
        }
        return new ArrayList<>();
    }
}
