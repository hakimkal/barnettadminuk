package com.zo.schoolmanager.domain.results.calculator;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ozo on 29/07/2016.
 */
public class ResultComposition {

    @Id
    private String id;
    private String resultCategory;
    private Double percent;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    // Rare cases where a ResultComposition is made specifically for specific student(s) to override the one set for classes
    private List<String> studentIds;

    public ResultComposition() {}

    public ResultComposition(String resultCategory, Double percent) {
        this.setResultCategory(resultCategory);
        this.setPercent(percent);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultCategory() {
        return resultCategory;
    }

    public void setResultCategory(String resultCategory) {
        this.resultCategory = resultCategory;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public List<String> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<String> studentIds) {
        this.studentIds = studentIds;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }
}
