package com.zo.schoolmanager.domain.results;

import org.springframework.data.annotation.Id;

public class ResultCategory {

    @Id
	private String id;
    private String parentCategory; // The Result Category this Category belongs to. Blank for all Main Categories
    private String name;
    private String schoolId;
    private String description;
    private String shortName;
    private Double maxScore;
    private Integer order;

    // This is used to determine if a score is entered for this category. If is set to false then it means we
    // calculate the scores for this category by adding the sums of its child result categories
    // todo - apply this boolean in logic in ResultCalculator, as currently it is not being used
    private boolean showInPrintableResult;

    private boolean resultKeyedIn;

    public ResultCategory(){}

    public ResultCategory(String parentCategory, String name, String schoolId, String description, Double maxScore,
                          String shortName, int order, boolean showInPrintableResult, boolean resultKeyedIn) {
        this.parentCategory = parentCategory;
        this.name = name;
        this.schoolId = schoolId;
        this.description = description;
        this.maxScore = maxScore;
        this.shortName = shortName;
        this.showInPrintableResult = showInPrintableResult;
        this.order = order;
        this.resultKeyedIn = resultKeyedIn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public boolean isShowInPrintableResult() {
        return showInPrintableResult;
    }

    public void setShowInPrintableResult(boolean showInPrintableResult) {
        this.showInPrintableResult = showInPrintableResult;
    }

    public boolean isResultKeyedIn() {
        return resultKeyedIn;
    }

    public void setResultKeyedIn(boolean resultKeyedIn) {
        this.resultKeyedIn = resultKeyedIn;
    }
}