var React = require('react');
var Reflux = require('reflux');
var TermStore = require('../../stores/term-store');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(TermStore, 'onUpdateTerms')
    ],

    getInitialState: function () {
        return {
            terms: []
        }
    },

    render: function () {

        var options = this.state.terms.map(function (option) {
            return <option key={option}>{option}</option>
        }.bind(this));

        return <select className="form-control">
            {options}
        </select>
    },

    onUpdateTerms: function () {
        this.setState({terms: TermStore.terms});
    }

});


