var React = require('react');
var Reflux = require('reflux');
var ClassStore = require('../../stores/class-store');
var Actions = require('../../actions');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(ClassStore, 'onUpdateClasses'),
        Reflux.listenTo(ClassStore, 'onGetClassesForTeacherOrAdmin'),
    ],

    getInitialState: function () {
        return {
            classes: [],
            showSelectCaption: true
        }
    },

    render: function () {

        var options = this.state.classes.map(function (option) {
            return <option id={option.id} key={option.id} value={option.name}>{option.name}</option>
        }.bind(this));

        return <select className="form-control" onChange={this.setResultCategories}>
            { this.state.showSelectCaption ?
                <option id="0" value="0">-- Select Class --</option>
                :
                null
            }
            {options}
        </select>

    },

    onUpdateClasses: function () {
        this.setState({classes: ClassStore.classes});
        // Reset Result Categories list
        Actions.getResultCategories('0');
    },

    onGetClassesForTeacherOrAdmin: function() {
        if (ClassStore.classes.length === 1) {
            // this has been temporarily set to true as a result of the need to have the user click a class to trigger the selection of result categories
            this.setState({classes: ClassStore.classes, showSelectCaption:true});
        } else {
            this.setState({classes: ClassStore.classes});
        }
    },
    
    setResultCategories: function(e) {
        Actions.getResultCategories(e.target.value);
    }

});