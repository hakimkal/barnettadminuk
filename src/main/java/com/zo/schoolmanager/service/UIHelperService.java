package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.helpers.TableViewItem;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.results.ResultItem;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.domain.results.calculator.ResultCalculator;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.repository.ResultCategoryRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.SubjectRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 15/03/2016.
 */
@Service
public class UIHelperService {

    @Autowired
    private SchoolService schoolService;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ResultItemService resultItemService;

    public List<List<TableViewItem>> getTableViewItems(List<StudentSubjectResult> studentSubjectResults,
                                                       String schoolId, String clazz, final boolean classView) {

        final List<ResultCategory> resultCategories = schoolService.getResultCategories(schoolId, clazz);

        ResultTemplate resultTemplate = schoolService.getResultTemplate(schoolId, clazz);

        List<List<TableViewItem>> items =
                studentSubjectResults
                        .stream()
                        .map( ssr ->  {
                            List<TableViewItem> studentsResults = new ArrayList<>();

                            TableViewItem rowTitle = null;

                            if (classView) {
                                Student student = studentRepository.findOne(ssr.getStudentId());
                                rowTitle = new TableViewItem(ssr.getId(), student.getSurname() + " " + student.getFirstName() +
                                        (!StringUtils.isEmpty(student.getMiddleName()) ? " " + student.getMiddleName() : ""));
                            } else {
                                rowTitle = new TableViewItem(ssr.getId(), ssr.getSubject());
                            }

                            // We need to have result items for all result categories, even if they don't exist - for front end rendering
                            List<ResultItem> resultItems = populateAllResultItems(resultCategories, ssr.getResultItems());


                            // Based on the configuration in ResultTemplate we normalise result items
                            List<ResultItem> normalisedResultItems = new ArrayList<>();

                            if (resultTemplate.isShowOnlyParentResultCategoriesForResultViewing()) {
                                normalisedResultItems = resultItemService.normaliseResultItems(resultItems, schoolId, clazz);
                            } else {
                                normalisedResultItems.addAll(resultItems);
                            }

                            // Create TableViewItem with scores for each List<TableViewItem> and sort them
                            List<TableViewItem> tableViewItems = normalisedResultItems
                                            .stream()
                                            .sorted((a, b) -> new Integer(a.getOrder()).compareTo(b.getOrder()))
                                            .map(ri -> {
                                                if (ri.getScore() != null) {
                                                    return new TableViewItem(ri.getId(), ri.getScore().toString());
                                                } else {
                                                    return new TableViewItem(ri.getId(), "");
                                                }
                                            })
                                            .collect(Collectors.toList());

                            // We need 'Student Name' and associated results as it is all laid out in a single row - in front end
                            studentsResults.add(rowTitle);
                            studentsResults.addAll(tableViewItems);

                            return studentsResults;

                        }).collect(Collectors.toList());

        return items;
    }

    // This method takes all the result categories and the existing result items and creates dummy result items if there
    //  are no existing ones, so that the results can be rendered properly on the front end when a user clicks ->>>
    //      'View Results Entered'
    private List<ResultItem> populateAllResultItems(List<ResultCategory> resultCategories, List<ResultItem> resultItems) {
        // todo - we need to ensure the result categories we use here are for only those for which results are entered

        List<ResultItem> allResultItems = new ArrayList<>();
        allResultItems.addAll(resultItems);

        for (ResultCategory rc : resultCategories) {
            boolean found = false;
            for (ResultItem ri : resultItems) {
                if (ri.getResultCategory().equals(rc.getName())) {
                    found = true;
                }
            }

            if (!found) {
                // Create dummy result item for category and add to list
                ResultItem resultItem = new ResultItem();
                resultItem.setResultCategory(rc.getName());
                resultItem.setParentResultCategory(rc.getParentCategory());
                resultItem.setOrder(rc.getOrder());
                String id = ObjectId.get().toString();
                resultItem.setId(id);

                allResultItems.add(resultItem);
            }
        }

        return allResultItems;
    }
}
