var React = require('react');
var Reflux = require('reflux');
var SubjectStore = require('../../stores/subject-store');
var Actions = require('../../actions');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(SubjectStore, 'onUpdateSubjects')
    ],

    getInitialState: function () {
        return {
            subjects: []
        }
    },

    render: function () {

        var options = this.state.subjects.map(function (option) {
            return <option id={option.id} key={option.id} value={option.name}>{option.name}</option>
        }.bind(this));

        return <select className="form-control" onChange={this.setSubject}>
            <option key={0} value="0">-- Select Subject --</option>
            {options}
        </select>

    },

    setSubject: function (e) {
        Actions.getClassesTaught(e.target.value);
    },

    onUpdateSubjects: function () {
        this.setState({subjects: SubjectStore.subjects});
    }

});