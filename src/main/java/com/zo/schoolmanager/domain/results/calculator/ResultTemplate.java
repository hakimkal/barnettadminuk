package com.zo.schoolmanager.domain.results.calculator;

import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.users.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Created by ozo on 29/07/2016.
 */
@Document(collection = "result_templates")
public class ResultTemplate {

    @Id
    private String id;

    private String schoolId;
    private String description;
    private Set<ResultComposition> resultCompositions;
    private LocalDateTime created;
    private LocalDateTime updated;
    private User lastUpdatedBy;
    private Set<ResultCategory> resultCategories;
    private Set<String> classes;
    private boolean deleted = false;
    private String staticTemplate;
    private boolean showOnlyParentResultCategoriesForResultEntry = false;
    private boolean showOnlyParentResultCategoriesForResultViewing = false;
    private boolean showOnlyParentResultCategoriesForResultPrinting = false;

    public ResultTemplate() {
    }

    public ResultTemplate(String schoolId, Set<ResultComposition> resultCompositions, Set<String> classes) {
        this.schoolId = schoolId;
        this.resultCompositions = resultCompositions;
        this.classes = classes;
    }

    public ResultTemplate(String schoolId, Set<ResultComposition> resultCompositions, Set<String> classes,
                          boolean showOnlyParentResultCategoriesForResultEntry,
                          boolean showOnlyParentResultCategoriesForResultPrinting,
                          boolean showOnlyParentResultCategoriesForResultViewing) {
        this.schoolId = schoolId;
        this.resultCompositions = resultCompositions;
        this.classes = classes;
        this.showOnlyParentResultCategoriesForResultEntry = showOnlyParentResultCategoriesForResultEntry;
        this.showOnlyParentResultCategoriesForResultPrinting = showOnlyParentResultCategoriesForResultPrinting;
        this.showOnlyParentResultCategoriesForResultViewing = showOnlyParentResultCategoriesForResultViewing;
    }

    public ResultTemplate(String schoolId, Set<ResultComposition> resultCompositions,
                          String description, Set<String> classes) {
        this(schoolId, resultCompositions, classes);
        this.description = description;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public User getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(User lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaticTemplate() {
        return staticTemplate;
    }

    public void setStaticTemplate(String staticTemplate) {
        this.staticTemplate = staticTemplate;
    }

    public Set<ResultComposition> getResultCompositions() {
        return resultCompositions;
    }

    public void setResultCompositions(Set<ResultComposition> resultCompositions) {
        this.resultCompositions = resultCompositions;
    }

    public Set<ResultCategory> getResultCategories() {
        return resultCategories;
    }

    public void setResultCategories(Set<ResultCategory> resultCategories) {
        this.resultCategories = resultCategories;
    }

    public Set<String> getClasses() {
        return classes;
    }

    public void setClasses(Set<String> classes) {
        this.classes = classes;
    }

    public boolean isShowOnlyParentResultCategoriesForResultEntry() {
        return showOnlyParentResultCategoriesForResultEntry;
    }

    public void setShowOnlyParentResultCategoriesForResultEntry(boolean showOnlyParentResultCategoriesForResultEntry) {
        this.showOnlyParentResultCategoriesForResultEntry = showOnlyParentResultCategoriesForResultEntry;
    }

    public boolean isShowOnlyParentResultCategoriesForResultViewing() {
        return showOnlyParentResultCategoriesForResultViewing;
    }

    public void setShowOnlyParentResultCategoriesForResultViewing(boolean showOnlyParentResultCategoriesForResultViewing) {
        this.showOnlyParentResultCategoriesForResultViewing = showOnlyParentResultCategoriesForResultViewing;
    }

    public boolean isShowOnlyParentResultCategoriesForResultPrinting() {
        return showOnlyParentResultCategoriesForResultPrinting;
    }

    public void setShowOnlyParentResultCategoriesForResultPrinting(boolean showOnlyParentResultCategoriesForResultPrinting) {
        this.showOnlyParentResultCategoriesForResultPrinting = showOnlyParentResultCategoriesForResultPrinting;
    }
}
