var React = require('react');
var ReactDOM = require('react-dom');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var SelectSubject = require('./dropdowns/select-subject');
var SelectClass = require('./dropdowns/select-class');
var Actions = require('../actions');
var Reflux = require('reflux');
var ClassResultStore = require('../stores/class-result-store');
var ResultHelperStore = require('../stores/result-helper-store');
var ErrorPopUp = require('../shared/error-popup');
var InfoPopUp = require('../shared/info-popup');
var ResultView = require('../shared/result-view');

var StudentResults = React.createClass({

    getInitialState: function () {
        return {
            results: [],
            headers: []
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState(nextProps)
    },

    render: function () {

        var body = this.state.results.map(result => {
            return <ResultView result={result} key={result[0].key} />
        });

        return (
            <tbody>
                {body}
            </tbody>
        )
    }

});

var ViewSubjectResults = React.createClass({

    getInitialState: function () {
        return {
            headers: [],
            results: []
        }
    },

    mixins: [
        Reflux.listenTo(ResultHelperStore, 'onUpdateStudentSubjectResultHeaders'),
        Reflux.listenTo(ClassResultStore, 'onUpdateStudentsSubjectResults')
    ],

    render: function () {

        var headers = this.state.headers.map((rc) => {
            return (
                <th className="text-left" key={rc.key}>{rc.shortName}</th>
            )
        });

        return (
            <div className="col-md-12">
                <div className="panel">
                    <div className="panel-heading">
                        <h3>View Subject Results</h3>
                    </div>

                    <div className="panel-body">
                        <div className="form-group col-md-12">
                            <p className="lead">
                                Select the Subject and associated Class to view Results
                            </p>
                        </div>


                        <div className="form-group col-md-12">
                            <div className="col-sm-2 form-group">
                                <SelectSession ref="session"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectTerm ref="term"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectSubject ref="subject"/>
                            </div>
                            <div className="col-sm-2 form-group">
                                <SelectClass ref="clazz"/>
                            </div>

                            <button className="btn btn-primary" onClick={this.getStudentSubjectResults}>
                                Get Results
                            </button>
                        </div>

                        <div className="form-group col-md-9">
                            <div className="table-responsive">
                                <table className="table table-bordered table-success nomargin">
                                    <thead>
                                    <tr>
                                        {headers}
                                    </tr>
                                    </thead>
                                    <StudentResults headers={this.state.headers} results={this.state.results}/>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },

    componentDidMount: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getSubjects();
    },

    getStudentSubjectResults: function () {

        // All fields must be filled
        if (ReactDOM.findDOMNode(this.refs.subject).value !== '' &&
            ReactDOM.findDOMNode(this.refs.clazz).value !== '0' &&
            ReactDOM.findDOMNode(this.refs.session).value !== '' &&
            ReactDOM.findDOMNode(this.refs.term).value !== '') {

            Actions.getStudentsSubjectResults(ReactDOM.findDOMNode(this.refs.subject).value,
                ReactDOM.findDOMNode(this.refs.clazz).value,
                ReactDOM.findDOMNode(this.refs.session).value,
                ReactDOM.findDOMNode(this.refs.term).value);

            Actions.getResultViewHeaders("Class", ReactDOM.findDOMNode(this.refs.clazz).value);
        }
    },

    onUpdateStudentSubjectResultHeaders: function () {
        this.setState({headers: ResultHelperStore.headers});
    },

    onUpdateStudentsSubjectResults: function () {
        this.setState({results: ClassResultStore.resp});
    }

});

ReactDOM.render(<ViewSubjectResults />, document.getElementById("view-subject-results"));