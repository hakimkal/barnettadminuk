package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 13/04/2016.
 */
public class MultipleTermsException extends ConfigurationException {

    public MultipleTermsException(String message) {
        super(message);
    }
}
