var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getTeachers: function() {
        return Api.get('/api/teachers').then(function(data) {
            this.teachers = data;
            this.trigger('updateTeachers');
        }.bind(this));
    },

    saveTeacher: function(teacher) {
        return Api.post('/api/teachers').then(function(data) {
            this.data = data;
            this.trigger('teacherSaved');
        }.bind(this));
    }

});