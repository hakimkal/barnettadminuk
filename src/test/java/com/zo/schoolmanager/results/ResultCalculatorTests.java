package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.results.PrintableStudentsResult;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.calculator.ResultCalculator;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.commons.util.mock.ResultMocker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.List;

/**
 * Created by ozo on 29/07/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ResultCalculatorTests {


    @Autowired
    private ResultCalculator resultCalculator;

    @Test
    public void testCreatePrintableResults() {

        String schoolId = "212";
        String session = "2015/2016";
        String term = "First Term";
        String clazz = "SS1A";

        List<StudentResult> studentResults = ResultMocker.createStudentResults(schoolId, session, term, clazz, 25, 10);

        PrintableStudentsResult printableStudentsResults =
                resultCalculator.getPrintableResults(clazz, studentResults);

        // Check number of student results in PrintableResults
        Assert.assertTrue(printableStudentsResults.getStudentResults().size() == 25);
        Assert.assertTrue(printableStudentsResults.getClazz().equals(clazz));
//        Assert.assertTrue(printableStudentResults.getSchoolName());

//        printableStudentResults.stream(r -> Assert.assertTrue(r.total != null));
//        printableStudentResults.stream(r -> Assert.assertTrue(r.average != null));
//        printableStudentResults.stream(r -> Assert.assertTrue(r.position != null));
//        printableStudentResults.stream(r -> Assert.assertTrue(r.grade != null));

    }

    @Test
    public void testNormaliseSubCategoryResults() {

        String schoolId = "212";
        String session = "2015/2016";
        String term = "First Term";
        String clazz = "SS1A";



        List<StudentResult> studentResults = ResultMocker.createStudentResults(schoolId, session, term, clazz, 25, 10);

        List<StudentResult> normalisedStudentResults =
                resultCalculator.normaliseSubCategoryResults(studentResults, schoolId);

        Assert.assertTrue(studentResults.size() == normalisedStudentResults.size());

        normalisedStudentResults.forEach(sr -> {
            sr.getStudentSubjectResults().forEach(ssr -> {
                ssr.getResultItems().forEach(ri -> {
                    switch (ri.getResultCategory()) {
                        case "Assignment 1":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Assignment 2":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Assignment 3":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Affective":
                            Assert.assertTrue(ri.getScore() == 5d);
                            break;
                        case "Psychomotor":
                            Assert.assertTrue(ri.getScore() == 2d);
                            break;
                        case "Continuous Assessment 1":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Continuous Assessment 2":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Continuous Assessment 3":
                            Assert.assertTrue(ri.getScore() == 10d);
                            break;
                        case "Examination":
                            Assert.assertTrue(ri.getScore() == 30d);
                            break;
                    }
                });
            });
        });
    }

    @Test
    public void testComputeResultItemScoresBasedOnResultComposition() {

        String schoolId = "212";
        String session = "2015/2016";
        String term = "First Term";
        String clazz = "SS1A";

        ResultTemplate resultTemplate = new ResultTemplate();
        resultTemplate.setResultCompositions(new HashSet<>(ResultMocker.createResultCompositions()));

        List<StudentResult> studentResults = ResultMocker.createStudentResults(schoolId, session, term, clazz, 25, 10);

        List<StudentResult> normalisedStudentResults =
                resultCalculator.normaliseSubCategoryResults(studentResults, schoolId);

        List<StudentResult> studentResultsComputedBasedOnResultComposition =
                resultCalculator.computeResultItemScoresBasedOnResultComposition(normalisedStudentResults,
                        resultTemplate.getResultCompositions());

        studentResultsComputedBasedOnResultComposition.forEach(sr ->
                sr.getStudentSubjectResults().forEach(ssr ->
                        ssr.getResultItems().forEach(ri -> {
                            switch (ri.getResultCategory()) {
                                case "Assignment 1":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Assignment 2":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Assignment 3":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Affective":
                                    Assert.assertTrue(ri.getScore() == 5d);
                                    break;
                                case "Psychomotor":
                                    Assert.assertTrue(ri.getScore() == 2d);
                                    break;
                                case "Continuous Assessment 1":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Continuous Assessment 2":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Continuous Assessment 3":
                                    Assert.assertTrue(ri.getScore() == 10d);
                                    break;
                                case "Examination":
                                    Assert.assertTrue(ri.getScore() == 30d);
                                    break;
                            }
                        })));


    }


    @Test
    public void testComputeTotalsAndAverages() {
        String schoolId = "212";
        String session = "2015/2016";
        String term = "First Term";
        String clazz = "SS1A";

        List<StudentResult> studentResults = ResultMocker.createStudentResults(schoolId, session, term, clazz, 25, 10);

        List<StudentResult> normalisedStudentResults =
                resultCalculator.normaliseSubCategoryResults(studentResults, schoolId);

        List<StudentResult> studentResultsWithTotalsAndAverages = resultCalculator.computeTotalsAndAverages(normalisedStudentResults);

        studentResultsWithTotalsAndAverages.forEach(sr ->
                sr.getStudentSubjectResults().forEach(ssr -> Assert.assertTrue(ssr.getAverage() == 10.777777777777779)));
    }
}
