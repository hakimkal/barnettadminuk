package com.zo.schoolmanager.api;

import com.zo.schoolmanager.controller.BaseController;
import com.zo.schoolmanager.domain.Clazz;
import com.zo.schoolmanager.domain.SubjectClasses;
import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.admin.ClazzDTO;
import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.ClassRepository;
import com.zo.schoolmanager.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 22/01/2016.
 */

@RestController
@RequestMapping("/api/classes")
public class ClassApiController extends BaseController {

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private TeacherService teacherService;

    @RequestMapping
    public List<Clazz> getClasses() {
        List<Clazz> classes = classRepository.findAll();

        if (classes != null) return classes;
        return new ArrayList<>();
    }

    @Secured({"ROLE_SUPER_ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public ApiResponse saveClass(@RequestBody ClazzDTO clazz) {

        Clazz classToSave = new Clazz();
        classToSave.setSchoolId(loggedInUser().getSchoolId());
        classToSave.setName(clazz.getClassName());
        classToSave.setDescription(clazz.getDescription());
        classToSave.setDisplayName(clazz.getDisplayName());

        Clazz savedClass = classRepository.save(classToSave);

        if (savedClass.getId() != null) {
            return success();
        }

        return failure();
    }

    @RequestMapping("/user")
    /**
     * This method gets the class that a teacher is a Class Teacher for. If it is a Super Admin,
     *  it returns all the Classes in the school
     */
    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN"})
    public List<Clazz> getClassesForTeacherOrAdmin() {
        List<Clazz> classes = new ArrayList<>();
        User user = loggedInUser();

        if (user.getRoles().contains("ROLE_CLASS_TEACHER")) {
            Teacher teacher = (Teacher) user;
            classes.add(classRepository.findOneByNameAndSchoolId(teacher.getClazz(), user.getSchoolId()));
        } else if (user.getRoles().contains("ROLE_SUPER_ADMIN")) {
            classes.addAll(classRepository.findBySchoolId(user.getSchoolId()));
        }

        return classes;
    }

    /**
     * This method should get the list of all classes taught for the specific subject by the teacher;
     *  or if the person is an admin should return all the classes
     * @return
     */
    @RequestMapping("/user/{subjectId}")
    public List<Clazz> getClassesTaught(@PathVariable String subjectId) {
        List<Clazz> classes = new ArrayList<>();
        User user = loggedInUser();

        if (user.getUserType() == UserType.TEACHER) {
            Teacher teacher = (Teacher) user;

            List<SubjectClasses> subjectClasses = teacher.getSubjectClasses()
                    .stream()
                    .filter(sc -> sc.getSubject().equals(subjectId))
                    .collect(Collectors.toList());

            if (subjectClasses.size() > 0) {
                classes = subjectClasses.get(0).getClasses()
                        .stream()
                        .map(id -> classRepository.findOne(id))
                        .collect(Collectors.toList());
            }

        } else if (user.getUserType() == UserType.ADMIN) {
            classes = classRepository.findAll();
        }

        return classes;
    }

    @RequestMapping("/user/subject/{subject}")
    public List<Clazz> getClasses(@PathVariable String subject) {

        User user = loggedInUser();

        if (user.getUserType() == UserType.TEACHER) {
            List<Clazz> classes = teacherService.getClassesTaught(user.getId(), subject, user.getSchoolId());

            if (classes != null) return classes;

        } else if (user.getUserType() == UserType.ADMIN && user.getRoles().contains("SUPER_ADMIN")) {
            // todo - return all subjects
            List<Clazz> classes = classRepository.findBySchoolId(user.getSchoolId());

            if (classes != null) return classes;
        }


        return new ArrayList<>();
    }

}
