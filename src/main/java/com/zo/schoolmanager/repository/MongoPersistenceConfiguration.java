package com.zo.schoolmanager.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ozo on 06/11/15.
 */

@Configuration
public class MongoPersistenceConfiguration {

    @Bean
    public CustomConversions customConversions() {
        List<Converter> converters = Arrays.asList(new WriteLocalDateConverter(), new ReadToLocalDateConverter(),
                new WriteLocalDateTimeConverter(), new ReadLocalDateTimeConverter());

        return new CustomConversions(converters);
    }

    @Bean
    public MappingMongoConverter mappingMongoConverter(MongoDbFactory mongoDbFactory,
                   MongoMappingContext mongoMappingContext, CustomConversions customConversions) throws Exception {

        MappingMongoConverter converter =
                new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), mongoMappingContext);
        converter.setCustomConversions(customConversions);

        return converter;
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory,
                                       MappingMongoConverter mappingMongoConverter) throws UnknownHostException {
        return new MongoTemplate(mongoDbFactory, mappingMongoConverter);
    }

}

class WriteLocalDateConverter implements Converter<LocalDate, String> {
    @Override
    public String convert(LocalDate localDate) {
        return localDate == null ? null : localDate.toString();
    }
}

class ReadToLocalDateConverter implements Converter<String, LocalDate> {
    @Override
    public LocalDate convert(String date) {
        return date == null ? null : LocalDate.parse(date);
    }
}

class WriteLocalDateTimeConverter implements Converter<LocalDateTime, String> {
    @Override
    public String convert(LocalDateTime localDateTime) {
        return localDateTime == null ? null : localDateTime.toString();
    }
}

class ReadLocalDateTimeConverter implements  Converter<String, LocalDateTime> {
    @Override
    public LocalDateTime convert(String date) {
        return date == null ? null : LocalDateTime.parse(date);
    }
}
