package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.results.ClassSubjectResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Created by ozo on 01/03/2016.
 */
public interface SubjectClassResultRepository extends MongoRepository<ClassSubjectResult, String> {

    Optional<ClassSubjectResult> findOneBySubjectIdAndClassNameAndSessionAndTerm(String subjectId, String className, String session, String term);
}
