package com.zo.schoolmanager.api;

import com.zo.schoolmanager.controller.BaseController;
import com.zo.schoolmanager.domain.Clazz;
import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.admin.TeacherDTO;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ozo on 30/06/2016.
 */

@RestController
@RequestMapping("/api/teachers")
public class TeacherApiController extends BaseController {

    @Autowired
    private TeacherRepository teacherRepository;

    @Secured({"ROLE_ADMIN", "ROLE_SUPER_ADMIN"})
    @RequestMapping(method = RequestMethod.GET)
    public List<Teacher> getAllTeachers() {
        List<Teacher> teachers = teacherRepository.findAll();
        teachers.stream().forEach(t -> t.setPassword(""));

        return teachers;
    }

    @Secured({"ROLE_ADMIN", "ROLE_SUPER_ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public ApiResponse saveTeacher(@RequestBody TeacherDTO teacherDTO) {

        Teacher teacherToSave = new Teacher();
        teacherToSave.setSchoolId(loggedInUser().getSchoolId());
        teacherToSave.setFirstName(teacherDTO.getFirstName());
        teacherToSave.setMiddleName(teacherDTO.getMiddleName());
        teacherToSave.setLastName(teacherDTO.getLastName());

        if (!StringUtils.isEmpty(teacherDTO.getClassName())) {
            teacherToSave.setClazz(teacherDTO.getClassName());
        }

        teacherToSave.setAddress(teacherDTO.getAddress());

        if (!teacherDTO.getEmail().isEmpty()) {
            teacherToSave.setEmail(teacherDTO.getEmail());
        }

        teacherToSave.setPrimaryPhone(teacherDTO.getPhoneNumber());
        teacherToSave.setSex(Sex.valueOf(teacherDTO.getSex()));
        teacherToSave.setUsername(teacherDTO.getFirstName().toLowerCase() + "." + teacherDTO.getLastName().toLowerCase());

        Teacher savedTeacher = teacherRepository.save(teacherToSave);

        if (!StringUtils.isEmpty(savedTeacher.getId())) {
            return success();
        } else {
            return failure();
        }

    }
}
