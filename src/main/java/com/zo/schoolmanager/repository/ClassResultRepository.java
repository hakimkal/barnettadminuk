package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.results.ClassResult;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ozo on 17/12/2015.
 */
public interface ClassResultRepository extends MongoRepository<ClassResult, String> {

    ClassResult findOneBySchoolIdAndClazzAndSessionAndTerm(String schoolId, String className, String session, String term);
}
