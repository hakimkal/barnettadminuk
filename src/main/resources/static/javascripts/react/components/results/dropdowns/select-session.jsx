var React = require('react');
var Reflux = require('reflux');
var SessionStore = require('../../stores/session-store');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(SessionStore, 'onUpdateSessions')
    ],

    getInitialState: function () {
        return {
            sessions: []
        }
    },

    render: function () {

        var options = this.state.sessions.map(function (option) {
            return <option key={option}>{option}</option>
        }.bind(this));

        return <select className="form-control">
            {options}
        </select>
    },

    onUpdateSessions: function () {
        this.setState({sessions: SessionStore.sessions});
    }

});