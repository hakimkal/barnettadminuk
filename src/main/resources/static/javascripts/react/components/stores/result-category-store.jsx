var Api = require('../util/api')
var Reflux = require('reflux')
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getResultCategories: function(clazz) {
        return Api.get('/result-categories/'+ clazz ).then(function(data) {
            this.resultCategories = data;
            this.trigger("updateResultCategories");
        }.bind(this));
    }

});