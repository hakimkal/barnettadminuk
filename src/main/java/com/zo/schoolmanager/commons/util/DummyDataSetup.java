package com.zo.schoolmanager.commons.util;

import com.zo.schoolmanager.domain.*;
import com.zo.schoolmanager.domain.constants.Position;
import com.zo.schoolmanager.domain.constants.SchoolType;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.Title;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.calculator.ResultComposition;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.domain.school.ResultConfiguration;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.school.Setting;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.*;
import com.zo.schoolmanager.service.SchoolService;
import com.zo.schoolmanager.service.UserService;
import com.zo.schoolmanager.commons.util.mock.ModelMocker;
import com.zo.schoolmanager.commons.util.mock.ResultMocker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by ozo on 12/08/2016.
 * <p>
 * This class is used to setup dummy data which can then be used for test purposes etc.
 * <p>
 * The methods in this class are also presented in order of precedence, so this should give anyone a good idea of
 * what elements they need to setup when setting up a new school
 */
@Component
public class DummyDataSetup {

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private StudentResultRepository studentResultRepository;

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptEncoder;

    @Autowired
    private UserService userService;

    private School school = null;

    private List<Session> sessions = new ArrayList<>();

    private List<Student> students = new ArrayList<>();

    private List<Teacher> subjectTeachers = new ArrayList<>();

    // 1. Create School, Classes, Subjects, Sessions, Setting - ResultConfiguration - ResultTemplate - ResultCategories
    public School createSchool() {
        // Basic Info
        school = ModelMocker.createSchool(null);
        // Save school so we have id
        school = schoolService.save(school);

        // Subjects - 100 will create all subjects
        List<Subject> subjects = ModelMocker.createSubjects(school.getId(), 100);
        subjectRepository.save(subjects);

        // Classes
        List<Clazz> classes = ModelMocker.createClasses(school.getId());
        classes.forEach(c -> {
            List<Subject> randomSubjects = new ArrayList(subjects);
            Collections.shuffle(randomSubjects);
            c.setSubjects(randomSubjects.subList(0, 12).stream().map(Subject::getName).collect(Collectors.toSet()));
        });

        classRepository.save(classes);
        school.setClasses(classes.stream().map(c -> c.getName()).collect(toList()));


        // Sessions
        List<Session> sessions = ModelMocker.createSessions(school.getId(), 3);
        sessionRepository.save(sessions);
        this.sessions = sessions;

        // Setting
        Setting setting = new Setting();
        setting.setSchoolType(SchoolType.NURSERY_PRIMARY_AND_SECONDARY);
        setting.setEnforcesMaxAndMinSubjects(true);

        // ResultConfiguration - under Setting
        ResultConfiguration resultConfiguration = new ResultConfiguration();
        // ResultTemplate
        ResultTemplate resultTemplate = new ResultTemplate();
        // ResultCompositions
        resultTemplate.setSchoolId(school.getId());
        resultTemplate.setDescription("Template for Result Generation for " + school.getName());
        Set<ResultComposition> resultCompositions = new HashSet<>(ResultMocker.createResultCompositions());
        resultTemplate.setResultCompositions(resultCompositions);
        resultTemplate.setClasses(new HashSet<>(school.getClasses()));
        resultTemplate.setCreated(LocalDateTime.now());

        // ResultCategories
        Set<ResultCategory> resultCategories = new HashSet<>(ResultMocker.createResultCategoriesWithSubCategoriesIncluded(school.getId()));
        resultTemplate.setResultCategories(resultCategories);
        resultTemplate.setShowOnlyParentResultCategoriesForResultEntry(true);
        resultTemplate.setShowOnlyParentResultCategoriesForResultPrinting(true);
        resultTemplate.setShowOnlyParentResultCategoriesForResultViewing(true);

        resultConfiguration.setResultTemplates(Arrays.asList(resultTemplate));
        // todo resultConfiguration.setResultFlow(resultFlow)
        setting.setResultConfiguration(resultConfiguration);

        // todo setting.setAccountConfiguration(accountConfiguration)
        school.setSetting(setting);

        schoolService.save(school);

        return school;
    }

    // 2. Create a School Admin
    public User createAdminUser() {
        User user = new User(Title.Mr, "Admin", "Admin", Sex.Male, "22 Drake Way");

        user.setPassword(bCryptEncoder.encode("admin"));
        user.setUsername("admin");

        user.setSchoolId(school.getId());

        Set<String> roles = new HashSet<>(Arrays.asList("ROLE_ADMIN"));
        user.setRoles(roles);
        userService.save(user);

        return user;
    }

    // 3. Create Students for each Class
    public List<Student> createStudents() {

        List<Student> students = new ArrayList<>();

        List<String> classes = school.getClasses();
        // Create random number of students for each class between 20 and 30
        classes.forEach(c -> students.addAll(ModelMocker.createStudents(school.getId(), new Random().nextInt(30 - 20 + 1) + 20, classes)));

        studentRepository.save(students);

        this.students = students;

        return students;
    }

    // 4. Create Subject Teachers
    public List<Teacher> createSubjectTeachers() {

        List<Teacher> teachers = ModelMocker.createGenericTeachers(school.getId(), 30);
        teachers.forEach(t -> t.setPassword(bCryptEncoder.encode("teacher")));
        teacherRepository.save(teachers);

        subjectTeachers = teachers;

        return teachers;
    }

    // 5. Create Class Teachers for each class
    public List<Teacher> createClassTeachers() {

        List<String> classes = school.getClasses();

        List<Teacher> allTeachers = new ArrayList<>(subjectTeachers);
        allTeachers.stream().filter(t -> !t.getUsername().equals("admin"));

        List<Teacher> teachers = allTeachers.subList(0, classes.size());
        Collections.shuffle(teachers);

        List<Teacher> classTeachers = ModelMocker.createClassTeachers(teachers, classes);

        return teacherRepository.save(classTeachers);
    }

    // 6. Assign subjects to teachers
    public List<Teacher> assignSubjectsToTeachers() {
        List<String> classNames = school.getClasses();

        List<Clazz> classes = classNames.stream()
                .map(c -> classRepository.findOneByNameAndSchoolId(c, school.getId()))
                .collect(Collectors.toList());

        classes.forEach(c ->
            c.getSubjects().forEach(s -> {
                List<Teacher> randomTeachers = new ArrayList<>(subjectTeachers);
                Collections.shuffle(randomTeachers);

                Teacher randomTeacher = randomTeachers.get(0);

                boolean found = false;
                for (SubjectClasses sc : randomTeacher.getSubjectClasses())  {
                    if (s.equals(sc.getSubject())) {
                        sc.getClasses().add(c.getName());
                        found = true;
                        randomTeacher.getRoles().add("ROLE_SUBJECT_TEACHER");

                        if (randomTeacher.getClazz() == null) {
                            randomTeacher.setPosition(Position.SUBJECT_TEACHER);
                        } else {
                            randomTeacher.setPosition(Position.CLASS_AND_SUBJECT_TEACHER);
                        }
                        break;
                    }
                }

                if (!found) {
                    SubjectClasses subjectClasses = new SubjectClasses();
                    subjectClasses.setSubject(s);
                    subjectClasses.getClasses().add(c.getName());
                    randomTeacher.getSubjectClasses().add(subjectClasses);
                    randomTeacher.getRoles().add("ROLE_SUBJECT_TEACHER");

                    if (randomTeacher.getClazz() == null) {
                        randomTeacher.setPosition(Position.SUBJECT_TEACHER);
                    } else {
                        randomTeacher.setPosition(Position.CLASS_AND_SUBJECT_TEACHER);
                    }
                }
            }));

        teacherRepository.save(subjectTeachers);

        return subjectTeachers;
    }

    // 7. Create Results for Students in all classes
    public List<StudentResult> createStudentResults() {

        final List<StudentResult> studentResults = new ArrayList<>();

        Map<String, List<Student>> classStudents = students.stream().collect(Collectors.groupingBy(Student::getClazz));

        classStudents.forEach((k,v) -> {
            List<String> subjects = classRepository.findOneByNameAndSchoolId(k, school.getId()).getSubjects()
                    .stream()
                    .collect(Collectors.toList());


            studentResults.addAll(ResultMocker.createStudentResults(school.getId(), sessions, v, subjects));

            studentResults.forEach(sr -> sr.getStudentSubjectResults().forEach(studentSubjectResultRepository::save));

            studentResultRepository.save(studentResults);
        });

        return studentResults;

    }


}
