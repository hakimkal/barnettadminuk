package com.zo.schoolmanager.repository;


import com.zo.schoolmanager.domain.payment.PaymentRecord;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Eze on 08/03/2016.
 */
public interface PaymentRecordRepository extends MongoRepository<PaymentRecord, String> {


    List<PaymentRecord> findByPaymentItemIdAndSessionAndTerm(String paymentItemId, String session, String term);
    List<PaymentRecord> findByStudentIdAndSessionAndTerm(String studentId,String session, String term);
    List<PaymentRecord> findByStudentIdAndPaymentItemId(String studentId, String paymentItemId);
    List<PaymentRecord> findByStudentId(String studentId);
    //For Test
    void deleteAll();
}
