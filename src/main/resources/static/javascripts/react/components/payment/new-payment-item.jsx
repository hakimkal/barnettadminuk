var React = require('react');
var ReactDOM = require('react-dom');
var Actions = require('../actions');
var Form = require('../shared/form');

var firstColumn = [
    {type:'string', title:'Name', validation: {required: true}},
    {type:'session', title:'Session', validation: {required: true}},
    {type:'multiple-terms', title:'Terms', validation: {required: true}},
    {type:'multiple-classes', title:'Classes', validation: {required: true}}
];
var secondColumn = [
    {type:'amount', title:'Amount', validation: {required: true}},
    {type:'date', title:'Due Date', validation: {required: true}},
    {type:'text-area', title:'Description', validation: {required: true}}
];

var NewPaymentItem = React.createClass({

    render: function () { 

        return (

            <Form title={"Create New Payment Item"} firstColumn={firstColumn} secondColumn={secondColumn}
                onSubmit={this.handleSubmit }
                />
        )
    },

    handleSubmit (data) {
        console.log('submit..' + JSON.stringify(data));
    },
    

    componentWillMount: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getAllClasses();
    }

});

ReactDOM.render(<NewPaymentItem />, document.getElementById("new-payment-item"));