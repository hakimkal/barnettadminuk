package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ozo on 18/12/2015.
 */
public interface UserRepository extends MongoRepository<User, String> {

    public List<User> findAllByUserType(UserType userType);
    public User findOneByUsername(String username);
    public User findOneByUsernameAndPassword(String username, String password);
}
