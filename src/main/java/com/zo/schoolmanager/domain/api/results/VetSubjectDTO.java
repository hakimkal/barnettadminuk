package com.zo.schoolmanager.domain.api.results;

import com.zo.schoolmanager.domain.api.ResponseObject;
import com.zo.schoolmanager.domain.results.Result;

/**
 * Created by ozo on 05/03/2016.
 */
public class VetSubjectDTO implements ResponseObject {

    private String id;
    private String studentName;
    private Result.ResultStatus status;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Result.ResultStatus getStatus() {
        return status;
    }

    public void setStatus(Result.ResultStatus status) {
        this.status = status;
    }
}
