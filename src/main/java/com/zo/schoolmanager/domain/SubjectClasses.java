package com.zo.schoolmanager.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 26/11/15.
 */
public class SubjectClasses {

    private String subject;
    private List<String> classes = new ArrayList<>();

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }
}
