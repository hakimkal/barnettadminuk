package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Clazz;
import com.zo.schoolmanager.domain.SubjectClasses;
import com.zo.schoolmanager.domain.api.admin.UserDTO;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.Title;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.repository.ClassRepository;
import com.zo.schoolmanager.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 26/11/15.
 */

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private ClassRepository classRepository;


    public List<Clazz> getClassesTaught(String userId, String subject, String schoolId) {

        List<Clazz> classes = new ArrayList<>();

        // TODO - This can be scrapped if you are able to pass the User object to this method
        List<SubjectClasses> subjectClassesList =
                teacherRepository.findOne(userId).getSubjectClasses().stream()
                        .filter(sc -> sc.getSubject().equals(subject))
                        .collect(Collectors.toList());

        SubjectClasses subjectClasses = subjectClassesList.size() > 0 ? subjectClassesList.get(0) : null;

        if (subjectClasses != null)
            classRepository.findByNameInAndSchoolId(subjectClasses.getClasses(), schoolId).forEach(classes::add);

        return classes;
    }

    public Teacher saveTeacher(UserDTO userDTO) {

        Teacher teacher = convertToTeacher(userDTO);
        if (teacher.getFirstName() != null && teacher.getLastName() != null &&
                !teacher.getFirstName().equals("") && !teacher.getLastName().equals("")) {
            teacher = teacherRepository.save(teacher);
        }
        return teacher;
    }




    private Teacher convertToTeacher(UserDTO userDTO) {
        Teacher teacher = new Teacher();
        if (!userDTO.getClazz().equals("")) teacher.setClazz(userDTO.getClazz());
        if (!userDTO.getEmail().equals("")) teacher.setEmail(userDTO.getEmail());
        if (!userDTO.getFirstName().equals("")) teacher.setFirstName(userDTO.getFirstName());
        if (!userDTO.getLastName().equals("")) teacher.setLastName(userDTO.getLastName());
        if (!userDTO.getMiddleName().equals("")) teacher.setMiddleName(userDTO.getMiddleName());
        if (!userDTO.getPrimaryPhone().equals("")) teacher.setPrimaryPhone(userDTO.getPrimaryPhone());
        if (!userDTO.getSecondaryPhone().equals("")) teacher.setOtherPhone(userDTO.getSecondaryPhone());
        if (!userDTO.getSex().equals("")) Sex.valueOf(userDTO.getSex());
        if (!userDTO.getTitle().equals("")) Title.valueOf(userDTO.getTitle());

        return teacher;
    }


    /**
     * Assigns a school class to a Teacher.
     * */


}
