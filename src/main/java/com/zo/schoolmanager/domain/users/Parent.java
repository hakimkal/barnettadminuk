package com.zo.schoolmanager.domain.users;


import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.constants.UserType;
import org.springframework.data.annotation.Transient;

import java.util.List;

/**
 * Created by ozo on 05/11/15.
 */
public class Parent extends User {

    private String phone2;
    private String phone3;
    private List<String> studentIds;

    @Transient
    private List<Student> children;

    public Parent() {}

    public Parent(String firstName, String lastName, String middleName, String primaryPhone, String password, String schoolId) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPrimaryPhone(primaryPhone);
        this.setUsername(firstName.toLowerCase() + "." + lastName.toLowerCase());
        this.setPassword(password);
        this.setSchoolId(schoolId);
        this.setMiddleName(middleName);
        this.setUserType(UserType.PARENT);
    }

    public Parent(String firstName, String lastName, String middleName, String primaryPhone, String password, String schoolId, String phone2) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPrimaryPhone(primaryPhone);
        this.setUsername(firstName.toLowerCase() + "." + lastName.toLowerCase());
        this.setPassword(password);
        this.setSchoolId(schoolId);
        this.setMiddleName(middleName);
        this.phone2 = phone2;
        this.setUserType(UserType.PARENT);
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone3() {
        return phone3;
    }

    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }

    public List<Student> getChildren() {
        return children;
    }

    public void setChildren(List<Student> children) {
        this.children = children;
    }

    public List<String> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<String> studentIds) {
        this.studentIds = studentIds;
    }
}
