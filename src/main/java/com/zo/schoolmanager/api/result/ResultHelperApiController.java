package com.zo.schoolmanager.api.result;

import com.zo.schoolmanager.controller.BaseController;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.helpers.TableViewHeader;
import com.zo.schoolmanager.domain.api.helpers.TableViewItem;
import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.results.Result;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.domain.results.calculator.ResultCalculator;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.repository.*;
import com.zo.schoolmanager.service.UIHelperService;
import com.zo.schoolmanager.service.SchoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by ozo on 10/03/2016.
 */

@RestController
public class ResultHelperApiController extends BaseController {

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UIHelperService uiHelperService;

    @Autowired
    private ResultCalculator resultCalculator;

    @Autowired
    private StudentResultRepository studentResultRepository;

    private final Logger LOG = LoggerFactory.getLogger(ResultHelperApiController.class);

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/helpers/students-subject-results", method = RequestMethod.GET)
    public List<List<TableViewItem>> getStudentsSubjectResults(@RequestParam String subjectId,
                                                               @RequestParam String clazz,
                                                               @RequestParam String session,
                                                               @RequestParam String term) {

        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository
                        .findBySessionAndTermAndClazzAndSubjectAndSchoolId(session, term, clazz, subjectId, loggedInUser().getSchoolId())
                        .stream()
                        .sorted((a, b) -> studentRepository.findOne(a.getStudentId()).getSurname().compareTo(
                                studentRepository.findOne(b.getStudentId()).getSurname()
                        )).collect(Collectors.toList());

        return uiHelperService.getTableViewItems(studentSubjectResults, loggedInUser().getSchoolId(), clazz, true);
    }

    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/helpers/student-results", method = RequestMethod.GET)
    public Map<String, Object> getStudentResults(@RequestParam String studentId, @RequestParam String session,
                                                       @RequestParam String term, @RequestParam String clazz) {


        Map<String, Object> result = new HashMap<>();

        // Get the Results
        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository
                        .findByStudentIdAndSessionAndTermAndSchoolId(studentId, session, term, loggedInUser().getSchoolId())
                        .stream()
                        .sorted((a, b) -> subjectRepository.findOneByNameAndSchoolId(a.getSubject(), loggedInUser().getSchoolId()).getName().compareTo(
                                subjectRepository.findOneByNameAndSchoolId(a.getSubject(), loggedInUser().getSchoolId()).getName())).collect(Collectors.toList());


        List<List<TableViewItem>> results = uiHelperService.getTableViewItems(studentSubjectResults, loggedInUser().getSchoolId(), clazz, false);
        result.put("results", results);

        // Get the Headers for the Results based on the configuration in ResultTemplate
        ResultTemplate resultTemplate = schoolService.getResultTemplate(loggedInUser().getSchoolId(), clazz);
        List<ResultCategory> resultCategories = new ArrayList<>();

        if (resultTemplate.isShowOnlyParentResultCategoriesForResultViewing()) {
            resultCategories.addAll(schoolService.getParentResultCategories(loggedInUser().getSchoolId(), clazz));
        } else {
            resultCategories.addAll(schoolService.getResultCategories(loggedInUser().getSchoolId(), clazz));
        }
        List<TableViewHeader> headers =
                resultCategories
                        .stream()
                        .map(rc -> new TableViewHeader(rc.getId(), rc.getName(), rc.getShortName(), rc.getOrder()))
                        .sorted((a,b) -> a.getOrder().compareTo(b.getOrder()))
                        .collect(Collectors.toList());

        result.put("headers", getHeaders(headers, "Student"));

        // Get the Student
        result.put("student", studentRepository.findOne(studentId));

        // Add teacherComment if present
        StudentResult studentResult = studentResultRepository.findOneByStudentIdAndSessionAndTerm(studentId,
                session, term);

        if (studentResult != null) {
            result.put("teacherComments", studentResult.getTeacherComments());
            result.put("resultStatus", studentResult.getStatus().toString());
        }

        return result;
    }

    @RequestMapping(value = "/helpers/student-subject-result-headers", method = RequestMethod.GET)
    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN", "ROLE_ADMIN"})
    public ApiResponse getStudentSubjectResultHeaders(@RequestParam String resultFor, @RequestParam String clazz) {

        ApiResponse success = success();

        String schoolId = loggedInUser().getSchoolId();

        ResultTemplate resultTemplate = schoolService.getResultTemplate(schoolId, clazz);

        List<ResultCategory> resultCategories = new ArrayList<>();

        if (resultTemplate.isShowOnlyParentResultCategoriesForResultViewing()) {
            resultCategories.addAll(schoolService.getParentResultCategories(schoolId, clazz));
        } else {
            resultCategories.addAll(schoolService.getResultCategories(schoolId, clazz));
        }

        List<TableViewHeader> headers =
                resultCategories
                        .stream()
                        .map(rc -> new TableViewHeader(rc.getId(), rc.getName(), rc.getShortName(), rc.getOrder()))
                        .sorted((a,b) -> a.getOrder().compareTo(b.getOrder()))
                        .collect(Collectors.toList());

        success.setResults(getHeaders(headers, resultFor));

        return success;
    }


    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/helpers/teacher-comment", method = RequestMethod.POST)
    public ApiResponse saveTeacherComments(@RequestBody Map<String, String> data) {

        ApiResponse success = success();
        ApiResponse failure = failure();

        try {

            StudentResult studentResult = studentResultRepository.findOneByStudentIdAndSessionAndTerm(data.get("studentId"),
                    data.get("session"), data.get("term"));

            Student student = studentRepository.findOne(data.get("studentId"));

            if (loggedInUser().getUserType() == UserType.TEACHER) {
                Teacher teacher = (Teacher) loggedInUser();
                if (!teacher.getClazz().equals(student.getClazz())) {
                    throw new Exception("Class Teacher not in charge of " + student.getClazz());
                }
            }

            // todo - improved logic needed here to check each state of studentResult.getStatus()
            // todo - this logic should also be driven by ResultFlow
            if (studentResult != null) {
                if (studentResult.getStatus() != Result.ResultStatus.CLASS_TEACHER_COMMENTED) {
                    throw new Exception("Cant make a comment on Student Result: " + student.getId() + "; It should have passed that stage");
                }
                studentResult.setTeacherComments(data.get("teacherComments"));
            } else {
                studentResult = new StudentResult(data.get("studentId"), data.get("session"), data.get("term"),
                        student.getClazz(), loggedInUser().getSchoolId());
                studentResult.setTeacherComments(data.get("teacherComments"));
                studentResult.setStatus(Result.ResultStatus.CLASS_TEACHER_COMMENTED);
            }

            studentResultRepository.save(studentResult);

            return  success;

        } catch (Exception ex) {

            ex.printStackTrace();
            return failure;
        }
    }

    private List<TableViewHeader> getHeaders(List<TableViewHeader> headers, String type) {
        List<TableViewHeader> tableViewHeaders = new ArrayList<>();

        if (headers != null && !headers.isEmpty()) {

            // We need 'Student Name' as header and I cant find a cleaner way to add it
            if (type.equals("Class")) {
                tableViewHeaders.add(new TableViewHeader("0", "Student Name", "Student Name", -1));
            } else {
                tableViewHeaders.add(new TableViewHeader("0", "Subject", "Subject", 0));
            }
            tableViewHeaders.addAll(headers);
        }

        return tableViewHeaders;
    }
}
