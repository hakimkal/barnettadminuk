package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.results.StudentResultBasicDTO;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by ozo on 18/03/2016.
 */

@Controller
public class StudentController extends BaseController {

    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentRepository studentRepository;

    private static final String STUDENT_PAGE = "student";

    @RequestMapping("/students/name-wrappers")
    @ResponseBody
    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN"})
    public List<StudentResultBasicDTO> getStudents(@RequestParam String clazz,
                                                   @RequestParam String session, @RequestParam String term) {

        User user = loggedInUser();
        List<Student> students =
                studentRepository.findByClazzAndSchoolId(clazz, user.getSchoolId());


        return studentService.getStudentResultBasicDTOs(students, session, term);
    }

    @RequestMapping("/student")
    @ResponseBody
    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN", "ROLE_ADMIN"})
    public Student getStudent(@RequestParam String studentId) {

        return studentRepository.findOne(studentId);
    }


    @ResponseBody
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_ADMIN"})
    @RequestMapping(value = "/student-save", method = RequestMethod.POST)
    public ApiResponse saveStudent(@RequestBody Map<String, String> studentMap) {

        User user = loggedInUser();

        Student student = new Student();
        student.setSurname(studentMap.get("surname"));
        student.setFirstName(studentMap.get("firstName"));
        student.setAddress(studentMap.get("address"));
        student.setClazz(studentMap.get("class"));
        student.setMiddleName(studentMap.get("middleName"));
        student.setSex(Sex.valueOf(studentMap.get("sex")));


//        student.setDateOfBirth(LocalDate.parse("dateOfBirth", DateTimeFormatter.ofPattern("dd-MM-yyyy")));

        student.setCreated(LocalDateTime.now());
        student.setNationality(studentMap.get("country"));
        student.setStateOfOrigin(studentMap.get("state"));

        student.setSchoolId(user.getSchoolId());


        studentRepository.save(student);

        return success();

    }

    @Secured({"ROLE_SUPER_ADMIN", "ROLE_ADMIN"})
    @RequestMapping("/students/new-student")
    public String newStudent(Model model) {

        model.addAttribute("page", STUDENT_PAGE);
        model.addAttribute("user", loggedInUser());

        return "students/new-student";

    }
}
