package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.TestSetup;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.helpers.TableViewItem;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.results.ResultItem;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.repository.ResultCategoryRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import com.zo.schoolmanager.service.UIHelperService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 15/03/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ResultItemTests extends TestSetup {

    @Autowired
    private UIHelperService uiHelperService;

    @Autowired
    private ResultCategoryRepository resultCategoryRepository;
    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void testGetResultItemViews() {

        List<Student> students = students(1);
        List<Student> savedStudents = students.stream().map(s -> studentRepository.save(s)).collect(Collectors.toList());

        savedStudents.forEach(s -> {
            StudentSubjectResult studentSubjectResult = createStudentSubjectResult(s.getId(), "ChemistryId");

            ResultCategory exam = resultCategoryRepository.findOneByNameAndSchoolId("Examination", ("56bc3d80d4c60e72a53806c0"));
            ResultCategory ass3 = resultCategoryRepository.findOneByNameAndSchoolId("Assignment 3", ("56bc3d80d4c60e72a53806c0"));


            ResultItem resultItem1 = new ResultItem(ass3.getName(), ass3.getParentCategory(), 10d, ResultItem.ResultStatus.SCORE_SAVED, ass3.getMaxScore());
            ResultItem resultItem2 = new ResultItem(exam.getName(), ass3.getParentCategory(), 40d, ResultItem.ResultStatus.SCORE_SAVED, exam.getMaxScore());

            resultItem1.setOrder(ass3.getOrder());
            resultItem2.setOrder(exam.getOrder());

            List firstResults = Arrays.asList(resultItem1, resultItem2);

            studentSubjectResult.getResultItems().addAll(firstResults);
            studentSubjectResultRepository.save(studentSubjectResult);
        });

        List<StudentSubjectResult> ssrs = studentSubjectResultRepository.findBySessionAndTermAndClazzAndSubjectAndSchoolId("2015/2016",
                "First Term", "SS3D", "ChemistryId", "schoolId");

        List<List<TableViewItem>> resultItemList = uiHelperService.getTableViewItems(ssrs,"56bc3d80d4c60e72a53806c0", "SS3D", true);

        Assert.assertTrue(resultItemList.get(0).size() == 8);

    }

    @Test
    public void testResultItemsHaveIds() {
        List<Student> students = studentRepository.findByClazzAndSchoolId("SS2A", "57c4b5ffd4c60b88cca3dfb8");

        List<StudentSubjectResult> studentSubjectResults = studentSubjectResultRepository
                .findByStudentIdAndSessionAndTermAndSchoolId(students.get(0).getId(), "2015/2016", "First Term", "57c4b5ffd4c60b88cca3dfb8");

        List<ResultItem> resultItems = studentSubjectResults.get(0).getResultItems();

        resultItems.forEach(ri -> Assert.assertNotNull(ri.getId()));
    }

    private StudentSubjectResult createStudentSubjectResult(String studentId, String subject) {
        StudentSubjectResult studentSubjectResult = new StudentSubjectResult(studentId, "2015/2016", "First Term", "SS3D",
                subject, "schoolId");

        return studentSubjectResultRepository.save(studentSubjectResult);
    }

//    @Before
    public void deleteAllData() {
        studentSubjectResultRepository.delete(
        studentSubjectResultRepository
                .findBySessionAndTermAndClazzAndSubjectAndSchoolId("2015/2016", "First Term", "SS3D", "ChemistryId", "schoolId"
                ));

    }

}
