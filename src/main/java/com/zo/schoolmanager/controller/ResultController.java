package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.constants.UserType;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by ozo on 07/02/2016.
 */

@Controller
public class ResultController extends BaseController {

    private final String RESULT_PAGE = "result";


    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping("/results/enter-results")
    public String showDossierForm(Model model) {
        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        return "results/enter-results";
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/submit-subject-results", method = RequestMethod.GET)
    public String showSubmitSubjectResultsPage(Model model) {

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        return "results/submit-subject-results";

    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/submit-class-results", method = RequestMethod.GET)
    public String showSubmitClassResultsPage(Model model) {

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        return "results/submit-class-results";

    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value="/results/vet-subject-results", method = RequestMethod.GET)
    public String showVetSubjectPage(Model model, @RequestParam(value = "className", required = false) String className,
                                     @RequestParam(value = "subject", required = false) String subject) {

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        if (loggedInUser().getRoles().contains("ROLE_SUPER_ADMIN") && className != null) {

        }

        return "results/vet-subject-results";
    }

    @Secured({"ROLE_CLASS_RESULT_VETTER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value="/results/vet-class-results", method = RequestMethod.GET)
    public String showVetClassResultsPage(Model model, @RequestParam(value = "className", required = false) String className) {

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        if (loggedInUser().getRoles().contains("ROLE_CLASS_RESULT_VETTER") && className != null) {

        }

        return "results/vet-class-results";
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value="/results/view-subject-results", method = RequestMethod.GET)
    public String showViewSubjectResultsPage(Model model) {

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", RESULT_PAGE);

        return "results/view-subject-results";
    }

    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN", "ROLE_HOD"})
    @RequestMapping(value = "/results/view-class-results", method = RequestMethod.GET)
    public String showClassResultsPage(Model model) {

        model.addAttribute("page", RESULT_PAGE);
        model.addAttribute("user", loggedInUser());

        return "results/view-class-results";
    }

    @RequestMapping("/results/view-student-result")
    @Secured({"ROLE_CLASS_TEACHER", "ROLE_SUPER_ADMIN"})
    public String showStudentResult(Model model, @RequestParam String studentId, @RequestParam String session,
                                    @RequestParam String term, @RequestParam String clazz) {

        model.addAttribute("page", RESULT_PAGE);
        model.addAttribute("user", loggedInUser());
        model.addAttribute("studentId", studentId);
        model.addAttribute("sessionName", session);
        model.addAttribute("term", term);
        model.addAttribute("clazz", clazz);

        return "results/view-student-result";
    }

}
