package com.zo.schoolmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BarnettApplication {

    public static void main(String[] args) {
        SpringApplication.run(BarnettApplication.class, args);
    }
}
