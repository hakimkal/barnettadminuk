var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    submitResultToClassTeacher: function (subjectId, clazz, session, term) {
        return Api.get('/results/submit-to-class-teacher?' + 'subjectId='+subjectId +
            '&className=' + clazz + '&session=' + session + '&term=' + term).then(function (data) {
            this.response = data;
            this.trigger('submitResultToClassTeacherNotification');
        }.bind(this));
    },

    submitResultsForFinalVetting: function(clazz, session, term) {
        return Api.get('/results/submit-for-final-vetting?' + 'className='+clazz + '&session=' + session +
            '&term=' + term).then(function (data) {
            this.response = data;
            this.trigger('submitResultsForFinalVettingNotification');
        }.bind(this));
    },

    getStudentSubjectResults: function (subjectId, clazz, session, term) {
        return Api.get('/results/students-subject-vetting-helpers?' + 'subjectId='+subjectId + '&className='+ clazz +
            '&session='+session + '&term='+term).then(function (data) {
            this.response = data;
            this.fromUpdate = false;
            this.trigger('updateSubjectResults');
        }.bind(this));
    },

    getStudentsSubjectResults: function (subjectId, clazz, session, term) {
        return Api.get('/helpers/students-subject-results?' + 'subjectId=' + subjectId + '&clazz=' + clazz +
            '&session=' + session + '&term=' + term).then(function (data) {
                this.resp = data;
                this.trigger('updateStudentsSubjectResults');
        }.bind(this));
    },

    vetSubjectResult: function (resultId, subjectId, clazz, session, term) {
        return Api.get('/results/vet-subject?' +
            'resultId='+resultId + '&subjectId='+subjectId + '&className='+clazz + '&session='+session +
            '&term='+term).then(function (data) {
            this.response = data;
            this.fromUpdate = true;
            this.trigger('updateSubjectResults');
        }.bind(this))
    },

    saveTeacherComments: function(json) {
        return Api.post("/helpers/teacher-comment", json).then(function (data) {
            this.response = data;
            this.trigger('saveTeacherComments');
        }.bind(this));
    }

});