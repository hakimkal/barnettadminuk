package com.zo.schoolmanager;

import com.zo.schoolmanager.commons.util.DummyDataSetup;
import com.zo.schoolmanager.domain.Session;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.*;
import com.zo.schoolmanager.service.SchoolService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by ozo on 25/08/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class BaseSetupTests {

    @Autowired
    private DummyDataSetup dummyDataSetup;

    @Autowired
    private SchoolService schoolService;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private StudentResultRepository studentResultRepository;
    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;



    @Test
    public void setup() {
        School school = dummyDataSetup.createSchool();
        User admin = dummyDataSetup.createAdminUser();
        List students = dummyDataSetup.createStudents();
        List subjectTeachers = dummyDataSetup.createSubjectTeachers();
        List classTeachers = dummyDataSetup.createClassTeachers();
        List teachers = dummyDataSetup.assignSubjectsToTeachers();
        // Results
        List<StudentResult> studentResults = dummyDataSetup.createStudentResults();
        // Others
        List<Session> sessions = sessionRepository.getSessionBySchoolId(school.getId());


        Assert.assertNotNull(school);
        Assert.assertNotNull(admin);
        Assert.assertNotNull(students);
        Assert.assertNotNull(subjectTeachers);
        Assert.assertNotNull(classTeachers);
        Assert.assertNotNull(teachers);
        //Results
        Assert.assertNotNull(studentResults);
        //Others
        Assert.assertNotNull(sessions);
    }

    @Before
    public void clearDb() {
        schoolService.deleteAll();
        userRepository.deleteAll();
        studentRepository.deleteAll();
        subjectRepository.deleteAll();
        classRepository.deleteAll();
        sessionRepository.deleteAll();
        studentResultRepository.deleteAll();
        studentSubjectResultRepository.deleteAll();
    }
}
