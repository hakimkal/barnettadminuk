var React = require('react');
var ReactDOM = require('react-dom');
var SelectClass = require('./dropdowns/select-class');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var Actions = require('../actions');
var Reflux = require('reflux');
var ClassResultStore = require('../stores/class-result-store');
var Modal = require('react-bootstrap/lib/Modal');

var SubmitResult = React.createClass({

    mixins: [
        Reflux.listenTo(ClassResultStore, 'onSubmitResultsForFinalVettingNotification')
    ],

    getInitialState: function () {
        return {
            resultsSubmitted: false,
            success: false,
            failure: false,
            customMessage: ''
        }
    },

    render: function () {
        return (
            <div className="col-md-12">
                <div className="panel">
                    <div className="panel-heading">
                        <h3>Submit Results for Final Vetting</h3>
                    </div>

                    <div className="panel-body">

                        { !this.state.resultsSubmitted ?
                            (
                                <div className="text-danger form-group">
                                    <p className="lead">
                                        Results should be submitted for final vetting only after you have entered
                                        comments and vetted the results.
                                        </p>
                                    <p className="lead">
                                        Once you submit results, this action cannot be reversed!
                                        &nbsp; <a className="underline-link" href="/results/view-class-results">Click
                                        Here to Enter Comments</a>
                                    </p>
                                </div>
                            ) :

                            (
                                <div className="text-info form-group">
                                    <p className="lead">
                                        Results have been submitted!
                                    </p>
                                </div>
                            )
                        }

                        <div className="form-group">
                            <div className="col-sm-2">
                                <SelectSession ref="session"/>
                            </div>
                            <div className="col-sm-2">
                                <SelectTerm ref="term"/>
                            </div>
                            <div className="col-sm-2">
                                <SelectClass ref="clazz"/>
                            </div>
                        </div>


                        <div className="form-group">
                            <button className="btn btn-primary" onClick={this.submitResultsForFinalVetting}>
                                Submit Results
                            </button>
                            &nbsp;

                            <a href="/index" className="btn btn-success">
                                Back to Dashboard
                            </a>
                        </div>

                    </div>

                    {
                        /* Todo - refactor this logic used across the system into mixin*/
                        <Modal show={this.state.success} onHide={this.close}>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    {'Comments'}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className={this.state.customMessage == '' ? 'alert alert-info fade in nomargin' :
                                'alert alert-warning fade in nomargin'}>
                                    <h4>Info</h4>
                                    <p className="lead">
                                        { this.state.customMessage != '' ? this.state.customMessage :
                                            'Results have been successfully submitted!'}</p>
                                </div>
                            </Modal.Body>
                        </Modal>

                    }


                    {
                        /* Todo - refactor this logic used across the system into mixin*/
                        <Modal show={this.state.failure} onHide={this.close}>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    {'Comments'}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="alert alert-danger fade in nomargin" >
                                    <h4>Error</h4>
                                    <p className="lead">
                                        { this.state.customMessage != '' ? this.state.customMessage :
                                        'There was a problem submitting results'}</p>
                                </div>
                            </Modal.Body>
                        </Modal>

                    }

                </div>
            </div>
        )
    },

    componentDidMount: function () {
        this.populateInitialData();
    },

    populateInitialData: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getClassesForTeacherOrAdmin();
    },

    submitResultsForFinalVetting: function () {
        Actions.submitResultsForFinalVetting(ReactDOM.findDOMNode(this.refs.clazz).value,
            ReactDOM.findDOMNode(this.refs.session).value, ReactDOM.findDOMNode(this.refs.term).value);
    },

    onSubmitResultsForFinalVettingNotification: function () {

        var success = ClassResultStore.response.status == 'success';
        if (success) {
            this.setState({success: true, teacherCommentsSaved: true, customMessage: ClassResultStore.response.message});
        } else {
            this.setState({failure: true, customMessage: ClassResultStore.response.message});
        }
    },

    close: function () {
        this.setState({success: false, failure: false});
    },
});

ReactDOM.render(<SubmitResult />, document.getElementById('submit-class-results'));


