package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.payment.PaymentItem;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by Eze on 08/03/2016.
 */


public interface PaymentItemRepository extends MongoRepository<PaymentItem, String>{


 PaymentItem findOne(String paymentId);
 List<PaymentItem> findAll();
    List<PaymentItem> findByClassesInAndSchoolId(String clazz,String schoolId);
    //For test
    void deleteAll();
}
