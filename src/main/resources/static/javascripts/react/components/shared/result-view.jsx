var React = require('react');

var ResultViewItem = React.createClass({

    getInitialState: function () {
        return {
            inEditMode: false,
            item: {}
        }
    },

    componentWillReceiveProps: function (nextProps) {
        console.log("<>" + JSON.stringify(nextProps));
        this.setState({item: nextProps});
        // this.setState(nextProps);
    },

    render: function () {
        return (
            !this.state.inEditMode ?
                <td className="text-left" key={this.props.row.key}
                    >{this.props.row.value}</td>
                :
                <td className="text-left" key={this.props.row.key}>
                        <input type="text" defaultValue={this.props.row.value} className="col-sm-2"
                               onChange={this.updateScore} value={this.state.item.score ? this.state.item.score : ''} />
                </td>
        )
    },

    toggleEditMode: function () {
        this.setState({inEditMode: !this.state.inEditMode});
    },

    updateScore: function (e) {
        var newItem = this.state.item;
        newItem.score = e.target.value;
        newItem.id = this.props.row.key;
        this.setState({item: newItem});
        // todo - this should add to an array - this.props.updateScore(this.state.item.studentId, e.target.value);
    }

});

module.exports = React.createClass({
    getInitialState: function () {
        return {
            result: this.props.result
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState(nextProps);
    },

    render: function () {

        var first = true;

        var columns = this.state.result.map(row => {
            if (first) {
                first = false;
            }

            return <ResultViewItem row={row} key={row.key}/>
        });

        return (
            <tr key={this.state.result[0].key}>
                {columns}
            </tr>
        )
    }
});