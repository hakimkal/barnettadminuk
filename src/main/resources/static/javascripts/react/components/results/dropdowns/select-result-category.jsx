var React = require('react')
var Reflux = require('reflux')
var ResultCategoryStore = require('../../stores/result-category-store');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(ResultCategoryStore, 'onUpdateResultCategories')
    ],

    getInitialState: function () {
        return {
            resultCategories: []
        }
    },

    render: function () {

        var options = this.state.resultCategories.map(function (option) {
            return <option key={option.id} value={option.name}>{option.name}</option>
        }.bind(this));

        return <select className="form-control">
            <option key={0} value="0">-- Select Result Category --</option>
            {options}
        </select>

    },

    onUpdateResultCategories: function () {
        this.setState({resultCategories: ResultCategoryStore.resultCategories})
    }


});