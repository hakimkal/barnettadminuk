package com.zo.schoolmanager.domain.api;

import java.util.List;

/**
 * Created by ozo on 23/01/2016.
 */
public class ApiResponse {
    private String status;
    private String message;
    private String errorCode;
    private ResponseObject result;
    private List<? extends ResponseObject> results;

    public ApiResponse(String status, String message, String errorCode) {
        this.status = status;
        this.message = message;
        this.errorCode = errorCode;
    }

    public ApiResponse() {}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseObject getResult() {
        return result;
    }

    public void setResult(ResponseObject result) {
        this.result = result;
    }

    public List<? extends ResponseObject> getResults() {
        return results;
    }

    public void setResults(List<? extends ResponseObject> results) {
        this.results = results;
    }
}
