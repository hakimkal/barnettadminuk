var React = require('react');
var BioDataInfo = require('./components/bio-data-info');

module.exports = React.createClass({

   componentDidMount: function() {

       var form = $('#wizard-form');
       form.steps({
           headerTag: 'h3',
           bodyTag: 'div',
           stepsContainerTag: 'div',
           transitionEffect: 'slideLeft',
           onStepChanging: function (event, currentIndex, newIndex) {

               // Always allow previous action even if the current form is not valid!
               if (currentIndex > newIndex) { return true; }

               // Needed in some cases if the user went back (clean up)
               if (currentIndex < newIndex) {
                   // To remove error styles
                   form.find('.body:eq(' + newIndex + ') label.error').remove();
                   form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
               }

               form.validate().settings.ignore = ':disabled,:hidden';
               return form.valid();
           },
           onFinishing: function (event, currentIndex) {
               form.validate().settings.ignore = ':disabled';
               return form.valid();
           },
           onFinished: function (event, currentIndex) {
               alert('Submitted!');
           }
       }).validate({
           highlight: function(element) {
               $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
           },
           success: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           }
       });

   },

   render: function() {
       return (
           <form className="form-horizontal" id="wizard-form" action="#">
                <h3>Bio-Data Info</h3>
                <BioDataInfo />

           </form>
       )
   }

});

React.render(
   <StudentForm />, document.getElementById("form-container")
);