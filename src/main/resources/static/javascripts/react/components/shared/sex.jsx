var React = require('react');

module.exports = React.createClass({

    render: function() {
        return (
            <select className="form-control">
                <option value="">-- Select Gender --</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        )
    }

});