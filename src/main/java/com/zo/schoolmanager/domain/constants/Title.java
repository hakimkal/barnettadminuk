package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 26/11/15.
 */
public enum Title {
    Mr,
    Mrs,
    Dr,
    Ms,
    Chief,
    None
}
