var React = require('react');
var Select = require('react-select');
var TermStore = require('../../stores/term-store');
var Reflux = require('reflux');


module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(TermStore, 'onUpdateTerms')
    ],

    getInitialState: function() {
        return {
            terms : []
        }
    },

    render: function() {
        var options = this.state.terms.map(function(term) {
            return (
                { value: term, label: term }
            )
        }.bind(this));

        return (
            <Select
                name="select-multiple-terms"
                options={options}
                multi
                joinValues={true}
                delimeter=","
                onChange={this.logChange}
            />
        )
    },

    onUpdateTerms: function () {
        this.setState({terms: TermStore.terms});
    },

    // tag is used by the hack - linked to shouldComponentUpdate
    // Essentially there is a bug with react-select where it clears out the value of what you have selected on re-render
    // The approach used here is used to check if actually an update has happened by checking if the value of tags (a custom field)
    // has been updated. I got the idea from - https://www.bountysource.com/issues/27265309-don-t-clear-input-value-after-receiving-new-options
    getDefaultProps: function() {
        return {
            tags: []
        }
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // Using this hack to fix the problem where on re-render the react-select component is clearing out what the user has selected
        return (this.props.tags.length == 0 || nextProps.tags.length !== this.props.tags.length)
    },

    logChange: function(val) {
        // this is used by the hack
        this.props.tags = [];
        this.props.tags.push(val.split(","));
    }
})

