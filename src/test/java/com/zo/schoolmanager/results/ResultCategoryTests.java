package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.repository.ResultCategoryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

/**
 * Created by ozo on 16/12/2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ResultCategoryTests {

    @Autowired
    private ResultCategoryRepository resultCategoryRepository;

    @Test
    public void testCanCreateResultCategory() {

        ResultCategory rc1 = new ResultCategory("Assignment 1", "Assignment 1", "56bc3d80d4c60e72a53806c0", "Assignment1", 10d, "Ass1", 1, true, true);
        ResultCategory rc2 = new ResultCategory("Assignment 2", "Assignment 2", "56bc3d80d4c60e72a53806c0", "Assignment2", 10d, "Ass2", 2, true, true);
        ResultCategory rc3 = new ResultCategory("Assignment 3", "Assignment 3", "56bc3d80d4c60e72a53806c0", "Assignment1", 10d, "Ass3", 3, true, true);
        ResultCategory rc4 = new ResultCategory("Continuous Assessment 1", "Continuous Assessment 1", "56bc3d80d4c60e72a53806c0", "Continuous Assessme nt 1", 10d, "CA1", 4, true, true);
        ResultCategory rc5 = new ResultCategory("Continuous Assessment 2", "Continuous Assessment 2", "56bc3d80d4c60e72a53806c0", "Continuous Assessment 2", 10d, "CA2", 5, true, true);
        ResultCategory rc6 = new ResultCategory("Continuous Assessment 3", "Continuous Assessment 3", "56bc3d80d4c60e72a53806c0", "Continuous Assessment 3", 10d, "CA3", 6, true, true);
        ResultCategory rc7 = new ResultCategory("Examination", "Examination", "56bc3d80d4c60e72a53806c0", "Exam Results Classified here", 60d, "Exam", 7, true, true);

        resultCategoryRepository.save(Arrays.asList(rc1, rc2, rc3, rc4, rc5, rc6, rc7));

        Assert.assertEquals(7, resultCategoryRepository.findAll().size());
    }
}
