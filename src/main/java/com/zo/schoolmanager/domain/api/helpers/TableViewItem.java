package com.zo.schoolmanager.domain.api.helpers;

import com.zo.schoolmanager.domain.api.ResponseObject;

/**
 * Created by ozo on 14/03/2016.
 */
public class TableViewItem implements ResponseObject {

    public TableViewItem() {}

    public TableViewItem(String key, String value) {
        this.key = key;
        this.value = value;
    }

    private String key;
    private String name;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
