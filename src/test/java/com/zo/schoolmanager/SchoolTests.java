package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.service.SchoolService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by ozo on 27/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class SchoolTests extends TestSetup {

    @Autowired
    private SchoolService schoolService;

    @Test
    public void testCanCreateSchool() {
        School school = school();
        School savedSchool = schoolService.save(school);
        Assert.assertNotNull(savedSchool);
        Assert.assertNotNull(savedSchool.getId());
        Assert.assertEquals(savedSchool.getName(), school.getName());

    }
}
