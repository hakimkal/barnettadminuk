var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getStudents: function(clazz, session, term) {
        return Api.get('/students/name-wrappers?' + 'clazz='+clazz+'&session='+session+'&term='+term).then(function(data) {
            this.students = data;
            this.trigger('updateStudentsForClass', data);
        }.bind(this));
    },

    getStudentResultsData: function (studentId, session, term, clazz) {
        return Api.get('/helpers/student-results?' + 'studentId=' + studentId + '&session=' + session +
            '&term=' + term + '&clazz=' + clazz).then(function (data) {
            this.data = data;
            this.trigger('updateStudentResultsData', data);
        }.bind(this));
    },

    saveStudent: function(student) {
        return Api.post('/student-save', student).then(function (data) {
            this.data = data;
            console.log(JSON.stringify(data));
            this.trigger("studentSavedData");
        }.bind(this));
    }
});
