package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 06/09/2016.
 */
public class ResultCategoryNotFoundException extends ConfigurationException {

    public ResultCategoryNotFoundException(String message) {
        super(message);
    }
}
