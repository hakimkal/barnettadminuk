var React = require('react');
var ReactDOM = require('react-dom');
var Actions = require('../actions');
var SessionStore = require('../stores/session-store');
var Reflux = require('reflux');
var SelectDate = require('../shared/select-date');

var Item = React.createClass({

    getInitialState: function() {
        return {
            editMode: false
        }
    },

    render: function() {

        return (
            <tr>
                <td className="lead">{this.props.session.name}</td>

                <td>
                    {   this.state.editMode ?
                            this.props.session.startDate
                        :
                            <SelectDate ref={'startDate'} startDate={this.props.session.startDate}/>
                    }
                </td>
                <td>
                    {   this.state.editMode ?
                            this.props.session.endDate
                        :
                            <SelectDate ref={'endDate'} startDate={this.props.session.endDate}/>
                    }
                </td>


                <td>
                    <button className="btn btn-warning" onClick={this.edit}>Edit</button>
                    <button className="btn btn-danger" onClick={this.save}>Save</button>
                </td>
            </tr>
        )

    },

    edit: function() {
        this.setState({editMode: !this.state.editMode});
    },

    save: function() {
        console.log("save this to back end");
    }
});

var Session = React.createClass({

    getInitialState: function () {
        return {
            sessions: []
        }
    },

    mixins: [
        Reflux.listenTo(SessionStore, "onUpdateSessions")
    ],

    render: function () {

        var rows = this.state.sessions.map(function (session) {
            return <Item session={session} key={session.id} />
        }.bind(this));

        return (

            <div>
                <div>
                    <div className="panel-heading col-md-9">
                        <div className="col-sm-7">
                            <h3>School Sessions</h3>
                        </div>
                    </div>

                    <div className="panel-body col-sm-8">
                        <div className="table-responsive panel">
                            <table className="table table-bordered table-success nomargin">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {rows}
                                </tbody>
                            </table>
                        </div>

                        <p className="col-sm-8 row">
                            <button className="btn btn-success btn-quirk">
                                New Session
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        )
    },

    componentWillMount: function () {
        Actions.getSessions();
    },

    onUpdateSessions: function () {
        this.setState({sessions: SessionStore.sessions});
    }
});

ReactDOM.render(<Session />, document.getElementById("sessions"));