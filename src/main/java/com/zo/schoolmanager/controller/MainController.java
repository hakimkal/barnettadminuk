package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.UserRepository;
import com.zo.schoolmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ozo on 27/11/15.
 */

@Controller
public class MainController extends BaseController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public String login(Model model) {
        model.addAttribute("imageUrl", "bg_shiningstar.jpg");
        return "login";
    }

    @RequestMapping("/index")
    public String index(Model model) {

        userService.updateLastLoggedIn(loggedInUser());

        model.addAttribute("user", loggedInUser());
        model.addAttribute("page", "index");

        return "index";
    }

//    @RequestMapping("/students")
//    public String showStudents(Model model) {
//        model.addAttribute("user", loggedInUser());
//        return "students";
//    }

}
