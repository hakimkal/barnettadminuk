package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 13/04/2016.
 */
public class NoSchoolSetException extends ConfigurationException {

    public NoSchoolSetException(String message) {
        super(message);
    }
}
