package com.zo.schoolmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ozo on 11/04/2016.
 */

@Controller
public class AdminController extends BaseController {

    private static final String ADMIN_PAGE = "admin";


    @RequestMapping("/admin/sessions")
    public String showSession(Model model) {

        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());

        return "admin/sessions";

    }
    @RequestMapping("/admin/sessions/view")
    public String showSessionView(Model model){
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());
        return "admin/sessionsView";
    }

    @RequestMapping("/admin/students")
    public String showStudent(Model model){
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());
        return "admin/students";
    }

    @RequestMapping("/admin/classes")
    public String showClass(Model model){
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());
        return "admin/classes";
    }

    @RequestMapping("/admin/subjects")
    public String showSubjects(Model model){
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());
        return "admin/subjects";
    }

    @RequestMapping("/admin/financials")
    public String showFinancials(Model model){
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());
        return "admin/financials";
    }

    @RequestMapping("/admin/teachers")
    public String showTeachers(Model model) {

        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());

        return "admin/teachers";

    }

    @RequestMapping("/admin/new-class")
    public String showNewClass(Model model) {
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());

        return "admin/class/new-class";
    }

    @RequestMapping("/admin/new-teacher")
    public String showNewTeacher(Model model) {
        model.addAttribute("page", ADMIN_PAGE);
        model.addAttribute("user", loggedInUser());

        return "admin/teacher/new-teacher";
    }





}
