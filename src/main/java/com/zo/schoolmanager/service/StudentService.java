package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.results.StudentResultBasicDTO;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.repository.StudentResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 18/03/2016.
 */

@Service
public class StudentService {

    @Autowired
    private StudentResultRepository studentResultRepository;

    public List<StudentResultBasicDTO> getStudentResultBasicDTOs(List<Student> students, String session, String term) {

        List<StudentResultBasicDTO> studentResultBasicDTOs =
                students.stream()
                        .map(s ->  {
                            StudentResult studentResult = studentResultRepository.findOneByStudentIdAndSessionAndTerm(s.getId(), session, term);
                            if (studentResult != null && studentResult.getStatus() != null) {
                                return new StudentResultBasicDTO(s.getId(), s.getSurname() + " " +  s.getFirstName() + " " +
                                        (s.getMiddleName() != null ? s.getMiddleName() : ""),
                                        studentResult.getStatus().toString());
                            } else {
                                return new StudentResultBasicDTO(s.getId(), s.getSurname() + " " +  s.getFirstName() + " " +
                                        (s.getMiddleName() != null ? s.getMiddleName() : ""),
                                        "NONE");
                            }
                        })
                .collect(Collectors.toList());

        if (studentResultBasicDTOs != null) {
            return studentResultBasicDTOs;
        }

        return new ArrayList<>();
    }
}
