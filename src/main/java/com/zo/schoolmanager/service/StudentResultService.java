package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.repository.ClassResultRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentResultRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 17/12/2015.
 */

@Service
public class StudentResultService {

    @Autowired
    private ClassResultRepository classResultRepository;

    @Autowired
    private StudentResultRepository studentResultRepository;

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    private final Logger LOG = LoggerFactory.getLogger(StudentResultService.class);



    public StudentResult findStudentResults(String studentId, String session, String term, String schoolId) {

        StudentResult studentResult = studentResultRepository.findOneByStudentIdAndSessionAndTerm(studentId, session, term);

        if (studentResult != null) {
            studentResult.setStudentSubjectResults(findStudentSubjectResults(studentId, session, term, schoolId));
        } else {
            LOG.warn("StudentResult not found with: " + studentId + ", " + session + ", " + term);
        }

        return studentResult;
    }

    public List<StudentSubjectResult> findStudentSubjectResults(String studentId, String session, String term, String schoolId) {
        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository.findByStudentIdAndSessionAndTermAndSchoolId(studentId, session, term, schoolId);

        return studentSubjectResults != null ? studentSubjectResults : new ArrayList<>();
    }



}
