package com.zo.schoolmanager.domain.results;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ozo on 09/08/2016.
 */
public class PrintableStudentsResult {

    private List<StudentResult> studentResults;
    private String startSession;
    private String endSession;
    private LocalDateTime created;
    private String resumptionDate; // Formatted Nicely
    private String clazz;
    private String schoolName;
    private String logoURL;
    private String addressLine1;
    private String addressLine2;
    private String city;


    public List<StudentResult> getStudentResults() {
        return studentResults;
    }

    public void setStudentResults(List<StudentResult> studentResults) {
        this.studentResults = studentResults;
    }

    public String getStartSession() {
        return startSession;
    }

    public void setStartSession(String startSession) {
        this.startSession = startSession;
    }

    public String getEndSession() {
        return endSession;
    }

    public void setEndSession(String endSession) {
        this.endSession = endSession;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getResumptionDate() {
        return resumptionDate;
    }

    public void setResumptionDate(String resumptionDate) {
        this.resumptionDate = resumptionDate;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
