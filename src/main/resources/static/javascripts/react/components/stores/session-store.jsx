var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getSessions: function() {
        return Api.get('/sessions').then(function(data) {
            this.sessions = data;
            this.trigger('updateSessions');
        }.bind(this));
    }

});