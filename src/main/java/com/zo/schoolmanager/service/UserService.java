package com.zo.schoolmanager.service;

import com.zo.schoolmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zo.schoolmanager.domain.users.User;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ozo on 17/03/2016.
 */

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void updateLastLoggedIn(User user) {
        User userToSave = userRepository.findOne(user.getId());
        userToSave.setLastLoggedIn(LocalDateTime.now());
        userRepository.save(userToSave);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public List<User> save(List<User> users) {
        return userRepository.save(users);
    }

}
