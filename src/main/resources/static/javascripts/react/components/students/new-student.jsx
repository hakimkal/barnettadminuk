var React = require('react');
var ReactDOM = require('react-dom');
var Form = require("../shared/form");
var Actions = require("../actions");
var StudentStore = require("../stores/student-store");
var Reflux = require('reflux');
var Modal = require('react-bootstrap/lib/Modal');

var firstColumn = [
    {type: "string", title: "Surname", validation: {required: true}},
    {type: "string", title: "First Name", validation: {required: true}},
    {type: "string", title: "Middle Name"},
    {type: "sex", title: "Sex", validation: {required: true}},
    {type: "date", title: "Date of Birth"}
];

var secondColumn = [
    {type: "country", title: "Country"},
    {type: "string", title: "State"},
    {type: "text-area", title: "Address", validation: {required: true}},
    {type: "select-class", title: "Class", validation: {required: true}}
];

var NewStudent = React.createClass({

    getInitialState: function() {
        return {
            success: false
        }
    },

    mixins: [
        Reflux.listenTo(StudentStore, 'onStudentSavedData')
    ],

    render: function () {
        return <div>
            <Form firstColumn={firstColumn}
                  secondColumn={secondColumn}
                  title={'Student'}
                  onSubmit={this.action}
            />

            <Modal show={this.state.success} onHide={this.closeSuccess}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Student'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-info fade in nomargin">
                        <h4>Info</h4>
                        <p className="lead">{'Student Saved Successfully!'}</p>
                    </div>
                </Modal.Body>
            </Modal>

            <Modal show={this.state.failure} onHide={this.closeFailure}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Student'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-danger fade in nomargin">
                        <h4>Error</h4>
                        <p className="lead">{'There was a problem saving student'}</p>
                    </div>
                </Modal.Body>
            </Modal>

        </div>
    },

    action: function (data) {
        console.log('Action: ' + JSON.stringify(data));
        Actions.saveStudent(data);

    },

    onStudentSavedData: function () {

        if (StudentStore.data.status == 'success') {
            this.setState({success: true});
        } else {
            this.setState({failure: true});
        }
    },

    closeSuccess: function () {
        this.setState({success: false});
        window.location.href = '/students/new-student';
    },

    closeFailure: function () {
        this.setState({failure: false});
    },


});


ReactDOM.render(<NewStudent />, document.getElementById('new-student'));
