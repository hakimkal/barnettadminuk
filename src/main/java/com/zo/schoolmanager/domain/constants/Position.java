package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 26/11/15.
 */
public enum Position {
    NONE,
    CLASS_TEACHER,
    SUBJECT_TEACHER,
    HEAD_OF_SCHOOL,
    CLASS_AND_SUBJECT_TEACHER
}
