package com.zo.schoolmanager.finance;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.payment.PaymentItem;
import com.zo.schoolmanager.domain.payment.PaymentRecord;
import com.zo.schoolmanager.repository.PaymentItemRepository;
import com.zo.schoolmanager.repository.PaymentRecordRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.service.PaymentService;
import com.zo.schoolmanager.service.SchoolService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eze on 15/03/2016.
 */



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class PaymentServiceTest {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    PaymentRecordRepository paymentRecordRepository;
    @Autowired
    PaymentItemRepository paymentItemRepository;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private SchoolService schoolService;

    private Student student1;
    private Student student2;
    private Student student3;
    private Student student4;
    private Student student5;
    private Student student6;
    private Student student7;
    private Student student8;
    private Student savedStudent1;
    private Student savedStudent2;
    private Student savedStudent3;
    private Student savedStudent4;
    private Student savedStudent5;
    private Student savedStudent6;
    private Student savedStudent7;
    private Student savedStudent8;
    private PaymentItem paymentItem1;
    private PaymentItem paymentItem2;
    private PaymentItem paymentItem3;
    private PaymentItem paymentItem4;
    private PaymentItem paymentItem5;
    private PaymentItem savedPaymentItem1;
    private PaymentItem savedPaymentItem2;
    private PaymentItem savedPaymentItem3;
    private PaymentItem savedPaymentItem4;
    private PaymentItem savedPaymentItem5;
    private PaymentRecord savedPaymentRecord1;
    private PaymentRecord savedPaymentRecord2;
    private PaymentRecord savedPaymentRecord3;
    private PaymentRecord savedPaymentRecord4;
    private PaymentRecord savedPaymentRecord5;
    private PaymentRecord savedPaymentRecord6;
    private PaymentRecord savedPaymentRecord7;
    private PaymentRecord savedPaymentRecord8;
    private PaymentRecord savedPaymentRecord9;
    private PaymentRecord savedPaymentRecord10;
    private PaymentRecord savedPaymentRecord11;
    private PaymentRecord savedPaymentRecord12;
    private School school1;
    private School school2;
    private School savedSchool1;
    private School savedSchool2;
    private BigDecimal amount1;
    private BigDecimal amount2;
    private BigDecimal amount3;
    private BigDecimal amount4;
    private BigDecimal amount5;
    private List<String> classes1;
    private List<String> classes2;
    private List<String> classes3;
    private List<String> terms;

    @Before
    public void setUp(){
        school1 = new School();
        school2 = new School();
        savedSchool1 = schoolService.save(school1);
        savedSchool2 = schoolService.save(school2);
        amount1 = new BigDecimal(1000);
        amount2 = new BigDecimal(1500);
        amount3 = new BigDecimal(500);
        amount4 = new BigDecimal(20000);
        amount5 = new BigDecimal(500);
        classes1 = new ArrayList<>();
        classes1.add("classSSS1");
        classes1.add("classSSS2");
        classes1.add("classSSS3");
        classes2 = new ArrayList<>();
        classes2.add("classSSS2");
        classes3 = new ArrayList<>();
        classes3.add("classSSS3");
        terms = new ArrayList<>();
        terms.add("First Term");
        terms.add("Second Term");
        terms.add("Third Term");

        //List of students
        //Students who are registered in "school1".
        student1 = new Student("Abdul", "Koroma", Sex.valueOf("Male"), LocalDate.of(1981, 2, 3), "Togolese", "Togo",
                "5 black lane road, BL7 8BJ", savedSchool1.getId(),"classSSS1");
        savedStudent1 = studentRepository.save(student1);
        student2 = new Student("Uche", "Ogbodo", Sex.valueOf("Male"), LocalDate.of(1980, 6, 3), "Nigerian", "Lagos",
                "136 Agbani road, AW7 5EJ", savedSchool1.getId(),"classSSS2");
        savedStudent2 = studentRepository.save(student2);
        student3 = new Student("Temi", "Tope", Sex.valueOf("Female"), LocalDate.of(1984, 4, 1), "Nigerian", "Kaduna State",
                "95 Gwari avenue, GA9 4KJ", savedSchool1.getId(),"classSSS2");
        savedStudent3 = studentRepository.save(student3);
        student4 = new Student("Karen", "JackMan", Sex.valueOf("Female"), LocalDate.of(1980, 2, 3), "British", "Liverpool",
                "55 Ditton road, SW7 5HJ", savedSchool1.getId(),"classSSS2");
        savedStudent4 = studentRepository.save(student4);
        student5 = new Student("Prince", "Charles", Sex.valueOf("Male"), LocalDate.of(1985, 7, 20), "Cambodia", "Cambodian",
                "5 red tale drive lane, TL7 8QJ", savedSchool1.getId(),"classSSS2");
        savedStudent5 = studentRepository.save(student5);
        //Students who are registered in "school2".
        student6 = new Student("Nkechi", "Apkan", Sex.valueOf("Female"), LocalDate.of(1985, 10, 30), "Congo", "Congolese",
                "78 diamond pit drive, DP5 8FJ", savedSchool2.getId(),"classSSS1");
        savedStudent6 = studentRepository.save(student6);
        student7 = new Student("Onahi", "Odeh", Sex.valueOf("Female"), LocalDate.of(1981, 2, 3), "American", "Alabama",
                "55 new church street, NC7 8ST", savedSchool2.getId(),"classSSS2");
        savedStudent7 = studentRepository.save(student7);
        student8 = new Student("Udemy", "Online", Sex.valueOf("Female"), LocalDate.of(1980, 6, 3), "Nigerian", "Lagos",
                "45 waterside road, WR7 AD9", savedSchool2.getId(),"classSSS3");
        savedStudent8 = studentRepository.save(student8);
        //List of payment items
        paymentItem1 = new PaymentItem("Engineering Mathematics", "New edition mathematics textbook that is suitable for senior secondary school students",
                LocalDate.of(2016,11,25),savedSchool1.getId(),classes1,amount1,"2016/2017",terms);
        savedPaymentItem1 = paymentItemRepository.save(paymentItem1);
        paymentItem2 = new PaymentItem("Science Project", "A science exhibition that shows the level of C02 emission on planet earth",
                LocalDate.of(2017,12,2),savedSchool1.getId(),classes2,amount2,"2016/2017",terms);
        savedPaymentItem2 = paymentItemRepository.save(paymentItem2);
        paymentItem3 = new PaymentItem("Computer class", "A special school project that encourages students to participate in computer training",
                LocalDate.of(2016,8,13),savedSchool1.getId(),classes1,amount3,"2016/2017",terms);
        savedPaymentItem3 = paymentItemRepository.save(paymentItem3);
        paymentItem4 = new PaymentItem("Excursion","A three months excursion to the ancient city in Egypt",LocalDate.of(2017,10,15),savedSchool1.getId(),classes3,amount4,"2017/2018",terms);
        savedPaymentItem4 = paymentItemRepository.save(paymentItem4);
        paymentItem5 = new PaymentItem("Food project", "A project to teach students how to cook healthy food",
                LocalDate.of(2016,9,21),savedSchool2.getId(),classes1,amount5,"2016/2017",terms);
        savedPaymentItem5 = paymentItemRepository.save(paymentItem5);
        //List of payment records for the created students and payment items.
        //List of payment records for paymentItem1(Engineering Maths).
        PaymentRecord paymentRecord1 = new PaymentRecord(savedStudent1.getId(),"Hassan",savedStudent1.getSurname(),"07702746678","564","GTbank","2016/2017","First Term",LocalDate.of(2016,3,18),savedPaymentItem1.getId());
        savedPaymentRecord1 = paymentRecordRepository.save(paymentRecord1);
        PaymentRecord paymentRecord2 = new PaymentRecord(savedStudent2.getId(),"Chioma",savedStudent2.getSurname(),"06602746678","784","Zenith bank","2016/2017","Second Term",LocalDate.of(2016,4,18),savedPaymentItem1.getId());
        savedPaymentRecord2 = paymentRecordRepository.save(paymentRecord2);
        PaymentRecord paymentRecord3 = new PaymentRecord(savedStudent3.getId(),"Ade",savedStudent3.getSurname(),"02202746678","784","Access bank","2016/2017","Second Term",LocalDate.of(2016,6,5),savedPaymentItem1.getId());
        savedPaymentRecord3 = paymentRecordRepository.save(paymentRecord3);
        PaymentRecord paymentRecord4 = new PaymentRecord(savedStudent4.getId(),"Linda",savedStudent4.getSurname(),"08802746678","784","Halifax bank","2016/2017","Second Term",LocalDate.of(2016,9,19),savedPaymentItem1.getId());
        savedPaymentRecord4 = paymentRecordRepository.save(paymentRecord4);
        PaymentRecord paymentRecord5 = new PaymentRecord(savedStudent5.getId(),"George",savedStudent5.getSurname(),"07708945567","398","Barclays bank","2016/2017","Second Term",LocalDate.of(2016,3,10),savedPaymentItem1.getId());
        savedPaymentRecord5 = paymentRecordRepository.save(paymentRecord5);
        //List of payment records for paymentItem2(Science project)
        PaymentRecord paymentRecord6 = new PaymentRecord(savedStudent5.getId(),"George",savedStudent5.getSurname(),"07708945567","398","Barclays bank","2016/2017","Second Term",LocalDate.of(2016,3,10),savedPaymentItem2.getId());
        savedPaymentRecord6 = paymentRecordRepository.save(paymentRecord6);
        //List of payment records for paymentItem3(Computer class)
        //No record
        //List of payment records for paymentItem4(Excursion)
        PaymentRecord paymentRecord7 = new PaymentRecord(savedStudent1.getId(),"Hassan",savedStudent1.getSurname(),"07702746678","564","GTbank","2017/2018","First Term",LocalDate.of(2016,3,18),savedPaymentItem4.getId());
        savedPaymentRecord7 = paymentRecordRepository.save(paymentRecord7);
        PaymentRecord paymentRecord8 = new PaymentRecord(savedStudent2.getId(),"Chioma",savedStudent2.getSurname(),"06602746678","784","Zenith bank","2017/2018","Second Term",LocalDate.of(2016,4,18),savedPaymentItem4.getId());
        savedPaymentRecord8 = paymentRecordRepository.save(paymentRecord8);
        PaymentRecord paymentRecord9 = new PaymentRecord(savedStudent3.getId(),"Ade",savedStudent3.getSurname(),"02202746678","784","Access bank","2017/2018","Second Term",LocalDate.of(2016,6,5),savedPaymentItem4.getId());
        savedPaymentRecord9 = paymentRecordRepository.save(paymentRecord9);
        PaymentRecord paymentRecord10 = new PaymentRecord(savedStudent4.getId(),"Linda",savedStudent4.getSurname(),"08802746678","784","Halifax bank","2017/2018","Second Term",LocalDate.of(2016,9,19),savedPaymentItem4.getId());
        savedPaymentRecord10 = paymentRecordRepository.save(paymentRecord10);
        PaymentRecord paymentRecord11 = new PaymentRecord(savedStudent5.getId(),"George",savedStudent5.getSurname(),"07708945567","398","Barclays bank","2017/2018","Second Term",LocalDate.of(2016,3,10),savedPaymentItem4.getId());
        savedPaymentRecord11 = paymentRecordRepository.save(paymentRecord11);
        //List of payment records for paymentItem5(Food Project)
        PaymentRecord paymentRecord12 = new PaymentRecord(savedStudent6.getId(),"Mercy",savedStudent6.getSurname(),"07708902345","419","Food bank","2016/2017","First Term",LocalDate.of(2016,4,5),savedPaymentItem5.getId());
        savedPaymentRecord12 = paymentRecordRepository.save(paymentRecord12);
    }

    @After
    public void tearDown(){
        studentRepository.deleteAll();
        paymentItemRepository.deleteAll();
        paymentRecordRepository.deleteAll();
    }

    @Test
    public void canFindStudentsWhoHavePaid1(){
        //List of students who have paid for Engineering textbook for the given session and term
        List<Student> paidStudents = paymentService.getStudentsWhoHavePaid(savedPaymentItem1.getId(),"2016/2017","Second Term");
        Assert.assertNotNull(paidStudents);
        Assert.assertTrue(paidStudents.stream().anyMatch(student -> student.getId().equalsIgnoreCase(savedStudent2.getId()) || student.getId().equalsIgnoreCase(savedStudent3.getId())));
        Assert.assertTrue(paidStudents.stream().noneMatch(student -> student.getId().equalsIgnoreCase(savedStudent6.getId()) || student.getId().equalsIgnoreCase(savedStudent7.getId())));
        Assert.assertEquals(paidStudents.size(),4);
    }

    @Test
    public void canFindStudentsWhoHavePaid2(){
        //List of students who have paid for Engineering textbook for the given session and term
        List<Student> paidStudents = paymentService.getStudentsWhoHavePaid(savedPaymentItem1.getId(),"2016/2017","First Term");
        Assert.assertNotNull(paidStudents);
        Assert.assertTrue(paidStudents.stream().allMatch(student -> student.getId().equals(student1.getId())));
        Assert.assertEquals(1, paidStudents.size());
    }

    @Test
    public void canFindStudentsWhoHavePaid3(){
        //List of students who have paid for Science project for the given session and term
        List <Student> paidStudents = paymentService.getStudentsWhoHavePaid(savedPaymentItem2.getId(),"2016/2017","Second Term");
        Assert.assertNotNull(paidStudents);
        Assert.assertTrue(paidStudents.stream().allMatch(student -> student.getId().equals(student5.getId())));
        Assert.assertEquals(1,paidStudents.size());
    }

    @Test
    public void canFindStudentsWhoHaveNotPaid1(){

        //List of students who have not paid for science project
        List<Student> studentsWhoHaveNotPaid = paymentService.getStudentsWhoHaveNotPaid(savedPaymentItem2.getId(),"2016/2017","Second Term","classSSS2",savedSchool1.getId());


        Assert.assertNotNull(studentsWhoHaveNotPaid);
        Assert.assertEquals(3,studentsWhoHaveNotPaid.size());
        Assert.assertTrue(studentsWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equalsIgnoreCase(savedStudent2.getId())));
        Assert.assertTrue(studentsWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equalsIgnoreCase(savedStudent3.getId())));
    }

    @Test
    public void canFindStudentWhoHaveNotPaid2(){
        //List of students who have not paid for computer class
        List<Student> studentWhoHaveNotPaid = paymentService.getStudentsWhoHaveNotPaid(savedPaymentItem3.getId(),"2016/2017","Second Term", "classSSS2",savedSchool1.getId());

        Assert.assertNotNull(studentWhoHaveNotPaid);
        Assert.assertEquals(4, studentWhoHaveNotPaid.size());
        Assert.assertTrue(studentWhoHaveNotPaid.stream().noneMatch(student -> student.getId().equals(student1.getId())));
        Assert.assertTrue(studentWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equals(student2.getId())));
        Assert.assertTrue(studentWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equals(student3.getId())));
        Assert.assertTrue(studentWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equals(student4.getId())));
        Assert.assertTrue(studentWhoHaveNotPaid.stream().anyMatch(student -> student.getId().equals(student5.getId())));
    }


    @Test
    public void canCheckIfStudentHasPaid1(){
        //Check if students have paid for Engineering textbook


        Assert.assertFalse(paymentService.hasStudentPaid(savedStudent1.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
        Assert.assertFalse(paymentService.hasStudentPaid(savedStudent6.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
        Assert.assertTrue(paymentService.hasStudentPaid(savedStudent2.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
        Assert.assertTrue(paymentService.hasStudentPaid(savedStudent3.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
        Assert.assertTrue(paymentService.hasStudentPaid(savedStudent4.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
    }

    @Test
    public void canCheckIfStudentHasPaid2(){
        //Check if students have paid for Engineering textbook
        Assert.assertFalse(paymentService.hasStudentPaid(savedStudent1.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
        Assert.assertFalse(paymentService.hasStudentPaid(savedStudent6.getId(),savedPaymentItem1.getId(),"2016/2017","Second Term"));
    }

    @Test
    public void canAddClassToPaymentItem(){
        String clazz = "classSSS1A";
        paymentService.addClassToPaymentItem(paymentItem1.getId(),clazz);
        paymentService.addClassToPaymentItem(paymentItem3.getId(),clazz);
        Assert.assertTrue(paymentItemRepository.findOne(paymentItem1.getId()).getClasses().contains(clazz));
        Assert.assertTrue(paymentItemRepository.findOne(paymentItem3.getId()).getClasses().contains(clazz));

    }

    @Test
    public void canCheckHowMuchIsPaid(){

        Assert.assertEquals(amount1,paymentService.howMuchIsPaid(savedStudent2.getId(),"2016/2017","Second Term"));
        Assert.assertEquals(amount4,paymentService.howMuchIsPaid(savedStudent2.getId(),"2017/2018","Second Term"));
    }




   @Test
    public void canFindHowMuchIsOwed(){


       Assert.assertEquals(amount2.add(amount3),paymentService.howMuchIsOwed(savedStudent2.getId(),"2016/2017","Second Term",savedSchool1.getId()));
       Assert.assertEquals(amount2.add(amount3),paymentService.howMuchIsOwed(savedStudent3.getId(),"2016/2017","Second Term",savedSchool1.getId()));
       Assert.assertEquals(amount2.add(amount3),paymentService.howMuchIsOwed(savedStudent4.getId(),"2016/2017","Second Term",savedSchool1.getId()));
       Assert.assertEquals(amount1.add(amount3),paymentService.howMuchIsOwed(savedStudent1.getId(),"2016/2017","Second Term",savedSchool1.getId()));
       Assert.assertEquals(amount3,paymentService.howMuchIsOwed(savedStudent5.getId(),"2016/2017","Second Term", savedSchool1.getId()));

    }

    @Test
    public void canGetItemsPaidFor1(){

        Assert.assertEquals(1,paymentService.getItemsPaidFor(savedStudent1.getId(),"2016/2017","First Term").size());
        Assert.assertTrue(paymentService.getItemsPaidFor(savedStudent1.getId(),"2016/2017","First Term").stream().allMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem1.getId())));
        Assert.assertTrue(paymentService.getItemsPaidFor(savedStudent1.getId(),"2017/2018","First Term").stream().anyMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem4.getId())));
        Assert.assertFalse(paymentService.getItemsPaidFor(savedStudent1.getId(),"2016/2017","Second Term").stream().anyMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem2.getId())));
        Assert.assertFalse(paymentService.getItemsPaidFor(savedStudent6.getId(),"2016/2017","Second Term").stream().anyMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem2.getId())));

    }

    @Test
    public void canGetItemsPaidFor2(){
        Assert.assertEquals(2,paymentService.getItemsPaidFor(savedStudent5.getId(),"2016/2017","Second Term").size());
       Assert.assertTrue(paymentService.getItemsPaidFor(savedStudent5.getId(),"2016/2017","Second Term").stream().anyMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem1.getId())));
        Assert.assertTrue(paymentService.getItemsPaidFor(savedStudent5.getId(),"2016/2017","Second Term").stream().anyMatch(paymentItem -> paymentItem.getId().equals(savedPaymentItem2.getId())));
    }


    @Test
    public void canGetStudentPaymentRecords(){

        Assert.assertEquals(2,paymentService.getPaymentRecords(savedStudent5.getId(),"2016/2017","Second Term").size());
        Assert.assertTrue(paymentService.getPaymentRecords(savedStudent5.getId(),"2016/2017","Second Term").stream().anyMatch(paymentRecord -> paymentRecord.getId().equals(savedPaymentRecord5.getId())));
        Assert.assertTrue(paymentService.getPaymentRecords(savedStudent5.getId(),"2016/2017","Second Term").stream().anyMatch(paymentRecord -> paymentRecord.getId().equals(savedPaymentRecord6.getId())));
    }


    @Test
    public void canGetStudentPaymentRecordsWithoutSessionAndTerm(){
        Assert.assertEquals(3,paymentService.getPaymentRecords(savedStudent5.getId()).size());
        Assert.assertTrue(paymentService.getPaymentRecords(savedStudent5.getId()).stream().anyMatch(paymentRecord -> paymentRecord.getId().equals(savedPaymentRecord5.getId())));
        Assert.assertTrue(paymentService.getPaymentRecords(savedStudent5.getId()).stream().anyMatch(paymentRecord -> paymentRecord.getId().equals(savedPaymentRecord6.getId())));
        Assert.assertTrue(paymentService.getPaymentRecords(savedStudent5.getId()).stream().anyMatch(paymentRecord -> paymentRecord.getId().equals(savedPaymentRecord11.getId())));


    }

}
