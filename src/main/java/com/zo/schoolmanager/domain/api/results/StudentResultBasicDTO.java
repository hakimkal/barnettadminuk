package com.zo.schoolmanager.domain.api.results;

/**
 * Created by ozo on 18/03/2016.
 */
public class StudentResultBasicDTO {

    private String studentId;
    private String studentName;
    private String status;

    public StudentResultBasicDTO(String studentId, String studentName, String status) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.status = status;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
