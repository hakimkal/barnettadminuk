package com.zo.schoolmanager.domain.results;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 16/12/2015.
 */

@Document(collection = "student_subject_result")
public class StudentSubjectResult extends Result {

    @Id
    private String id;
    private String studentId;
    private String subject;
    private String subjectId;
    private int order;
    private List<ResultItem> resultItems = new ArrayList<>();
    private ResultStatus status;
    @Transient
    private int position;
    @Transient
    private double total;
    @Transient
    private double average;

    public StudentSubjectResult() {}

    public StudentSubjectResult(String studentId, String session, String term, String clazz, String subjectId, String schoolId) {
        this.studentId = studentId;
        this.subjectId = subjectId;
        this.setSession(session);
        this.setTerm(term);
        this.setClazz(clazz);
        this.setSchoolId(schoolId);
        this.status = ResultStatus.NONE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<ResultItem> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ResultItem> resultItems) {
        this.resultItems = resultItems;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

}
