package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.Clazz;
import com.zo.schoolmanager.repository.ClassRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;

/**
 * Created by ozo on 11/02/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class ClazzTests {

    @Autowired
    private ClassRepository classRepository;

    @Test
    public void testCanCreateClasses() {

        Clazz class1 = new Clazz("BASIC 7A", "56bc3d80d4c60e72a53806c0" );
        Clazz class2 = new Clazz("BASIC 8A", "56bc3d80d4c60e72a53806c0" );
        Clazz class3 = new Clazz("BASIC 9A", "56bc3d80d4c60e72a53806c0" );
        Clazz class4 = new Clazz("SS1A", "56bc3d80d4c60e72a53806c0" );
        Clazz class5 = new Clazz("SS2A", "56bc3d80d4c60e72a53806c0" );

        classRepository.save(Arrays.asList(class1, class2, class3, class4, class5));
        Assert.assertEquals(5, classRepository.findAll().size());

    }

    @Test
    public void testCanFindClasses() {

    }



}
