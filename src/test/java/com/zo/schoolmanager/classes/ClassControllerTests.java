package com.zo.schoolmanager.classes;

import com.zo.schoolmanager.BarnettApplication;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasItems;

/**
 * Created by ozo on 22/01/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@IntegrationTest("server.port:0")
public class ClassControllerTests {

    @Value("${local.server.port}")
    int port;


    @Test
    public void canFetchClasses() {

        when().
                get("/classes").
                then().
                body(hasItems());

    }


}
