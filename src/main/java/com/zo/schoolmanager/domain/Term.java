package com.zo.schoolmanager.domain;

import org.springframework.data.annotation.Transient;

import java.time.LocalDateTime;

/**
 * Created by ozo on 01/03/2016.
 */
public class Term {

    private LocalDateTime starts;
    private LocalDateTime ends;
    private String name;

    @Transient
    private boolean current;

    public Term() {}
    public Term(String name, LocalDateTime starts, LocalDateTime ends) {
        this.name = name;
        this.starts = starts;
        this.ends = ends;
    }

    public LocalDateTime getStarts() {
        return starts;
    }

    public void setStarts(LocalDateTime starts) {
        this.starts = starts;
    }

    public LocalDateTime getEnds() {
        return ends;
    }

    public void setEnds(LocalDateTime ends) {
        this.ends = ends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }
}
