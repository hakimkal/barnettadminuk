var React = require('react');
var ReactDOM = require('react-dom');
var Form = require("../../shared/form");
var Actions = require("../../actions");
var ClassStore = require("../../stores/class-store");
var Reflux = require('reflux');
var Modal = require('react-bootstrap/lib/Modal');

var firstColumn = [
    {type: "string", title: "Class Name", validation: {required: true}},
    {type: "string", title: "Display Name", validation: {required: true}},
    {type: "text-area", title: "Description"}
];

var NewClass = React.createClass({

    getInitialState: function() {
        return {
            success: false
        }
    },

    mixins: [
        Reflux.listenTo(ClassStore, 'onClassSaved')
    ],

    render: function () {
        return <div>
            <Form firstColumn={firstColumn}
                  title={'New Class'}
                  onSubmit={this.action}
            />

            <Modal show={this.state.success} onHide={this.closeSuccess}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Class'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-info fade in nomargin">
                        <h4>Info</h4>
                        <p className="lead">{'Class Saved Successfully!'}</p>
                    </div>
                </Modal.Body>
            </Modal>

            <Modal show={this.state.failure} onHide={this.closeFailure}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Class'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-danger fade in nomargin">
                        <h4>Error</h4>
                        <p className="lead">{'There was a problem saving Class'}</p>
                    </div>
                </Modal.Body>
            </Modal>

        </div>
    },

    action: function (data) {
        console.log('Action: ' + JSON.stringify(data));
        Actions.saveClass(data);

    },

    onClassSaved: function () {

        if (ClassStore.data.status == 'success') {
            this.setState({success: true});
        } else {
            this.setState({failure: true});
        }
    },

    closeSuccess: function () {
        this.setState({success: false});
        window.location.href = '/admin/classes';
    },

    closeFailure: function () {
        this.setState({failure: false});
    },


});


ReactDOM.render(<NewClass />, document.getElementById('new-class'));
