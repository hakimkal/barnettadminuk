package com.zo.schoolmanager.domain.results.calculator;

import com.zo.schoolmanager.commons.exceptions.configuration.ConfigurationException;
import com.zo.schoolmanager.commons.exceptions.configuration.MultipleResultCategoriesException;
import com.zo.schoolmanager.commons.exceptions.configuration.ResultCategoryNotFoundException;
import com.zo.schoolmanager.commons.exceptions.configuration.ResultTemplateException;
import com.zo.schoolmanager.domain.results.*;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.repository.ResultCategoryRepository;
import com.zo.schoolmanager.service.ResultItemService;
import com.zo.schoolmanager.service.SchoolService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ozo on 29/07/2016.
 */
@Component
public class ResultCalculator {

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private ResultItemService resultItemService;

    private Logger LOG = LoggerFactory.getLogger(ResultCalculator.class);

    public PrintableStudentsResult getPrintableResults(String clazz,
                                                       List<StudentResult> studentResults) {

        PrintableStudentsResult printableStudentsResult = new PrintableStudentsResult();

        if (!StringUtils.isEmpty(clazz)) {
            printableStudentsResult.setClazz(clazz);
            printableStudentsResult.setStudentResults(studentResults);
        }

        // todo - set all the other parameters, e.g. logo etc. these should be gotten from the Configuration object

        return printableStudentsResult;

    }

    /**
     * This method is used to populate the averages and totals for StudentResults. These are first computed for
     * StudentSubjectResult objects then subsequently for StudentResult objects
     *
     * @param studentResults
     * @return
     */
    public List<StudentResult> computeTotalsAndAverages(List<StudentResult> studentResults) {

        // Set totals and averages for each StudentSubject Result for each StudentResult for each Student
        studentResults.forEach(sr -> sr.getStudentSubjectResults().forEach(ssr -> {
            ssr.setTotal(ssr.getResultItems().stream().collect(Collectors.summingDouble(ResultItem::getScore)));
            ssr.setAverage(ssr.getResultItems().stream().collect(Collectors.averagingDouble(ResultItem::getScore)));
        }));

        // Set totals and averages for each StudentResult for each Student
        studentResults.forEach(sr -> {
            sr.setTotal(sr.getStudentSubjectResults().stream().collect(Collectors.summingDouble(StudentSubjectResult::getTotal)));
            sr.setAverage(sr.getStudentSubjectResults().stream().collect(Collectors.averagingDouble(StudentSubjectResult::getAverage)));
        });

        return studentResults;
    }

    /**
     * This method is used to compute the results for each result category based on what is defined in the ResultCompositions
     * of the ResultTemplate for that School. ResultComposition is used to store the percentage of any ResultCategory, and is stored
     * as a percentage, e.g. 20, 40, etc. We reassign the value of ResultItem for each ResultItem in StudentSubjectResult. This makes
     * it a lot easier for us to manage the generation of PrintableStudentResult as we are using the same ResultItem which is used to
     * keep track of scores entered by teachers, and we don't have to create a new wrapper class just to present these computed values
     * One can argue against this approach and suggest that we use wrapper classes instead, but I used that in the first version of
     * the SchoolManager App and it wasn't pretty
     *
     * @param studentResults
     * @param resultCompositions
     * @return
     */
    public List<StudentResult> computeResultItemScoresBasedOnResultComposition(List<StudentResult> studentResults,
                                                                               Set<ResultComposition> resultCompositions) {

        // todo - needs implementation - applying student specific result composition

        // Get ResultComposition that are specific to Specific Students
//        List<ResultComposition> resultCompositionForSpecificStudents =
//                resultCompositions.stream()
//                        .filter(rc -> !CollectionUtils.isEmpty(rc.getStudentIds())).collect(Collectors.toList());

        // Get ResultComposition that are specific to all Classes
//        List<ResultComposition> resultCompositionForClasses =
//                resultCompositions.stream()
//                        .filter(rc -> !CollectionUtils.isEmpty(rc.getClasses())).collect(Collectors.toList());
//

        studentResults.forEach(sr ->
                sr.getStudentSubjectResults().forEach(ssr ->
                        ssr.getResultItems()
                                .forEach(ri ->
                                        resultCompositions.forEach(rc -> {
                                            if (ri.getResultCategory().equals(rc.getResultCategory())) {
                                                ri.setScore(rc.getPercent() * ri.getScore() / ri.getMaxScore());
                                            }
                                        })
                                )));

        return studentResults;

    }

    /**
     * Student Results have StudentSubjectResults, and StudentSubjectResults have ResultItems
     * ResultItems each have a resultCategory and parentResultCategory.
     * In preparing results we need all ResultItems to be at the top resultCategory level - parentResultCategory
     * So we need to group by ParentResultCategory, and then sum up the scores of the subcategory ResultItems
     * SubCategory ResultItems are ResultItems that have a ResultCategory with a different ParentResultCategory
     * <p>
     * The logic here is doing a lot, albeit with the help of Java 8 Streams the code block is succinct
     *
     * @param studentResults
     * @return
     */
    public List<StudentResult> normaliseSubCategoryResults(List<StudentResult> studentResults, String schoolId) {


        studentResults.forEach(r ->
                r.getStudentSubjectResults().forEach(ssr -> {
                    List<ResultItem> normalisedResultItems =
                            resultItemService.normaliseResultItems(ssr.getResultItems(), schoolId, ssr.getClazz());
                    ssr.setResultItems(normalisedResultItems);
                }));

        return studentResults;

    }
}
