var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

var baseUrl = '/api/classes';

module.exports = Reflux.createStore({

    listenables: [Actions],

    getClassesTaught: function(subjectId) {
        return Api.get(baseUrl + '/user/subject/' + subjectId).then(function(data) {
            this.classes = data;
            this.trigger('updateClasses');
        }.bind(this));
    },

    getAllClasses: function() {
        return Api.get(baseUrl).then(function(data) {
            this.classes = data;
            this.trigger('updateClass');
        }.bind(this));
    },

    getClassesForTeacherOrAdmin: function() {
        return Api.get(baseUrl + '/user').then(function(data) {
            this.classes = data;
            this.trigger('updateClassesForTeacherOrAdmin');
        }.bind(this));
    },

    saveClass: function(clazz) {
        console.log("triggered");
        return Api.post(baseUrl, clazz).then(function(data) {
            this.data = data;
            this.trigger('classSaved');
        }.bind(this));
    }

});