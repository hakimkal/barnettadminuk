var React = require('react');
var ReactDOM = require('react-dom');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var SelectClass = require('./dropdowns/select-class');
var Actions = require('../actions');
var Reflux = require('reflux');
var StudentStore = require('../stores/student-store');
var Tooltip = require('react-bootstrap/lib/Tooltip');
var OverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');

var ViewClassResults = React.createClass({

    mixins: [
        Reflux.listenTo(StudentStore, 'onUpdateStudentsForClass')
    ],

    getInitialState: function () {
        return {
            students: []
        }
    },

    render: function () {
        var students = this.state.students.map(function(student) {
            return <tr key={student.studentId}>
                <td className="lead">{student.studentName}</td>
                <td>
                    <button className="btn btn-success btn-quirk" onClick={() => this.viewStudentResult(student.studentId)}>
                        Enter Comments
                    </button>
                </td>
                <td className="text-center">
                    {this.getStatusLabel(student.status, student.studentId)}
                </td>
            </tr>
        }.bind(this));

        return (
            <div className="col-md-12">
                <div className="panel">
                    <div className="panel-heading">
                        <h3>View Class Results</h3>
                    </div>

                    <div className="panel-body">
                        <div className="form-group col-md-12">
                            <p className="lead">
                                Select Class to View Results
                            </p>
                        </div>

                        <div className="form-group col-md-10">
                            <div className="col-sm-2 form-group">
                                <SelectSession ref="session"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectTerm ref="term"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectClass ref="clazz"/>
                            </div>

                            <button className="btn btn-primary btn-quirk" onClick={this.getClassResults}>
                                View Students
                            </button>
                        </div>

                        <div className="form-group col-md-9">
                            <div className="table-responsive">
                                <table className="table table-bordered table-success nomargin">
                                    <thead>
                                    <tr>
                                        <th>Student</th>
                                        <th>Action</th>
                                        <th className="text-center">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {students}
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },

    componentDidMount: function () {
        this.populateInitialData();
    },

    populateInitialData: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getClassesForTeacherOrAdmin();
    },

    onUpdateStudentsForClass: function () {
        this.setState({students: StudentStore.students});
    },

    getClassResults: function () {
        if (ReactDOM.findDOMNode(this.refs.session).value !== null && ReactDOM.findDOMNode(this.refs.term).value !== null &&
            ReactDOM.findDOMNode(this.refs.clazz).value !== '') {

            Actions.getStudents(ReactDOM.findDOMNode(this.refs.clazz).value, ReactDOM.findDOMNode(this.refs.session).value,
                ReactDOM.findDOMNode(this.refs.term).value);
        }

    },

    getStatusLabel: function(status, id) {
        switch (status) {
            case undefined:
                return this.getToolTip('No Status set for this Result', id, "btn btn-danger btn-icon btn-stroke", "fa fa-exclamation");

            case '':
                return this.getToolTip('No Status set for this Result', id, "btn btn-danger btn-icon btn-stroke", "fa fa-exclamation");

            case 'CLASS_TEACHER_COMMENTED':
                return this.getToolTip('You have entered comments', id, "btn btn-success btn-icon btn-stroke", "fa fa-check-square");

            case 'CLASS_TEACHER_SUBMITTED':
                return this.getToolTip('You have entered submitted results and comments cannot be edited again. However you can view the result', id,
                    "btn btn-danger btn-icon btn-stroke", "fa fa-exclamation-circle");

            case 'CLASS_TEACHER_VETTED':
                return this.getToolTip('You have vetted this result and can no longer edit your comments. However you can view the result', id,
                    "btn btn-danger btn-icon btn-stroke", "fa fa-exclamation-circle");

            case 'NONE':
                return this.getToolTip('No Comments have been entered', id,
                    "btn btn-default btn-icon btn-stroke", "fa fa-minus-circle");
            default:
                return this.getToolTip('There is a strange problem, contact your Administrator', id,
                    "btn btn-danger btn-icon btn-stroke", "fa fa-exclamation-triangle");
        }
    },

    getToolTip: function(message, id, divClass, iconClass) {
        var toolTip =  <Tooltip placement="top" className="in" id={id}>
            {message}
        </Tooltip>;

        return <div>
            <OverlayTrigger placement="top" overlay={toolTip}>
                <button className={divClass}><i className={iconClass}></i>
                </button>
            </OverlayTrigger>
        </div>
    },

    viewStudentResult: function(studentId) {
        window.location.href= "/results/view-student-result?" + 'studentId='+studentId +
            '&session='+ReactDOM.findDOMNode(this.refs.session).value +
            '&term='+ReactDOM.findDOMNode(this.refs.term).value + '&clazz='+ ReactDOM.findDOMNode(this.refs.clazz).value;
    }
});


ReactDOM.render(<ViewClassResults />, document.getElementById("view-class-results"));