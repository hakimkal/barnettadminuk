var React = require('react')

module.exports = React.createClass({

    render: function() {
        return (
            <nav className="sidebar" data-sidebar-anyclick-close="">
            <ul className="nav">
            <li className="active">
            <a data-toggle="collapse" title="Dashboard" href="#dashboard" className="" aria-expanded="true">
            <em className="icon-home"></em>
            <span data-localize="sidebar.nav.DASHBOARD">My Area</span>
        </a>
        <ul className="nav sidebar-subnav collapse in" id="dashboard" aria-expanded="true" styles="">
            <li className="sidebar-subnav-header"></li>
            <li className=" active">
            <a title="My Items" href="/my_area">
            <span>My Items</span>
        </a>
        </li>
        <li className=" ">
            <a title="Lost Items" href="/my_area/lost-items">
            <span>Lost Items</span>
        </a>
        </li>
        <li className=" ">
            <a title="Payments" href="#">
            <span>Payments</span>
            </a>
            </li>
            </ul>
            </li>
            </ul>
            </nav>
        );
    }
});

React.render(
<SideSection />,
    document.getElementById('sidebar_sec')
);