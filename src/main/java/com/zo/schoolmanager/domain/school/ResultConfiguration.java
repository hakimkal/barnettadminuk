package com.zo.schoolmanager.domain.school;

import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.domain.school.workflow.ResultFlow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 30/07/2016.
 */
public class ResultConfiguration implements Configuration {

    private List<ResultTemplate> resultTemplates = new ArrayList<>();
    private List<ResultFlow> resultFlows = new ArrayList<>();

    public List<ResultTemplate> getResultTemplates() {
        return resultTemplates;
    }

    public void setResultTemplates(List<ResultTemplate> resultTemplates) {
        this.resultTemplates = resultTemplates;
    }

    public List<ResultFlow> getResultFlows() {
        return resultFlows;
    }

    public void setResultFlows(List<ResultFlow> resultFlows) {
        this.resultFlows = resultFlows;
    }
}
