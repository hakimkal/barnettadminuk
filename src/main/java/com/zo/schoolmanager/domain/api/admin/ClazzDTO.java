package com.zo.schoolmanager.domain.api.admin;

/**
 * Created by ozo on 04/07/2016.
 */
public class ClazzDTO {

    private String className;
    private String displayName;
    private String description;


    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
