var React = require('react');



module.exports = React.createClass({

    render : function(){
        return <div className= {this.props.page } >
                <div className="panel">
                    <a href={this.props.href}>
                        <div className="panel-heading">
                            <h4 className="panel-title">{this.props.title}</h4>
                        </div>
                        <div className="panel-body">
                            <div className="page-icon"><i className={this.props.icon}></i></div>
                        </div>
                    </a>
                </div>
            </div>
    }

});
