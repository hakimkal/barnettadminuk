var React = require('react');
var ReactDOM = require('react-dom');
var Actions = require('../actions')
var ResultItemList = require('./resultitems/result-item-list');
var FilterResult = require('./filter-results');

var EnterResults = React.createClass({

    getInitialState: function() {
        return {
            filter: null
        }
    },

    render: function () {
        return <div>
            <FilterResult filter={this.filter}/>
            <ResultItemList updateScores={this.updateScores} saveResultItems={this.saveResultItems} />
        </div>
    },

    filter: function (session, term, clazz, subject, resultCategory) {

        var filter = {
            'session': session,
            'term': term,
            'clazz': clazz,
            'subject': subject,
            'resultCategory': resultCategory
        };

        this.setState({filter:filter});

        Actions.getResultItems(filter);
    },


    saveResultItems: function(resultItems) {

        Actions.saveResultItems({
            filter: this.state.filter,
            resultItems: resultItems
        });

    }

});


ReactDOM.render(
    <EnterResults />,
    document.getElementById('enter-results')
);
