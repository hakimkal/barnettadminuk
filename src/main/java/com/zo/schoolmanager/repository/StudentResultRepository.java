package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.results.StudentResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ozo on 17/12/2015.
 */
public interface StudentResultRepository extends MongoRepository<StudentResult, String> {
    StudentResult findOneByStudentIdAndSessionAndTerm(String studentId, String session, String term);
    List<StudentResult> findBySchoolIdAndClazzAndSessionAndTerm(String schoolId, String clazz, String session, String term);
}
