package com.zo.schoolmanager.api;

import com.zo.schoolmanager.controller.BaseController;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 01/02/2016.
 */

@RestController
public class ResultCategoryApiController extends BaseController {

    @Autowired
    private SchoolService schoolService;

    @RequestMapping("/result-categories/{clazz}")
    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN", "ROLE_ADMIN"})
    public List<ResultCategory> getResultCategories(@PathVariable String clazz) {

        User user = loggedInUser();

        // No class selected so default parameter is 0
        if (clazz.equals("0")) {
            return new ArrayList<>();

        } else {
            School school = schoolService.getSchool(user.getSchoolId());

            List<ResultCategory> resultCategories = schoolService.getResultCategories(school.getId(), clazz);
            resultCategories.sort((r1, r2) -> Integer.valueOf(r1.getOrder()).compareTo(r2.getOrder()));

            return resultCategories != null ? resultCategories : new ArrayList<>();
        }



    }
}
