package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 06/09/2016.
 */
public class NoResultCategoryException extends ConfigurationException {
    public NoResultCategoryException(String message) {
        super(message);
    }
}
