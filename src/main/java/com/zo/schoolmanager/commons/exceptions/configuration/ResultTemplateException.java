package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 06/09/2016.
 */
public class ResultTemplateException extends ConfigurationException {

    public ResultTemplateException(String message) {
        super(message);
    }
}
