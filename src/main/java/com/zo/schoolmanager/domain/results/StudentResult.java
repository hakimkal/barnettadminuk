package com.zo.schoolmanager.domain.results;

import com.zo.schoolmanager.domain.Student;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ozo on 17/12/2015.
 */

@Document(collection = "student_results")
public class StudentResult extends Result {

    @Id
    private String id;
    private String studentId;
    private ResultStatus status;
    private String teacherComments = "";
    private String principalComments = "";
    private String parentComments = "";
    private String studentComments = "";
    @Transient
    private Student student;
    @Transient
    private List<StudentSubjectResult> studentSubjectResults;
    @Transient
    private int position;
    @Transient
    private double total;
    @Transient
    private double average;

    public StudentResult() {}

    public StudentResult(String studentId, String session, String term, String className, String schoolId) {
        this.studentId = studentId;
        this.setSession(session);
        this.setTerm(term);
        this.setClazz(className);
        this.setSchoolId(schoolId);
        this.status = ResultStatus.NONE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public String getTeacherComments() {
        return teacherComments;
    }

    public void setTeacherComments(String teacherComments) {
        this.teacherComments = teacherComments;
    }

    public String getPrincipalComments() {
        return principalComments;
    }

    public void setPrincipalComments(String principalComments) {
        this.principalComments = principalComments;
    }

    public String getParentComments() {
        return parentComments;
    }

    public void setParentComments(String parentComments) {
        this.parentComments = parentComments;
    }

    public String getStudentComments() {
        return studentComments;
    }

    public void setStudentComments(String studentComments) {
        this.studentComments = studentComments;
    }

    public List<StudentSubjectResult> getStudentSubjectResults() {
        return studentSubjectResults;
    }

    public void setStudentSubjectResults(List<StudentSubjectResult> studentSubjectResults) {
        this.studentSubjectResults = studentSubjectResults;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
