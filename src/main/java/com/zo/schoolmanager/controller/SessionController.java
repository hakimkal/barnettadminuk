package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.Session;
import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.SessionRepository;
import com.zo.schoolmanager.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 25/01/2016.
 */

@Controller
public class SessionController extends BaseController {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private SessionRepository sessionRepository;

    @ResponseBody
    @RequestMapping("/sessions")
    public List<String> getSessions() {

        List<Session> sessions = new ArrayList<>();

        User user = loggedInUser();

        sessions = sessionRepository.getSessionBySchoolId(user.getSchoolId());

        return sessions.stream().map(s -> s.getName()).collect(Collectors.toList());

    }

    @ResponseBody
    @RequestMapping("/sessions/terms")
    public List<String> getTerms() {
        return Arrays.asList("First Term", "Second Term", "Third Term");
    }

}
