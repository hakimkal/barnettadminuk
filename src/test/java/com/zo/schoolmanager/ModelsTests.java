package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ozo on 05/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ModelsTests extends TestSetup {

    // Parent Tests

    Parent p1, p2, p3;
    Student s1, s2, s3;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void testCanCreateParent() {
        Parent parent = new Parent();
        Assert.assertNotNull(parent);
    }

    @Test
    public void testParentCanSetFirstName() {
        Parent parent = new Parent();
        parent.setFirstName("Frankie");
        Assert.assertEquals(parent.getFirstName(), "Frankie");
    }

    @Test
    public void testParentCanSetLastName() {
        Parent parent = new Parent();
        parent.setLastName("Emmanuel");
        Assert.assertEquals(parent.getLastName(), "Emmanuel");
    }

    @Test
    public void testSaveBulkParents() {
        p1 = new Parent("Mike", "Lewis", "Joe", "08392938329", "admin12", "1234567");
        p2 = new Parent("Jane", "Oga", "Henry", "08398399029", "admin12", "1234567");
        p3 = new Parent("Henry", "Adah", "Mark", "09099399029", "admin12", "1234567");

        Student student = student();

        student.setParents(Arrays.asList(p1,p2,p3));
        Student savedStudent = studentRepository.save(student);

        Assert.assertEquals(savedStudent.getParents().size(), 3);

    }

    @Test
    public void testCanGetAllParents() {
        clearDB();

        userRepository.save(Arrays.asList(father(), mother(), mother()));
        userRepository.save(Arrays.asList(father(), mother()));
        userRepository.save(Arrays.asList(father1()));

        List<Parent> parents = (List<Parent>) (List<?>) userRepository.findAllByUserType(UserType.PARENT);

        Assert.assertEquals(parents.size(), 6);

    }



}
