package com.zo.schoolmanager.domain.results;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ozo on 01/03/2016.
 */
@Document(collection = "class-subject-result")
public class ClassSubjectResult extends Result {

    @Id
    private String id;
    private ResultStatus status;
    private String className;
    // SubjectId is unique to schools so we don't need to have schoolId as a field
    private String subjectId;

    public ClassSubjectResult() {}

    public ClassSubjectResult(String subjectId, String className, String session, String term, String schoolId) {
        this.subjectId = subjectId;
        this.className = className;
        this.status = ResultStatus.SUBJECT_TEACHER_SUBMITTED;
        this.setSession(session);
        this.setTerm(term);
        this.setSchoolId(schoolId);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }


}
