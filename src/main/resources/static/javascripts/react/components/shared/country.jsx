var React = require('react');

module.exports = React.createClass({

    render: function() {
        return (
            <select className="form-control">
                <option value="Nigeria">Nigeria</option>
                <option value="Other">Other</option>
            </select>
        )
    }

});