package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.results.calculator.ResultComposition;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

/**
 * Created by ozo on 29/07/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ResultTemplateTests {

    @Test
    public void testCanCreateResultTemplate() {
        Set<String> classes = new HashSet<>(Arrays.asList("SS1A", "SS2A", "SS3A"));
        Set<ResultComposition> resultCompositions = new HashSet<>();

        ResultTemplate resultTemplate = new ResultTemplate("schoolId", resultCompositions, "Basic Result Template", classes);

        Assert.assertNotNull(resultTemplate);
    }


}
