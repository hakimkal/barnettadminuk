package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.results.ClassResult;
import com.zo.schoolmanager.domain.results.Result;
import com.zo.schoolmanager.repository.ClassResultRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by ozo on 17/12/2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ClassResultTests {

    @Autowired
    private ClassResultRepository classResultRepository;

    @Test
    public void testCreateClassResult() {
        ClassResult classResult = new ClassResult();
        Assert.assertNotNull(classResult);
    }

    @Test
    public void testCanSaveClassResult() {
        ClassResult classResult = new ClassResult("SS2A", "2015/2016", "First Term", Result.ResultStatus.HOD_VETTED, "schoolId");
        classResultRepository.save(classResult);

        Assert.assertNotNull(classResult);
        Assert.assertNotNull(classResult.getId());

        ClassResult savedClassResult = classResultRepository.findOne(classResult.getId());
        Assert.assertTrue(savedClassResult.getStatus().equals(Result.ResultStatus.HOD_VETTED));
    }

}
