package com.zo.schoolmanager.commons.util.mock;

import com.zo.schoolmanager.domain.*;
import com.zo.schoolmanager.domain.constants.Position;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.Title;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.users.Teacher;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by ozo on 30/07/2016.
 */
public class ModelMocker {

    @Autowired
    private BCryptPasswordEncoder bCryptEncoder;

    /**
     * 1. Create School, session, terms
     *
     * @return school
     */
    public static School createSchool(String schoolId) {
        School school = new School("Ozo School", "44 Acanthus Drive", "Mark", "Wood");
        school.setId(schoolId);

        return school;
    }

    /**
     * 2. Create List of Students in School
     *
     * @param number
     * @param schoolId
     * @return
     */
    public static List<Student> createStudents(String schoolId, int number, List<String> classes) {

        List<String> namePrefixes = Arrays.asList("Smi", "Low", "Har", "Jan", "Can",
                "Lew", "Jon", "Mar", "Het", "Ema", "Hen", "Eme", "Pau", "Pol", "Lon");

        List<Student> students = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Sex sex = Sex.valueOf("Male");
            if (i % 2 != 0) {
                sex = Sex.valueOf("Female");
            }

            Student student = new Student(namePrefixes.get(new Random().nextInt(14)) + RandomStringUtils.random(3, true, false).toLowerCase(),
                    namePrefixes.get(new Random().nextInt(14)) + RandomStringUtils.random(3, true, false).toLowerCase(),
                    sex, LocalDate.of(1983, 2, 3), "Nigerian", "Anambra",
                    "18 School Fields, SE1 5HJ", schoolId);

            student.setClazz(classes.get(new Random().nextInt(classes.size())));
            students.add(student);
        }

        return students;
    }


    public static List<Teacher> createGenericTeachers(String schoolId, int number) {

        List<Teacher> teachers = new ArrayList<>();

        for (int i = 0; i < number; i++) {

            Teacher teacher = new Teacher(Title.Mr, "May" + RandomStringUtils.random(3, true, false).toLowerCase(),
                    "Lin" + RandomStringUtils.random(3, true, false).toLowerCase(),
                    "Wan" + RandomStringUtils.random(2, true, false).toLowerCase(), Sex.Male,
                    "22 Drake Way", Position.NONE, schoolId);

            if (i % 2 != 0) {
                teacher.setTitle(Title.Mrs);
                teacher.setSex(Sex.Female);
            }

            teacher.setPosition(Position.NONE);

            teachers.add(teacher);
        }

        return teachers;
    }

    public static List<Teacher> createClassTeachers(List<Teacher> teachers, List<String> classes) {

        for (int i = 0; i < teachers.size(); i++) {

            teachers.get(i).setClazz(classes.get(i));
            teachers.get(i).setPosition(Position.CLASS_TEACHER);
            teachers.get(i).getRoles().add("ROLE_CLASS_TEACHER");
        }

        return teachers;
    }


    /**
     *
     * @param number
     * @param schoolId
     */
    public static List<Session> createSessions(String schoolId, int number) {

        List<Session> sessions = new ArrayList<>();

        LocalDateTime now = LocalDateTime.now();
        Term thirdTerm = new Term();

        Integer startSession = 2015;

        for (int i = 0; i < number; i++) {

            if (i != 0) {
                now = thirdTerm.getEnds();
                startSession = ++startSession;
            }

            Term firstTerm = new Term("First Term", now, now.plusMonths(4));
            Term secondTerm = new Term("Second Term", firstTerm.getEnds(), firstTerm.getEnds().plusMonths(4));
            thirdTerm = new Term("Third Term", secondTerm.getEnds(), secondTerm.getEnds().plusMonths(4));

            List<Term> terms = Arrays.asList(firstTerm, secondTerm, thirdTerm);

            Session session = new Session(startSession + "/" + ++startSession, schoolId, terms);

            sessions.add(session);
        }

        return sessions;
    }

    /**
     * @param schoolId
     * @return
     */
    public static List<Clazz> createClasses(String schoolId) {

        Clazz basic7a = new Clazz("Basic 7A", schoolId);
        Clazz basic7b = new Clazz("Basic 7B", schoolId);
        Clazz basic8a = new Clazz("Basic 8A", schoolId);
        Clazz basic8b = new Clazz("Basic 8B", schoolId);
        Clazz basic9a = new Clazz("Basic 9A", schoolId);
        Clazz basic9b = new Clazz("Basic 9B", schoolId);

        Clazz ss1a = new Clazz("SS1A", schoolId);
        Clazz ss1b = new Clazz("SS1B", schoolId);
        Clazz ss2a = new Clazz("SS2A", schoolId);
        Clazz ss2b = new Clazz("SS2B", schoolId);
        Clazz ss3a = new Clazz("SS3A", schoolId);
        Clazz ss3b = new Clazz("SS3B", schoolId);

        return Arrays.asList(basic7a, basic8a, basic9a, basic7b, basic8b, basic9b, ss1a, ss1b,
                ss2a, ss2b, ss3a, ss3b);
    }

    /**
     *
     * @param schoolId
     * @param number
     * @return
     */
    public static List<Subject> createSubjects(String schoolId, int number) {

        List subjects;

        List<String> subjectNames = Arrays.asList(
                "Physical and Health Education",
                "English Studies",
                "Basic Science",
                "Computer Science",
                "Music",
                "Social Studies (7)",
                "Typing",
                "Basic Technology",
                "Shorthand",
                "Social Studies",
                "Business Studies",
                "Computer Studies",
                "Home Economics (7)",
                "Basic Science and Technology (7)",
                "Basic Science (7)",
                "Visual Arts",
                "Islamic Studies (7)",
                "Christian Religious Studies (8)",
                "Civic Education/Security Education (8)",
                "Pre-vocational Studies (8)",
                "Islamic Studies (8)",
                "French",
                "Computer Studies (8)",
                "Social Studies (8)",
                "Agriculture/Entrepreneurship (8)",
                "Basic Technology (8)",
                "Technical Drawing",
                "Further Mathematics",
                "Islamic Religious Studies",
                "Hausa Language",
                "Religious and Values Education (8)",
                "Physical and Health Education (7)",
                "Christian Religious Studies",
                "Economics",
                "Christian Religious Language",
                "Computer Studies (7)",
                "Islamic Religious Language",
                "Literature in English",
                "Religious and Values Education (7)",
                "Physical and Health Education (8)",
                "Government",
                "Basic Science (8)",
                "Agricultural Science",
                "Foods & Nutrition",
                "English Language");



        if (number < subjectNames.size() && number > 0) {

            subjects =
                    subjectNames.stream()
                            .limit(number)
                            .map(name -> new Subject(name, schoolId))
                            .collect(Collectors.toList());

        } else {
            subjects =
                    subjectNames.stream()
                            .map(name -> new Subject(name, schoolId))
                            .collect(Collectors.toList());

        }

        return subjects;
    }
}
