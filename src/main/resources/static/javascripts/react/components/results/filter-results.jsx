var React = require('react');
var ReactDOM = require('react-dom');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var SelectClass = require('./dropdowns/select-class');
var SelectSubject = require('./dropdowns/select-subject');
var SelectResultCategory = require('./dropdowns/select-result-category');
var Actions = require('../actions');

module.exports =  React.createClass({

    getInitialState: function() {
        return {
            sessions: [],
            terms: [],
            classes: [],
            subjects: [],
            resultCategories: [],
            resultItem: []
        }
    },

    render: function() {
        return <div className="col-md-14">
            <div className="row mb20">
                <div className="col-sm-2">
                    <SelectSession ref="session"/>
                </div>
                <div className="col-sm-2">
                    <SelectTerm ref="term"/>
                </div>
                <div className="col-sm-3">
                    <SelectSubject ref="subject"/>
                </div>
                <div className="col-sm-2">
                    <SelectClass ref="clazz"/>
                </div>
                <div className="col-sm-2">
                    <SelectResultCategory ref="resultCategory"/>
                </div>
                <div className="col-sm-1">
                    <button className="btn btn-primary" onClick={this.filterResults}>Search</button>
                </div>
            </div>
        </div>

    },

    handleClick: function(e) {
        e.preventDefault();
        this.filterResults();
    },

    componentWillMount: function() {
        this.populateInitialData();
    },

    populateInitialData: function() {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getSubjects();
    },


    filterResults: function() {

        if (ReactDOM.findDOMNode(this.refs.session).value !== null && ReactDOM.findDOMNode(this.refs.term).value !== null &&
            ReactDOM.findDOMNode(this.refs.clazz).value !== "" && ReactDOM.findDOMNode(this.refs.subject).value !== "" &&
            ReactDOM.findDOMNode(this.refs.resultCategory).value !== "") {

            this.props.filter(
                ReactDOM.findDOMNode(this.refs.session).value, ReactDOM.findDOMNode(this.refs.term).value,
                ReactDOM.findDOMNode(this.refs.clazz).value, ReactDOM.findDOMNode(this.refs.subject).value,
                ReactDOM.findDOMNode(this.refs.resultCategory).value
            );
        }
    }
});