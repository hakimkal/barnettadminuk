package com.zo.schoolmanager.domain.users;



import com.zo.schoolmanager.domain.SubjectClasses;
import com.zo.schoolmanager.domain.constants.Position;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.Title;
import com.zo.schoolmanager.domain.constants.UserType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Eze on 23/11/2015.
 */

public class Teacher extends User {

    private Position position;
    private LocalDate employmentDate;

    private List<SubjectClasses> subjectClasses = new ArrayList<>();
    private Set<String> classesToVet = new HashSet<>();
    private String clazz;


    public Teacher(){}

    public Teacher(Title title, String firstName, String lastName, String middleName, Sex sex, String address, Position position, String schoolId){

        this.setTitle(title);

        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setMiddleName(middleName);
        this.setAddress(address);
        this.setSex(sex);
        this.position = position;
        this.setSchoolId(schoolId);
        this.setUserType(UserType.TEACHER);
        this.setUsername(firstName.toLowerCase() + "." + lastName.toLowerCase());
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(LocalDate employmentDate) {
        this.employmentDate = employmentDate;
    }

    public List<SubjectClasses> getSubjectClasses() {
        return subjectClasses;
    }

    public void setSubjectClasses(List<SubjectClasses> subjectClasses) {
        this.subjectClasses = subjectClasses;
    }

    public Set<String> getClassesToVet() {
        return classesToVet;
    }

    public void setClassesToVet(Set<String> classesToVet) {
        this.classesToVet = classesToVet;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}
