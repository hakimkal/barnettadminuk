package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.Subject;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ozo on 11/02/2016.
 */
public interface SubjectRepository extends MongoRepository<Subject, String> {

    Subject findOneByNameAndSchoolId(String name, String schoolId);

}
