package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.SubjectClasses;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.repository.ClassRepository;
import com.zo.schoolmanager.repository.SubjectRepository;
import com.zo.schoolmanager.repository.TeacherRepository;
import com.zo.schoolmanager.domain.constants.Position;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.constants.Title;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 25/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class TeacherTests {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptEncoder;

    @Test
    public void testCanCreateTeacher() {

        Teacher teacher = new Teacher(Title.Mr, "Jake", "Smith", "Mark", Sex.Male, "22 Drake Way", Position.CLASS_TEACHER, "56bc3d80d4c60e72a53806c0");
        teacher.setClazz("SS1A");
        teacher.setPassword(bCryptEncoder.encode("admin"));

        SubjectClasses subjectClasses1 = new SubjectClasses();

        subjectClasses1.setSubject(subjectRepository.findOneByNameAndSchoolId("Mathematics", "56bc3d80d4c60e72a53806c0").getId());
        subjectClasses1.setClasses(
                classRepository.findByNameInAndSchoolId(Arrays.asList("SS1A", "SS2A", "Basic 7A"), "56bc3d80d4c60e72a53806c0")
                        .stream()
                        .map(c -> c.getId())
                        .collect(Collectors.toList()));

        teacher.setSubjectClasses(Arrays.asList(subjectClasses1));

        List<String> roles = Arrays.asList("ROLE_CLASS_TEACHER");
        teacher.setRoles(new HashSet<>(roles));


        Teacher savedTeacher = teacherRepository.save(teacher);

        Assert.assertNotNull(savedTeacher);
        Assert.assertNotNull(savedTeacher.getId());
    }

}
