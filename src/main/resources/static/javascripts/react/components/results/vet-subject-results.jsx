var React = require('react');
var ReactDOM = require('react-dom');
var SelectSubject = require('./dropdowns/select-subject');
var SelectClass = require('./dropdowns/select-class');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var Actions = require('../actions');
var Reflux = require('reflux');
var ClassResultStore = require('../stores/class-result-store');
var ErrorPopUp = require('../shared/error-popup');
var InfoPopUp = require('../shared/info-popup');

var VetSubjectResult = React.createClass({

    mixins: [
        Reflux.listenTo(ClassResultStore, 'onUpdateSubjectResults')
    ],

    getInitialState: function () {
        return {
            studentsResults: [],
            error: {},
            info: {},
            disable_button: true
        }

    },

    componentDidMount: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getSubjects();
    },

    render: function () {

        var rows = this.state.studentsResults.map((result) => {
            return <tr key={result.id}>
                <td className="lead">{result.studentName}</td>
                <td className="text-center">
                    { result.status === 'NONE' ?
                        <button className="btn btn-primary btn-quirk" onClick={() => this.vet(result.id)}>Vet
                            Result</button>
                        :
                        <button className="btn btn-warning btn-quirk disabled"
                                onClick={() => this.vet(result.id)} disabled={this.state.disable_button}>Vet Result</button>
                    }
                </td>
            </tr>
        });

        return (
            <div className="col-md-12">
                <div className="panel">
                    <div className="panel-heading">
                        <h3>Vet Students Results</h3>
                    </div>

                    <div className="panel-body">
                        <div className="form-group">
                            <p className="lead">
                                Prior to Vetting Results, ensure you have <span className="text-danger">Entered ALL Scores Correctly</span>.
                                If you proceed to vet results, the process <span className="text-danger">cannot be reverted!</span>
                                You can <a href="/results/view-subject-results"
                                           className="underline-link">View/Modify</a> the Scores you entered for your
                                Student by <a className="underline-link" href="/results/view-subject-results">Clicking
                                Here</a>
                            </p>
                        </div>


                        <div className="form-group col-md-12">
                            <div className="col-sm-2 form-group">
                                <SelectSession ref="session"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectTerm ref="term"/>
                            </div>

                            <div className="col-sm-2 form-group">
                                <SelectSubject ref="subjectId"/>
                            </div>
                            <div className="col-sm-2 form-group">
                                <SelectClass ref="clazz"/>
                            </div>

                            <button className="btn btn-primary" onClick={this.getStudentSubjectResults}>
                                Get Results
                            </button>
                        </div>

                        <div className="form-group col-md-9">
                            <div className="table-responsive">
                                <table className="table table-bordered table-success nomargin">
                                    <thead>
                                    <tr>
                                        <th className="text-left">Student Name</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {rows}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {
                            this.state.error.open === undefined ?
                                null :
                                <ErrorPopUp open={this.state.error.open}
                                            message={this.state.error.message}
                                            title={this.state.error.message}/>
                        }

                        {
                            this.state.info.open === undefined ?
                                null :
                                <InfoPopUp open={this.state.info.open}
                                           message={this.state.info.message}
                                           title={this.state.info.message}/>
                        }

                    </div>
                </div>
            </div>
        )
    },

    getStudentSubjectResults: function () {

        if (ReactDOM.findDOMNode(this.refs.subjectId).value !== '' &&
            ReactDOM.findDOMNode(this.refs.clazz).value !== '' &&
            ReactDOM.findDOMNode(this.refs.session).value !== '' &&
            ReactDOM.findDOMNode(this.refs.term).value !== '') {

            Actions.getStudentSubjectResults(ReactDOM.findDOMNode(this.refs.subjectId).value,
                ReactDOM.findDOMNode(this.refs.clazz).value, ReactDOM.findDOMNode(this.refs.session).value,
                ReactDOM.findDOMNode(this.refs.term).value);
        }
    },

    onUpdateSubjectResults: function () {

        //this.setState({});
        this.setState({studentsResults: ClassResultStore.response.results});

        if (ClassResultStore.fromUpdate == true) {
            if (ClassResultStore.response.status === 'success') {
                // todo - fixme
                this.setState({info: {}});
                this.setState({
                    info: {
                        message: "Subject Vetted Successfully",
                        title: "Vetting Results",
                        open: "true"
                    }
                });

                // Due to 'my' 'bad' structure/design of this class we need to reload
                // all the models so the change is reflected in the render - disabled button
                this.getStudentSubjectResults();

            } else {
                // todo - fixme
                this.setState({error: {}});
                this.setState({
                    error: {
                        message: "There was an error during Vetting",
                        title: "Vetting Results",
                        open: "true"
                    }
                });
            }
        }
    },

    vet: function (resultId) {
        Actions.vetSubjectResult(resultId,
            ReactDOM.findDOMNode(this.refs.subjectId).value,
            ReactDOM.findDOMNode(this.refs.clazz).value,
            ReactDOM.findDOMNode(this.refs.session).value,
            ReactDOM.findDOMNode(this.refs.term).value);
    }
});

ReactDOM.render(<VetSubjectResult/>, document.getElementById("vet-subject-results"));