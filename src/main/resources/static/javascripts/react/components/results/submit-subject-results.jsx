var React = require('react');
var ReactDOM = require('react-dom');
var SelectSubject = require('./dropdowns/select-subject');
var SelectClass = require('./dropdowns/select-class');
var SelectSession = require('./dropdowns/select-session');
var SelectTerm = require('./dropdowns/select-term');
var Actions = require('../actions');
var Reflux = require('reflux');
var ClassResultStore = require('../stores/class-result-store');
var Modal = require('react-bootstrap/lib/Modal');
var SubmitResult = React.createClass({

    mixins: [
        Reflux.listenTo(ClassResultStore, 'onSubmitResultToClassTeacherNotification')
    ],

    getInitialState: function () {
        return {

            resultsSubmitted: false,
            success: false,
            failure: false,
            customMessage: '',
            userConfig: {}
        }
    },

    render: function () {
        return (
            <div className="col-md-10">
                <div className="panel">
                    <div className="panel-heading">
                        <h3>Submit Results to Class Teacher</h3>
                    </div>

                    <div className="panel-body">

                        { !this.state.resultsSubmitted ?
                            (
                                <div className="text-danger form-group">
                                    <p className="lead">Results can be submitted to a Class Teacher only after the
                                        results have been fully
                                        entered and vetted.</p>

                                    <p className="lead">Once you submit results, this action cannot be reversed!
                                        &nbsp; <a className="underline-link" href="/results/vet-subject-results">Click Here to
                                            Vet Results</a></p>
                                </div>
                            ) :

                            (
                                <div className="text-info form-group">
                                    <p className="lead">
                                        Results have been submitted!
                                    </p>
                                </div>
                            )
                        }

                        <div className="form-group col-sm-2">
                            <SelectSession ref="session"/>
                        </div>
                        <div className="form-group col-sm-2">
                            <SelectTerm ref="term"/>
                        </div>
                        <div className="form-group col-sm-2">
                            <SelectSubject ref="subjectId"/>
                        </div>
                        <div className="form-group col-sm-2">
                            <SelectClass ref="clazz"/>
                        </div>


                        <div className="form-group">
                            <button className="btn btn-primary" onClick={this.submitResultsToClassTeacher}>
                                Submit Results
                            </button>
                            &nbsp;

                            <a href="/index" className="btn btn-success">
                                Back to Dashboard
                            </a>
                        </div>

                    </div>
                </div>

                {
                    /* Todo - refactor this logic used across the system into mixin*/
                    <Modal show={this.state.success} onHide={this.close}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                {'Submitting Results'}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className= { this.state.customMessage == '' ? 'alert alert-info fade in nomargin' :
                                'alert alert-warning fade in nomargin'}>
                                <h4>Info</h4>
                                <p className="lead">
                                    { this.state.customMessage != '' ? this.state.customMessage :
                                        'Your Results have been successfully submitted!'}
                                </p>
                            </div>
                        </Modal.Body>
                    </Modal>

                }


                {
                    /* Todo - refactor this logic used across the system into mixin*/
                    <Modal show={this.state.failure} onHide={this.close}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                {'Error Submitting Results'}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="alert alert-danger fade in nomargin">
                                <h4>Error</h4>
                                <p className="lead">
                                    { this.state.customMessage != '' ? this.state.customMessage :
                                        'There was a problem submitting results'}</p>
                            </div>
                        </Modal.Body>
                    </Modal>

                }

            </div>
        )
    },

    componentDidMount: function () {
        this.populateInitialData();
    },

    populateInitialData: function () {
        Actions.getSessions();
        Actions.getTerms();
        Actions.getSubjects();
    },

    submitResultsToClassTeacher: function () {
        if (ReactDOM.findDOMNode(this.refs.subjectId).value != '' &&
            ReactDOM.findDOMNode(this.refs.clazz).value != '' &&
            ReactDOM.findDOMNode(this.refs.session).value != undefined &&
            ReactDOM.findDOMNode(this.refs.term).value != undefined
        ) {
            Actions.submitResultToClassTeacher(ReactDOM.findDOMNode(this.refs.subjectId).value,
                ReactDOM.findDOMNode(this.refs.clazz).value,
                ReactDOM.findDOMNode(this.refs.session).value,
                ReactDOM.findDOMNode(this.refs.term).value);
        }
    },

    onSubmitResultToClassTeacherNotification: function () {

        if (ClassResultStore.response.status == 'success') {
            this.setState({success: true, failure:false, customMessage: ClassResultStore.response.message});
        } else {
            this.setState({failure: true, success:false, customMessage: ClassResultStore.response.message});
        }
    },

    close: function () {
        this.setState({success: false, failure: false});
    }
});

ReactDOM.render(<SubmitResult />, document.getElementById('submit-subject-results'));


