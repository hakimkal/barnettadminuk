var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getSubjects: function() {
        return Api.get('/subjects/user').then(function(data) {
            this.subjects = data;
            this.trigger("updateSubjects", data);
        }.bind(this));
    }

});