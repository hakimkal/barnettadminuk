package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ozo on 16/12/2015.
 */
public interface StudentSubjectResultRepository extends MongoRepository<StudentSubjectResult, String> {
    List<StudentSubjectResult> findByStudentIdAndSessionAndTermAndSchoolId(String studentId, String session, String term, String schoolId);
    StudentSubjectResult findByStudentIdAndSessionAndTermAndSubjectAndSchoolId(String studentId, String session, String term, String subjectId, String schoolId);
    List<StudentSubjectResult> findBySessionAndTermAndClazzAndSubjectAndSchoolId(String session, String term, String clazz, String subject, String schoolId);
}
