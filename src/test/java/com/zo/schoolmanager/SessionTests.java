package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.Session;
import com.zo.schoolmanager.domain.Term;
import com.zo.schoolmanager.repository.SessionRepository;
import com.zo.schoolmanager.service.SessionService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.Month;
import java.util.*;
import java.time.LocalDateTime;

/**
 * Created by ozo on 11/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class SessionTests {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private SessionRepository sessionRepository;

    @Test
    public void testCreateSession() {
        List terms = new ArrayList<Term>();
        Term first = createTerm("First Term", 2015, Month.SEPTEMBER, 20, 2015, Month.DECEMBER, 10);
        Term second = createTerm("Second Term", 2016, Month.JANUARY, 10, 2016, Month.APRIL, 30);
        Term third  = createTerm("Third Term", 2016, Month.MAY, 30, 2016, Month.JULY, 5);

        terms.addAll(Arrays.asList(first, second, third));

        Session session = new Session("2016/2017", "schoolId", terms);
        Session savedSession = sessionService.save(session);
        Assert.assertNotNull(savedSession);
        Assert.assertNotNull(savedSession.getId());
    }

    @Test
    public void testCreateMultipleSessions() {

        Term first1 = createTerm("First Term", 2015, Month.OCTOBER, 20, 2015, Month.DECEMBER, 10);
        Term second1 = createTerm("Second Term", 2016, Month.JANUARY, 10, 2016, Month.APRIL, 30);
        Term third1  = createTerm("Third Term", 2016, Month.MAY, 30, 2016, Month.JUNE, 5);

        Session session1 = new Session("2016/2017", "schoolId", Arrays.asList(first1, second1, third1));
        Session savedSession1 = sessionService.save(session1);
        Assert.assertNotNull(savedSession1);
        Assert.assertNotNull(savedSession1.getId());

        Term first2 = createTerm("First Term", 2016, Month.SEPTEMBER, 15, 2016, Month.DECEMBER, 14);
        Term second2 = createTerm("Second Term", 2017, Month.JANUARY, 9, 2017, Month.MARCH, 30);
        Term third2  = createTerm("Third Term", 2017, Month.MAY, 23, 2017, Month.JUNE, 5);

        Session session2 = new Session("2017/2018", "schoolId", Arrays.asList(first2, second2, third2));
        Session savedSession2 = sessionService.save(session2);
        Assert.assertNotNull(savedSession2);
        Assert.assertNotNull(savedSession2.getId());
    }

    @Test
    public void testGetCurrentSession() {

        Term f1 = createTerm("First Term", 2015, Month.SEPTEMBER, 20, 2015, Month.DECEMBER, 9);
        Term f2 = createTerm("Second Term", 2016, Month.JANUARY, 10, 2016, Month.APRIL, 30);
        Term f3  = createTerm("Third Term", 2016, Month.MAY, 30, 2016, Month.JUNE, 5);
        Session session1 = new Session("2016/2017", "schoolId", Arrays.asList(f1, f2, f3));

        Term first2 = createTerm("First Term", 2016, Month.SEPTEMBER, 15, 2016, Month.DECEMBER, 14);
        Term second2 = createTerm("Second Term", 2017, Month.JANUARY, 9, 2017, Month.MARCH, 30);
        Term third2  = createTerm("Third Term", 2017, Month.MAY, 23, 2017, Month.JUNE, 5);

        Session session2 = new Session("2017/2018", "schoolId", Arrays.asList(first2, second2, third2));

        sessionService.save(session1);
        sessionService.save(session2);

        Session currentSession = sessionService.currentSession("schoolId");

        Assert.assertTrue(currentSession.getName().equals(session1.getName()));
    }

    @Test
    public void testGetSession() {

        Term f1 = createTerm("First Term", 2015, Month.AUGUST, 29, 2015, Month.DECEMBER, 9);
        Term f2 = createTerm("Second Term", 2016, Month.JANUARY, 10, 2016, Month.APRIL, 30);
        Term f3  = createTerm("Third Term", 2016, Month.MAY, 30, 2016, Month.JUNE, 5);
        Session session1 = new Session("2016/2017", "schoolId", Arrays.asList(f1, f2, f3));

        Term first2 = createTerm("First Term", 2016, Month.SEPTEMBER, 15, 2016, Month.DECEMBER, 14);
        Term second2 = createTerm("Second Term", 2017, Month.JANUARY, 9, 2017, Month.MARCH, 30);
        Term third2  = createTerm("Third Term", 2017, Month.MAY, 23, 2017, Month.JUNE, 5);

        Session session2 = new Session("2017/2018", "schoolId", Arrays.asList(first2, second2, third2));

        sessionService.save(session1);
        sessionService.save(session2);


        LocalDateTime timeInMarch2017 = LocalDateTime.of(2017, Month.MAY, 25, 23, 59);

        Session sessionIn2017 = sessionService.getSession("schoolId", timeInMarch2017);

        Assert.assertTrue(sessionIn2017.getName().equals(session2.getName()));

    }

    @Test
    public void testGetCurrentTerm() {

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime dateInFuture = now.plusDays(30);

        Term f1 = createTerm("First Term", 2015, Month.SEPTEMBER, 20, 2015, Month.DECEMBER, 9);
        Term f2 = createTerm("Second Term", 2016, Month.JANUARY, 10, 2016, Month.APRIL, 30);
        Term f3  = createTerm("Third Term", 2016, Month.MAY, 30, 2016, Month.JUNE, 5);
        Session session1 = new Session("2016/2017", "schoolId", Arrays.asList(f1, f2, f3));

        Term first2 = createTerm("First Term", 2016, Month.SEPTEMBER, 15, 2016, Month.DECEMBER, 14);
        Term second2 = createTerm("Second Term", 2017, Month.JANUARY, 9, 2017, Month.MARCH, 30);
        Term third2  = createTerm("Third Term", 2017, Month.MAY, 23, 2017, Month.JUNE, 5);

        Session session2 = new Session("2017/2018", "schoolId", Arrays.asList(first2, second2, third2));

        sessionService.save(session1);
        sessionService.save(session2);

        Term term = sessionService.currentTerm("schoolId");

        Assert.assertNotNull(term);
        Assert.assertTrue(term.getName().equals("Second Term"));
        Assert.assertTrue(term.getEnds().isBefore(dateInFuture));

    }

    private Term createTerm(String name, int startYear, Month startMonth, int startDay,
                                 int endYear, Month endtMonth, int endDay) {

        return new Term(name, LocalDateTime.of(startYear, startMonth, startDay, 0, 0),
                LocalDateTime.of(endYear, endtMonth, endDay, 23, 59));
    }

//    @After
    @Before
    public void cleanUp() {
        sessionRepository.delete(sessionRepository.findAll());
    }
}
