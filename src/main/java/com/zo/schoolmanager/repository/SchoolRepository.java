package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.school.School;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ozo on 27/11/15.
 */
public interface SchoolRepository extends MongoRepository<School, String> {
}
