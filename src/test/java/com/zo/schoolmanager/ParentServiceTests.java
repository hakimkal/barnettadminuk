package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.UserRepository;
import com.zo.schoolmanager.service.ParentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ozo on 10/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class ParentServiceTests extends TestSetup {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ParentService parentService;

    private Parent p1, p2, p3;

    public ParentServiceTests() {

        this.p1 = father();
        this.p2 = mother();
        this.p3 = mother();

        p1.setPassword("father1");
        p2.setPassword("mother1");
        p3.setPassword("mother2");
    }

    @Test
    public void testCanAuthenticateParent() {

        Student student = student();

        student.setParents(Arrays.asList(p1, p2, p3));
        studentRepository.save(student);

//        todo needs to be refactored to use spring security
//        Assert.assertTrue(parentService.authenticate(p1, p1.getUsername(), "father1"));
//        Assert.assertTrue(parentService.authenticate(p2, p2.getUsername(), "mother1"));
//        Assert.assertTrue(parentService.authenticate(p3, p3.getUsername(), "mother2"));
    }

    @Test
    public void testCanGetParentByUsername() {

        Parent father = father1();
        userRepository.save(father);

        Assert.assertNotNull(father);

        Parent savedFather = (Parent) userRepository.findOne(father.getId());

        Assert.assertTrue(savedFather.getFirstName().equals(father.getFirstName()));
        Assert.assertTrue(savedFather.getLastName().equals(father.getLastName()));
        Assert.assertTrue(savedFather.getUsername().equals(father.getUsername()));
        Assert.assertEquals(savedFather.getPhone2(), "089889392823");

    }

    @Test
    public void testCanCreateRolesForParents() {
        Parent parent = father();
        List<String> roles = new ArrayList<>();

        roles.addAll(Arrays.asList("PARENT_CAN_VIEW", "PARENT_CAN_EDIT"));

        parent.setRoles(new HashSet<>(roles));

        userRepository.save(parent);

    }

    @Test
    public void testCanGetChildrenFromParentId() {
        Parent parent = mother();
        userRepository.save(parent);

        Student child1 = student();
        Student child2 = student();
        Student child3 = student();

        child1.getParentIds().add(parent.getId());
        child2.getParentIds().add(parent.getId());
        child3.getParentIds().add(parent.getId());

        studentRepository.save(Arrays.asList(child1, child2, child3));

        Parent savedParent = parentService.findParentById(parent.getId());
        List<Student> children = savedParent.getChildren();

        Assert.assertEquals(3, children.size());

    }

}
