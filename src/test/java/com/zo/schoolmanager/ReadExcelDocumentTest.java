package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.commons.util.ReadExcelDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Map;

/**
 * Created by Eze on 02/03/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class ReadExcelDocumentTest {

    @Autowired
    private ReadExcelDocument readExcelDocument;

    @Test
    public void testReadStudentsFromExcelDoc() {
        Map<Student,Parent> map;
        map = readExcelDocument.readExcelDoc("/Users/ozo/Desktop/PRIMARY LIST FOR NET (1).xlsx");

        System.out.println((map.containsKey(null) || map.containsValue(null)) +" This is the result of the test");

        map.forEach((student,parent)->
            System.out.println(student.getSurname()+" "+student.getFirstName()+ " "+student.getMiddleName()+" "+parent.getPhone2()) );

    }
}
