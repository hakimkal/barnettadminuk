package com.zo.schoolmanager.domain.api.results;

import com.zo.schoolmanager.domain.api.ResponseObject;
import com.zo.schoolmanager.domain.results.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 08/03/2016.
 */
public class StudentSubjectResultDTO implements ResponseObject {

    private String id;
    private String studentId;
    private String studentName;
    private String subjectId;
    private Result.ResultStatus status;
    List<ResultItemDTO> resultItems = new ArrayList<>();

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public List<ResultItemDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ResultItemDTO> resultItems) {
        this.resultItems = resultItems;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Result.ResultStatus getStatus() {
        return status;
    }

    public void setStatus(Result.ResultStatus status) {
        this.status = status;
    }
}
