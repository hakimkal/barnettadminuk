package com.zo.schoolmanager.domain.payment;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Eze on 08/03/2016.
 */

@Document(collection = "paymentItems")
public class PaymentItem {

    @Id
    private String id;
    private String name;
    private String description;
    private LocalDate dueDate;
    private String schoolId;
    private List<String> classes;
    private LocalDateTime createdDate;
    private BigDecimal amount;
    private String session;
    private List<String> terms;


    public PaymentItem(){}

    public PaymentItem( String name, String description, LocalDate
            dueDate, String schoolId, List<String> classes, BigDecimal amount,
                       String session, List<String> terms) {

        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.schoolId = schoolId;
        this.classes = classes;
        this.createdDate = LocalDateTime.now();
        this.amount = amount;
        this.session = session;
        this.terms = terms;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }


   @Override
    public boolean equals(Object o){
        if((o instanceof PaymentItem) && ((PaymentItem) o).getId().equals(this.getId()) && ((PaymentItem)o).getName().equals(this.getName())){
            return true;
        }
        else  {
            return false;
        }
    }

    @Override
    public int hashCode(){
        int result = 17;
        result = 31 * result + id.hashCode() + name.hashCode() ;
        return result;
    }


}
