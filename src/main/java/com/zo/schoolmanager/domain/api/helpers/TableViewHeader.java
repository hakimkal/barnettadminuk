package com.zo.schoolmanager.domain.api.helpers;
import com.zo.schoolmanager.domain.api.ResponseObject;

/**
 * Created by ozo on 10/03/2016.
 */
public class TableViewHeader implements ResponseObject {

    private String key;
    private String name;
    private String shortName;
    private Integer order;

    public TableViewHeader() {}

    public TableViewHeader(String key, String name, String shortName, Integer order) {
        this.key = key;
        this.name = name;
        this.shortName = shortName;
        this.order = order;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
