package com.zo.schoolmanager.domain.results;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import java.time.LocalDateTime;


/**
 * Created by ozo on 16/12/2015.
 */

public class ResultItem {

    @Id
    private String id;

    private String resultCategory;
    private Double score;
    private ResultStatus status;
    private LocalDateTime created;
    private LocalDateTime lastUpdated;
    private int order;
    private Double maxScore;

    // Used only for passing data to ResultItemDTO
    @Transient
    private String subjectResultStatus;

    // Used for grouping results by parent category
    private String parentResultCategory;

    public enum ResultStatus {
        SCORE_SAVED,
        NO_SCORE
    }

    public ResultItem() {}

    public ResultItem(String resultCategory, String parentResultCategory, Double score, ResultStatus status, Double maxScore) {
        this.resultCategory = resultCategory;
        this.parentResultCategory = parentResultCategory;
        this.score = score;
        this.status = status;
        this.created = LocalDateTime.now();
        this.maxScore = maxScore;
        this.setId(id);
    }

    public ResultItem(String resultCategory, String parentResultCategory, ResultStatus status, Double maxScore) {
        this.parentResultCategory = parentResultCategory;
        this.resultCategory = resultCategory;
        this.status = status;
        this.created = LocalDateTime.now();
        this.maxScore = maxScore;
        this.setId(id);
    }

    public String getResultCategory() {
        return resultCategory;
    }

    public void setResultCategory(String resultCategory) {
        this.resultCategory = resultCategory;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectResultStatus() {
        return subjectResultStatus;
    }

    public void setSubjectResultStatus(String subjectResultStatus) {
        this.subjectResultStatus = subjectResultStatus;
    }

    public String getParentResultCategory() {
        return parentResultCategory;
    }

    public void setParentResultCategory(String parentResultCategory) {
        this.parentResultCategory = parentResultCategory;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }
}
