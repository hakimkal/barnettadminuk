package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.Subject;
import com.zo.schoolmanager.repository.SubjectRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;

/**
 * Created by ozo on 11/02/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class SubjectTests {

    @Autowired
    private SubjectRepository subjectRepository;

    @Test
    public void testCanCreateSubjects() {

        Subject subject1 = new Subject("English Language", "56bc3d80d4c60e72a53806c0");
        Subject subject2 = new Subject("French", "56bc3d80d4c60e72a53806c0");
        Subject subject3 = new Subject("Mathematics", "56bc3d80d4c60e72a53806c0");
        Subject subject4 = new Subject("Further Mathematics", "56bc3d80d4c60e72a53806c0");
        Subject subject5 = new Subject("Biology", "56bc3d80d4c60e72a53806c0");
        Subject subject6 = new Subject("Computer Science", "56bc3d80d4c60e72a53806c0");

        subjectRepository.save(Arrays.asList(subject1, subject2, subject3, subject4, subject5, subject6));

        Assert.assertEquals(6, subjectRepository.findAll().size());
    }

    @Test
    public void testCanFindSubjectByName() {

        Subject subject1 = new Subject("French Language", "schoolId");
        subjectRepository.save(subject1);

        Subject found = subjectRepository.findOneByNameAndSchoolId("French Language", "schoolId");
        Assert.assertTrue("French Language".equals(found.getName()));
    }

}
