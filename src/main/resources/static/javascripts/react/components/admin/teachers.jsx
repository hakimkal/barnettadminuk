var React = require('react');
var ReactDOM = require('react-dom');
var Reflux = require('reflux');
var Actions = require('../actions');
var TeacherStore = require('../stores/teacher-store')

var TeacherList = React.createClass({

    mixins: [
        Reflux.listenTo(TeacherStore, "onUpdateTeachers")
    ],

    getInitialState: function () {
        return {teachers: []}
    },

    componentWillMount: function() {
        Actions.getTeachers();
    },

    componentDidMount: function () {

        $('#dataTable').dataTable({
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "bDestroy": true
        });


    },

    render: function () {
        var rows = this.state.teachers.map(function(teacher) {
            return <tr>
                <td>
                    {teacher.firstName} {teacher.lastName}
                </td>
                <td>

                </td>
                <td>
                    {teacher.clazz}
                </td>
                <td>
                    {teacher.primaryPhone}
                </td>
                <td>
                    {teacher.email}
                </td>
            </tr>

        });

        return (
            <table id="dataTable" className="table table-bordered table-success nomargin">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Subject(s) Taught</th>
                    <th>Class Teacher</th>
                    <th>Phone</th>
                    <th>Email</th>
                </tr>
                </thead>

                <tbody>
                {rows}
                </tbody>
            </table>
        )
    },

    onUpdateTeachers: function() {
        console.log("udpate");
        this.setState({teachers: TeacherStore.teachers});
    }

});

ReactDOM.render(
    <TeacherList />,
    document.getElementById('teachers')
);