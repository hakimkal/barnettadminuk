var React = require('react')
var StudentListHeader = require('./student-list-header');

var StudentList = React.createClass({

    getInitialState: function() {
        return {data: []}
    },

    loadData: function() {
        $.ajax({
            url: this.props.url,
            success: function(data) {
                this.setState({data: data})
            }.bind(this)
        });
    },

    componentWillMount: function() {
        this.loadData();
    },

    componentDidUpdate: function(){
        $('#dataTable1').dataTable({
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "bDestroy": true
        });
    },

    render: function() {
        var rows = this.state.data.map(function(r) {
            return (
                <tr>
                    <td>1</td><td>{r.lastname}  {r.firstname} </td><td>{moment(r.dob).format("DD MMM YYYY")}</td><td>{r.sex}</td><td>SS1A</td>
                    <td>
                        <div className="btn-group mr5">
                            <button type="button" className="btn btn-primary">View</button>
                            <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span className="caret"></span>
                                <span className="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Edit</a></li>
                                <li className="divider"></li>
                                <li><a href="#">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            )
        });

        return (
            <table id="dataTable1" className="table table-bordered table-success nomargin">
                <thead>
                <StudentListHeader url="/api/html/students/list/2"/>
                </thead>

                <tfoot>
                <StudentListHeader url="/api/html/students/list/2" />
                </tfoot>

                <tbody>
                {rows}
                </tbody>
            </table>
        )
    }

});

React.render(
    <StudentList url="/api/students/school/2" />,
    document.getElementById('student-list')
);