var React = require('react')

module.exports = React.createClass({
    render: function() {
        return (
            <div>
                <div className="form-group mt10">
                    <label className="col-sm-3 control-label"><span className="text-danger">*</span> Admission Number:</label>
                    <div className="col-sm-8">
                        <input name="admissionnumber" type="text" className="form-control required" placeholder="Enter Admission Number" />
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-3 control-label"><span className="text-danger">*</span> Surname:</label>
                    <div className="col-sm-8">
                        <input name="lastname" type="text" className="form-control required" placeholder="Enter your Surname" />
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-3 control-label"><span className="text-danger">*</span> First Name:</label>
                    <div className="col-sm-8">
                        <input name="firstname" type="text" className="form-control required" placeholder="Enter your First Name" />
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-3 control-label"><span className="text-danger">*</span> Middle Name:</label>
                    <div className="col-sm-8">
                        <input name="middlename" type="text" className="form-control" placeholder="Enter your Middle Name" />
                    </div>
                </div>
            </div>
        )
    }
});