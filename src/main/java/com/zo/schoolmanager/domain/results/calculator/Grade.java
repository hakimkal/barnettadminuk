package com.zo.schoolmanager.domain.results.calculator;

/**
 * Created by ozo on 11/08/2016.
 */
public class Grade {
    private String id;
    private String name;
    private String description;
    private Integer aStartGrade;
    private Integer aEndGrade;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getaStartGrade() {
        return aStartGrade;
    }

    public void setaStartGrade(Integer aStartGrade) {
        this.aStartGrade = aStartGrade;
    }

    public Integer getaEndGrade() {
        return aEndGrade;
    }

    public void setaEndGrade(Integer aEndGrade) {
        this.aEndGrade = aEndGrade;
    }
}
