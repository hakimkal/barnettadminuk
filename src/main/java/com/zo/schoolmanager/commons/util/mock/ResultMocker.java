package com.zo.schoolmanager.commons.util.mock;

import com.zo.schoolmanager.domain.Session;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.Subject;
import com.zo.schoolmanager.domain.results.*;
import com.zo.schoolmanager.domain.results.calculator.ResultComposition;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ozo on 01/08/2016.
 */
public class ResultMocker {

    public static List<StudentResult> createStudentResults(String schoolId, List<Session> sessions, List<Student> students, List<String> subjects) {

        List<StudentResult> studentResults = new ArrayList<>();

        // for each session and term, and for each student
        // create student subject results for all subjects
        // create student results for students

        sessions.forEach(ses ->
                ses.getTerms().forEach(t ->
                        students.forEach(s -> {
                            StudentResult studentResult = new StudentResult();

                            List<StudentSubjectResult> studentSubjectResults =
                                    createStudentSubjectResults(schoolId, ses.getName(), t.getName(),
                                            subjects, s.getClazz(), s.getId(), true);

                            studentResult.setStudentSubjectResults(studentSubjectResults);
                            studentResult.setStudent(s);
                            studentResult.setStatus(Result.ResultStatus.NONE);
                            studentResult.setClazz(s.getClazz());
                            studentResult.setTerm(t.getName());
                            studentResult.setSession(ses.getName());
                            studentResult.setStudentId(s.getId());
                            studentResult.setCreated(LocalDateTime.now());
                            studentResult.setSchoolId(schoolId);

                            studentResults.add(studentResult);
                        })));

        return studentResults;
    }

    public static List<StudentResult> createStudentResults(String schoolId, String session, String term, String clazz,
                                                           int noOfStudents, int noOfSubjects) {

        final List<Student> students = ModelMocker.createStudents(schoolId, noOfStudents, Arrays.asList(clazz));
        final List<Subject> subjects = ModelMocker.createSubjects(schoolId, noOfSubjects);

        final List<StudentResult> studentResults = new ArrayList<>();

        students.forEach(s -> {
            StudentResult studentResult = new StudentResult();

            List<StudentSubjectResult> studentSubjectResults =
                    createStudentSubjectResults(schoolId, session, term,
                            subjects.stream().map(Subject::getName).collect(Collectors.toList()),
                            clazz, s.getId(), false);

            studentResult.setStudentSubjectResults(studentSubjectResults);
            studentResult.setStudent(s);
            studentResult.setStatus(Result.ResultStatus.CLASS_TEACHER_SUBMITTED);
            studentResult.setClazz(clazz);
            studentResult.setTerm(term);
            studentResult.setSession(session);
            studentResult.setStudentId(s.getId());
            studentResult.setCreated(LocalDateTime.now());
            studentResult.setSchoolId(schoolId);

            studentResults.add(studentResult);
        });

        return studentResults;
    }

    public static List<StudentSubjectResult> createStudentSubjectResults(String schoolId, String session, String term,
                                                                         List<String> subjects, String clazz,
                                                                         String studentId, boolean scoresRandomised) {

        ResultTemplate resultTemplate = createResultTemplate(schoolId);

        List<StudentSubjectResult> studentSubjectResults = new ArrayList<>();

        for (String subject : subjects) {
            StudentSubjectResult studentSubjectResult = new StudentSubjectResult();
//            studentSubjectResult.setId(id.toString());
            studentSubjectResult.setClazz(clazz);
            studentSubjectResult.setStudentId(studentId);
            studentSubjectResult.setSubject(subject);
            studentSubjectResult.setSession(session);
            studentSubjectResult.setTerm(term);
            studentSubjectResult.setSchoolId(schoolId);
//            studentSubjectResult.setId(String.valueOf(new Random().nextInt(1000000000)));
            studentSubjectResult.setStatus(Result.ResultStatus.NONE);

            // Create ResultItems for all ResultCategories
            final List<ResultItem> resultItems = new ArrayList<>();
            // This includes sub categories
            resultTemplate.getResultCategories().
                    stream().filter(rc -> rc.isResultKeyedIn())
                    .forEach(rc -> {
                ResultItem resultItem = new ResultItem();
                resultItem.setCreated(LocalDateTime.now());
                resultItem.setOrder(rc.getOrder());
                resultItem.setResultCategory(rc.getName());
                resultItem.setParentResultCategory(rc.getParentCategory());
                resultItem.setStatus(ResultItem.ResultStatus.SCORE_SAVED);
                resultItem.setId(ObjectId.get().toString());
//                resultItem.setId(String.valueOf(new Random().nextInt(1000000000)));
                resultItem.setMaxScore(rc.getMaxScore());

                // Get a random score for the specific result category, which is within the limits of the max score for that category
                int score = 0;
                if (scoresRandomised) {
                    score = new Random().nextInt(resultTemplate.getResultCategories()
                            .stream()
                            .filter(resultCategory -> resultCategory.getId().equals(rc.getId()))
                            .collect(Collectors.toList()).get(0).getMaxScore().intValue());
                } else {
                    int tempScore = rc.getMaxScore().intValue();
                    if (tempScore < 5) {
                        score = 1;
                    } else if (tempScore < 10) {
                        score = 5;
                    } else if (tempScore < 20) {
                        score = 10;
                    } else if (tempScore < 30) {
                        score = 20;
                    } else if (tempScore < 40) {
                        score = 30;
                    } else if (tempScore < 100) {
                        score = 40;
                    }
                }

                resultItem.setScore(new Double(Double.valueOf(score)));

                resultItems.add(resultItem);

            });

            studentSubjectResult.setResultItems(resultItems);
            studentSubjectResults.add(studentSubjectResult);

        }


        return studentSubjectResults;


    }

    public static ResultTemplate createResultTemplate(String schoolId) {
        ResultTemplate resultTemplate = new ResultTemplate();
        resultTemplate.setSchoolId(schoolId);
        resultTemplate.setCreated(LocalDateTime.now());
        resultTemplate.setResultCategories(new HashSet(createResultCategoriesWithSubCategoriesIncluded(schoolId)));
        resultTemplate.setResultCompositions(new HashSet(createResultCompositions()));
        resultTemplate.setDescription("ResultTemplate for School");
        resultTemplate.setId(String.valueOf(new Random().nextInt(1000000000)));

        return resultTemplate;
    }

    public static List<ResultCategory> createResultCategoriesWithSubCategoriesIncluded(String schoolId) {

        ResultCategory rc1 = new ResultCategory("Assignment 1", "Assignment 1", schoolId, "Assignment1", 10d, "Ass1", 1, true, true);
        rc1.setId("1");
        ResultCategory rc2 = new ResultCategory("Assignment 2", "Assignment 2", schoolId, "Assignment2", 10d, "Ass2", 2, true, true);
        rc2.setId("2");
        ResultCategory rc3 = new ResultCategory("Assignment 3", "Assignment 3", schoolId, "Assignment1", 10d, "Ass3", 3, true, true);
        rc3.setId("3");
        ResultCategory rc4 = new ResultCategory("Affective", "Affective", schoolId, "Affective", 5d, "Aff", 4, true, false);
        rc4.setId("4");
        ResultCategory rc5 = new ResultCategory("Psychomotor", "Psychomotor", schoolId, "Psychomotor", 5d, "Psy", 5, true, false);
        rc5.setId("5");
        ResultCategory rc6 = new ResultCategory("Affective", "Attendance", schoolId, "Attendance", 1d, "Attendance", 6, true, true);
        rc6.setId("6");
        ResultCategory rc7 = new ResultCategory("Affective", "Appearance", schoolId, "Appearance", 1d, "Appearance", 7, true, true);
        rc7.setId("7");
        ResultCategory rc8 = new ResultCategory("Affective", "Honesty", schoolId, "Honesty", 1d, "Honesty", 8, true, true);
        rc8.setId("8");
        ResultCategory rc9 = new ResultCategory("Affective", "Punctuality", schoolId, "Punctuality", 1d, "Punctuality", 9, true, true);
        rc9.setId("9");
        ResultCategory rc10 = new ResultCategory("Affective", "Relationship with Others", schoolId, "Relationship with Others", 1d, "Relationship with Others", 10, true, true);
        rc10.setId("10");
        ResultCategory rc11 = new ResultCategory("Psychomotor", "Class Exercise / Note Copying", schoolId, "Class Exercise / Note Copying", 2.5d, "Class Exercise / Note Copying", 11, true, true);
        rc11.setId("11");
        ResultCategory rc12 = new ResultCategory("Psychomotor", "Practicals / Projects", schoolId, "Practicals / Projects", 2.5d, "Practicals / Projects", 12, true, true);
        rc12.setId("12");
        ResultCategory rc13 = new ResultCategory("Continuous Assessment 1", "Continuous Assessment 1", schoolId, "Continuous Assessment 1", 10d, "CA1", 13, true, true);
        rc13.setId("13");
        ResultCategory rc14 = new ResultCategory("Continuous Assessment 2", "Continuous Assessment 2", schoolId, "Continuous Assessment 2", 10d, "CA2", 14, true, true);
        rc14.setId("14");
        ResultCategory rc15 = new ResultCategory("Continuous Assessment 3", "Continuous Assessment 3", schoolId, "Continuous Assessment 3", 10d, "CA3", 15, true, true);
        rc15.setId("15");
        ResultCategory rc16 = new ResultCategory("Examination", "Examination", schoolId, "Exam Results Classified here", 30d, "Exam", 16, true, true);
        rc16.setId("16");

        return Arrays.asList(rc1, rc2, rc3, rc4, rc5, rc6, rc7, rc8, rc9, rc10, rc11, rc12, rc13, rc14, rc15, rc16);
    }

    public static List<ResultComposition> createResultCompositions() {
        ResultComposition rc1 = new ResultComposition("Assignment 1", 10d);
        ResultComposition rc2 = new ResultComposition("Assignment 2", 10d);
        ResultComposition rc3 = new ResultComposition("Assignment 3", 10d);
        ResultComposition rc4 = new ResultComposition("Continuous Assessment 1", 10d);
        ResultComposition rc5 = new ResultComposition("Continuous Assessment 2", 10d);
        ResultComposition rc6 = new ResultComposition("Continuous Assessment 3", 10d);
        ResultComposition rc7 = new ResultComposition("Affective", 10d);
        ResultComposition rc8 = new ResultComposition("Psychomotor", 10d);
        ResultComposition rc9 = new ResultComposition("Examination", 20d);

        return Arrays.asList(rc1, rc2, rc3, rc4, rc5, rc6, rc7, rc8, rc9);
    }
}
