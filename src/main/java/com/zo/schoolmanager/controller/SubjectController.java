package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.Subject;
import com.zo.schoolmanager.domain.constants.UserType;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.domain.users.User;
import com.zo.schoolmanager.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 27/01/2016.
 */

@RestController
@RequestMapping("/subjects")
public class SubjectController extends BaseController {

    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping("/user")
    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    public List<Subject> getSubjectsTaughtByTeacher() {

        List<Subject> subjects = new ArrayList<>();
        User user = loggedInUser();

        if (user.getUserType() == UserType.TEACHER) {
            Teacher teacher = (Teacher) user;

            subjects = teacher.getSubjectClasses()
                    .stream()
                    .map(sc -> subjectRepository.findOneByNameAndSchoolId(sc.getSubject(), user.getSchoolId()))
                    .collect(Collectors.toList());


        } else if (user.getUserType() == UserType.SUPER_ADMIN) {
            subjects = subjectRepository.findAll();
        }

        return subjects;
    }
}
