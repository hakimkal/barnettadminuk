package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.TestSetup;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.results.ResultFilter;
import com.zo.schoolmanager.domain.api.results.ResultItemDTO;
import com.zo.schoolmanager.domain.api.results.ResultItemSaveRequest;
import com.zo.schoolmanager.domain.results.Result;
import com.zo.schoolmanager.domain.results.ResultItem;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import com.zo.schoolmanager.service.ResultItemService;
import com.zo.schoolmanager.service.ResultService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by ozo on 16/12/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class StudentSubjectResultTests extends TestSetup {

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ResultItemService resultItemService;

    @Autowired
    private ResultService resultService;

    @Before
    public void clearDB() {
        studentSubjectResultRepository.deleteAll();
    }

    @Test
    public void testCanCreateStudentSubjectResult() {
        StudentSubjectResult studentSubjectResult = new StudentSubjectResult();
        Assert.assertNotNull(studentSubjectResult);
    }

    @Test
    public void testCanSaveStudentSubjectResult() {

        Student student = studentRepository.save(student());
        StudentSubjectResult studentSubjectResult = createStudentSubjectResult(student.getId(), "Physics");

        Assert.assertNotNull(studentSubjectResult);
        Assert.assertNotNull(studentSubjectResult.getId());
        Assert.assertNotNull(studentSubjectResult.getCreated());

    }

    @Test
    public void testCanAddResultItemsToStudentSubjectResult() {
        Student student = studentRepository.save(student());
        StudentSubjectResult studentSubjectResult = createStudentSubjectResult(student.getId(), "Chemistry");
        ResultItem resultItem1 = new ResultItem("Continuous Assessment 1", "Continuous Assessment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem2 = new ResultItem("Continuous Assessment 2", "Continuous Assessment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem3 = new ResultItem("Continuous Assessment 3", "Continuous Assessment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem4 = new ResultItem("Assignment 1", "Assignment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem5 = new ResultItem("Assignment 2", "Assignment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem6 = new ResultItem("Assignment 3", "Assignment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem7 = new ResultItem("Examination", "Examination", 40d, ResultItem.ResultStatus.SCORE_SAVED, 40d);

        List firstResults = Arrays.asList(resultItem1, resultItem2, resultItem3);

        studentSubjectResult.getResultItems().addAll(firstResults);
        studentSubjectResultRepository.save(studentSubjectResult);

        Assert.assertTrue(studentSubjectResult.getStudentId().equals(student.getId()));
        Assert.assertNotNull(studentSubjectResult.getResultItems());
        Assert.assertEquals(3, studentSubjectResult.getResultItems().size());

        List secondResults = Arrays.asList(resultItem4, resultItem5, resultItem6, resultItem7);

        studentSubjectResult.getResultItems().addAll(secondResults);
        studentSubjectResultRepository.save(studentSubjectResult);

        StudentSubjectResult savedStudentSubjectResult = studentSubjectResultRepository.findOne(studentSubjectResult.getId());

        Assert.assertTrue(savedStudentSubjectResult.getStudentId().equals(student.getId()));
        Assert.assertNotNull(savedStudentSubjectResult.getResultItems());
        Assert.assertEquals(7, savedStudentSubjectResult.getResultItems().size());
    }

    @Test
    public void testCanFindStudentSubjectResultsByStudentIdSessionAndTerm() {

        Student student = studentRepository.save(student());
        StudentSubjectResult chemistrySubjectResult = createStudentSubjectResult(student.getId(), "Chemistry");
        StudentSubjectResult physicsSubjectResult = createStudentSubjectResult(student.getId(), "Physics");
        StudentSubjectResult frenchSubjectResult = createStudentSubjectResult(student.getId(), "French");

        ResultItem resultItem1 = new ResultItem("Continuous Assessment 1", "Continuous Assessment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem2 = new ResultItem("Continuous Assessment 2", "Continuous Assessment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem3 = new ResultItem("Continuous Assessment 3", "Continuous Assessment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem4 = new ResultItem("Assignment 1", "Assignment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem5 = new ResultItem("Assignment 2", "Assignment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);

        List firstResults = Arrays.asList(resultItem1, resultItem2, resultItem3, resultItem4, resultItem5);

        chemistrySubjectResult.getResultItems().addAll(firstResults);
        physicsSubjectResult.getResultItems().addAll(firstResults);
        frenchSubjectResult.getResultItems().addAll(firstResults);

        studentSubjectResultRepository.save(Arrays.asList(chemistrySubjectResult, physicsSubjectResult, frenchSubjectResult));

        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository.findByStudentIdAndSessionAndTermAndSchoolId(student.getId(), "2015/2016",
                        "First Term", student.getSchoolId());

        Assert.assertNotNull(studentSubjectResults);
        Assert.assertEquals(3, studentSubjectResults.size());
    }

    @Test
    public void testCanSaveResultItemDTOs() {
        ResultItemSaveRequest saveRequest = new ResultItemSaveRequest();

        ResultFilter filter = new ResultFilter();
        filter.setResultCategory("Examination");
        filter.setClazz("SS1A");
        filter.setSession("2015/2016");
        filter.setTerm("Second Term");
        filter.setSubject("Mathematics");

        saveRequest.setFilter(filter);
        saveRequest.setResultItems(getRandomResultItemForNewStudent("SS1A", "Examination", 60, 10));

        resultItemService.saveResultItemDTOs(saveRequest, "schoolId");

        // Need to check that results were saved properly
        List<ResultItemDTO> savedResultItemDTOs = resultItemService.getResultItemDTOs(filter, "schoolId");

        Assert.assertEquals(4, savedResultItemDTOs.size());

    }

    @Test
    public void testCanGetResultItemDTOsWithNoSavedResults() {

        studentRepository.findAll().forEach(studentRepository::delete);

        studentRepository.save(students(6));

        ResultFilter resultFilter = new ResultFilter("2015/2016", "First Term", "SS3A", "English Language", "Examinations");

        List<ResultItemDTO> resultItemDTOs = resultItemService.getResultItemDTOs(resultFilter, "schoolId");

        Assert.assertNotNull(resultItemDTOs);
        Assert.assertEquals(6, resultItemDTOs.size());
    }

    @Test
    public void testAreSubjectResultsInClassVetted() {

        // create a list of student subject results

        List<Student> students = students(20);
        students.forEach(s -> {
            StudentSubjectResult studentSubjectResult = createStudentSubjectResult(s.getId(), "ChemistryId");
            ResultItem resultItem1 = new ResultItem("Continuous Assessment 1", "Continuous Assessment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem2 = new ResultItem("Continuous Assessment 2", "Continuous Assessment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem3 = new ResultItem("Continuous Assessment 3", "Continuous Assessment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem4 = new ResultItem("Assignment 1", "Assignment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem5 = new ResultItem("Assignment 2", "Assignment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem6 = new ResultItem("Assignment 3", "Assignment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
            ResultItem resultItem7 = new ResultItem("Examination", "Examination", 40d, ResultItem.ResultStatus.SCORE_SAVED, 40d);

            List firstResults = Arrays.asList(resultItem1, resultItem2, resultItem3, resultItem4, resultItem5, resultItem6, resultItem7);

            studentSubjectResult.getResultItems().addAll(firstResults);
            studentSubjectResultRepository.save(studentSubjectResult);
        });

        Assert.assertFalse(resultService.areSubjectResultsInClassVetted("ChemistryId", "SS3B", "2015/2016", "First Term", "schoolId"));

        // Vet all Student Subject Results
        studentSubjectResultRepository.findBySessionAndTermAndClazzAndSubjectAndSchoolId("2015/2016", "First Term", "SS1A", "ChemistryId",
                "schoolId")
                .stream()
                .forEach(ssr -> {
                    ssr.setStatus(Result.ResultStatus.SUBJECT_TEACHER_VETTED);
                    studentSubjectResultRepository.save(ssr);
                });

        Assert.assertTrue(resultService.areSubjectResultsInClassVetted("ChemistryId", "SS3B", "2015/2016", "First Term", "schoolId"));

        // Vet all Student Subject Results
        studentSubjectResultRepository
                .findBySessionAndTermAndClazzAndSubjectAndSchoolId("2015/2016", "First Term", "SS3B", "ChemistryId", "schoolId")
                .stream()
                .limit(3)
                .forEach(ssr -> {
                    ssr.setStatus(Result.ResultStatus.NONE);
                    studentSubjectResultRepository.save(ssr);
                });

        Assert.assertFalse(resultService.areSubjectResultsInClassVetted("ChemistryId", "SS3B", "2015/2016", "First Term", "schoolId"));

    }

    private StudentSubjectResult createStudentSubjectResult(String studentId, String subject) {
        StudentSubjectResult studentSubjectResult = new StudentSubjectResult(studentId, "2015/2016", "First Term", "SS1A", subject, "schoolId");

        return studentSubjectResultRepository.save(studentSubjectResult);
    }


    private List<ResultItemDTO> getRandomResultItemForNewStudent(String className, String resultCategory, int maxScore, int records) {

        List<ResultItemDTO> resultItemDTOs = new ArrayList<>();
        Random random = new Random();

        for (int i = 1; i <= records; i++) {
            Student student = studentRepository.save(student());
            student.setClazz(className);
            ResultItemDTO item = new ResultItemDTO();
            item.setStudentId(student.getId());
            item.setResultCategory(resultCategory);
            item.setScore(new Integer(random.nextInt(maxScore)).toString());

            resultItemDTOs.add(item);
        }

        return resultItemDTOs;
    }


}
