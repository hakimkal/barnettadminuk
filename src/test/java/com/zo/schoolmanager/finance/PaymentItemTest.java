package com.zo.schoolmanager.finance;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.payment.PaymentItem;
import com.zo.schoolmanager.repository.PaymentItemRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eze on 08/03/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class PaymentItemTest  {


    private PaymentItem paymentItem;

    @Autowired
    PaymentItemRepository paymentItemRepository;

    @Test
    public void testCanCreatePaymentItem(){

        PaymentItem paymentItem = new PaymentItem();
        Assert.assertNotNull(paymentItem);
    }


    @Test
    public void testCanCreatePaymentItemWithFields(){
        School school = new School();
        List<String> classes = new ArrayList<>();
        classes.add("class1A");
        classes.add("class2B");
        List<String>terms = new ArrayList<>();
        terms.add("First Term");
        terms.add("Second Term");
        terms.add("Last Term");
        BigDecimal amount = new BigDecimal("15000.00");


        PaymentItem paymentItem = new PaymentItem("School fees","A student's school fees for a particular term",
                LocalDate.of(2017,3,4),school.getId(),classes,amount,"2017/2018",terms);

        Assert.assertEquals(paymentItem.getDueDate(),LocalDate.of(2017,3,4));
        Assert.assertEquals(paymentItem.getAmount().toString(),"15000.00");
    }


    @Test
    public void testSavePaymentItem(){
        School school = new School();
        List<String> classes = new ArrayList<>();
        classes.add("classJSS1A");
        classes.add("classJSS2B");
        classes.add("classJSS3C");
        List<String>terms = new ArrayList<>();
        terms.add("First Term");
        terms.add("Second Term");
        terms.add("Last Term");
        BigDecimal amount = new BigDecimal("2000.00");

        paymentItem = new PaymentItem("Excurtion", "This is an excurtion to a mine field. Students who attend this excurtion will not return home",
                LocalDate.of(2016,10,30),school.getId(),classes,amount,"2016/2017",terms);

        PaymentItem savedPaymentItem = paymentItemRepository.save(paymentItem);
        Assert.assertNotNull(savedPaymentItem);

    }

}
