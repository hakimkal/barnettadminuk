var React = require('react');
var ReactDom = require('react-dom');
var Tile = require('./tile');

var tiles = [
    {
        title: "view students",
        href: "",
        icon: "fa fa-eye",
        page: "col-xs-4 col-sm-5 col-md-3 page-events"
    },
    {
        title: "find student",
        href: "",
        icon: "fa fa-binoculars",
        page: "col-xs-4 col-sm-4 col-md-3 page-settings"
    },
    {
        title: "Add new student",
        href: "",
        icon: "fa fa-plus",
        page: "col-xs-4 col-sm-4 col-md-3 page-user"
    },
    {
        title: "edit student",
        href: "",
        icon: "fa fa-edit",
        page: "col-xs-4 col-sm-3 col-md-3 page-support"
    },
    {
        title: "view student result",
        href: "",
        icon: "fa fa-folder-open",
        page: "col-xs-4 col-sm-3 col-md-4 page-messages"
    },
    {
        title: "view student payments",
        href: "",
        icon: "fa fa-money",
        page: "col-xs-4 col-sm-5 col-md-4 page-statistics"
    },
    {
        title: "promote students",
        href: "",
        icon: "fa fa-graduation-cap",
        page: "col-xs-4 col-sm-4 col-md-4 page-settings"
    }
];



var Student = React.createClass({

    render: function(){
        var arrayOfTiles = [];

        tiles.map(function(value){
            if(value != undefined){
                arrayOfTiles.push(<Tile title = {value.title} icon = {value.icon} href = {value.href} page={value.page}/>);
            }
        });
        return (
            <div className="row panel-quick-page">
                {arrayOfTiles}
            </div>
        )
    }
});

ReactDom.render(<Student />, document.getElementById('students'));