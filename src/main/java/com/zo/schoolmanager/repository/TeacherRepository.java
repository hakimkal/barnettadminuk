package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.users.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ozo on 26/11/15.
 */
public interface TeacherRepository extends MongoRepository<Teacher, String> {
}
