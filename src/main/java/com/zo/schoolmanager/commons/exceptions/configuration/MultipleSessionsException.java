package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 13/04/2016.
 *
 * This Exception is thrown if a date falls within two or more Sessions
 */
public class MultipleSessionsException extends ConfigurationException {

    public MultipleSessionsException(String message) {
        super(message);
    }
}
