var webpack = require('webpack');

module.exports = {
    entry: {
        enterResults : "./components/results/enter-results.jsx",
        submitSubjectResults : "./components/results/submit-subject-results.jsx",
        submitClassResults : "./components/results/submit-class-results.jsx",
        vetSubjectResults : "./components/results/vet-subject-results.jsx",
        viewSubjectResults : "./components/results/view-subject-results.jsx",
        viewClassResults: "./components/results/view-class-results.jsx",
        viewStudentResult: "./components/results/view-student-result.jsx",
        sessions: "./components/admin/sessions.jsx",
        students:"./components/admin/student.jsx",
        classes:"./components/admin/classes.jsx",
        subjects: "./components/admin/subjects.jsx",
        financials:"./components/admin/financials.jsx",
        sessionsDash: "./components/admin/sessionsDash",
        newPaymentItem : "./components/payment/new-payment-item.jsx",
        newStudent : "./components/students/new-student.jsx",
        teachers: "./components/admin/teachers.jsx",
        newClass: "./components/admin/class/new-class.jsx",
        newTeacher: "./components/admin/teacher/new-teacher.jsx"
    },

    output: {
        path: './generated',
        filename: "[name].min.js",
        // Todo - Turned off for development. Turn on when done
        //chunkFilename: "[id].chunk.js"
    },

    module: {
        loaders: [
            { test: /\.jsx?$/, loaders: ['react-hot', 'jsx-loader?harmony'] }
        ]
    },
    resolve: {
        // you can now require('result-item') instead of require('result-item.jsx')
        extensions: ['', '.jsx', '.js', '.json']
    },
    plugins: [
        new webpack.NoErrorsPlugin()
        // Todo - Turned off for development. Turn on when done
        //new webpack.optimize.UglifyJsPlugin({minimize: true}),
        //new webpack.optimize.CommonsChunkPlugin({ filename: "commons.js", name: "commons" })
    ]

};
