package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.repository.StudentRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

/**
 * Created by ozo on 27/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class StudentTests extends TestSetup {

    private Student s1;

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void testCanCreateStudent() {
        Student student = new Student();
        Assert.assertNotNull(student);
    }

    @Test
    public void testCanCreateStudentWithFields() {
        School school = school();
        Student student = new Student("Emmanuel", "Odigbo", Sex.valueOf("Male"), LocalDate.of(1983, 2, 3), "Nigerian", "Anambra",
                "18 School Fields, SE1 5HJ", school.getId());
        Assert.assertEquals(student.getFirstName(), "Emmanuel");
        Assert.assertEquals(student.getNationality(), "Nigerian");
    }

    @Test
    public void testSaveStudent() {
//        School school = school();
        s1 = new Student("Chiazor", "Ozorji", Sex.valueOf("Male"), LocalDate.of(1985, 1, 3), "Nigerian", "Edo",
                "Blk 2, Flat 3, Domestic Way, Garki II, Abuja", "56bc3d80d4c60e72a53806c0");
        s1.setClazz("SS2A");

        Student savedStudent = studentRepository.save(s1);
        Assert.assertNotNull(savedStudent);
        Assert.assertNotNull(savedStudent.getId());
    }

}
