package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.admin.UserDTO;
import com.zo.schoolmanager.domain.users.Teacher;
import com.zo.schoolmanager.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ozo on 29/01/2016.
 */

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController extends BaseController {

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(method = RequestMethod.POST)
    public ApiResponse saveUser(@RequestBody UserDTO userDTO) {

        Teacher teacher = teacherService.saveTeacher(userDTO);
        if (teacher.getId() != null) {
            ApiResponse success = success();
            success.setMessage(teacher.getFirstName() + " " + teacher.getLastName() + " was saved Successfully");
            return success;

        } else {
            ApiResponse failure = failure();
            failure.setMessage("There was an error saving Teacher, try again");
            return failure;
        }
    }
}
