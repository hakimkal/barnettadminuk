var React = require('react');
var Reflux = require('reflux');
var ResultItemStore = require('../../stores/result-item-store');
var ResultItem = require('./result-item');
var InfoPopUp = require('../../shared/info-popup');

module.exports = React.createClass({

    mixins: [Reflux.listenTo(ResultItemStore, 'onUpdateResultItems')],

    getInitialState: function () {
        return {
            resultItems: [],
            updateOccurred: false,
            info: {}
        }
    },

    render: function () {

        var rows = this.state.resultItems.map(function (item) {
            return <ResultItem item={item} updateScore={this.updateScore} key={item.key}/>
        }.bind(this));

        return <div className="panel">
            <div className="panel-body">
                <div className="row mb20">

                    <table className="table table-bordered table-success table-striped mb20">
                        <thead>
                        <tr>
                            <th className="text-left">Student</th>
                            <th className="text-left">Score</th>
                            <th className="text-left">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {rows}
                            { this.state.updateOccurred ?
                                (   <tr>
                                        <td>
                                            <div>
                                                <button className="btn btn-primary btn-block" onClick={this.saveAll}>Save All
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ) :
                                    null
                            }
                        </tbody>
                    </table>
                </div>
            </div>

            { this.state.info.open === undefined ?
                null
                :
                <InfoPopUp title={this.state.info.title} message={this.state.info.message} open={this.state.info.open}/>
            }
        </div>
    },

    updateScore: function (studentId, score) {
        if (!this.state.updateOccurred) {
            this.setState({updateOccurred: true});
        }

        var allResultItems = this.state.resultItems;
        for (var i = 0; i < allResultItems.length; i++) {
            if (allResultItems[i].studentId === studentId) {
                allResultItems[i].score = score;
                break;
            }
        }
    },

    onUpdateResultItems: function () {

        // dirty hack - I hate it!
        // todo for self - fix this sometime in the future
        // I'm clearing the resultItems so react renders all the child elements again as currently it is not
        // updating correctly. Although it has the updated model, there seems no way to inform the child - ResultItem
        // that the state has changed

        this.setState({resultItems: [], updateOccurred: false });
        this.setState({resultItems: ResultItemStore.response.resultItems, updateOccurred:false });

        if (ResultItemStore.response.fromSave) {
            // todo - needs to fix
            this.setState({info: {}});
            this.setState({info: {
                title: 'Save Results', message:'Results Saved Successfully!', open:'true'
            }});
        }
    },

    saveAll: function () {
        this.props.saveResultItems(this.state.resultItems);
    }

});