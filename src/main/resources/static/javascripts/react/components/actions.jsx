var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getTeachers', 'saveClass', 'getClassesTaught', 'getAllClasses', 'getClassesForTeacherOrAdmin', 'getSessions', 'getSubjects', 'getResultItems', 'getTerms', 'getResultCategories',
    'saveResultItems', 'submitResultToClassTeacher', 'getStudentSubjectResults', 'vetSubjectResult', 'getStudentsSubjectResults',
    'getResultViewHeaders', 'getStudents', 'getStudentResults', 'getStudent', 'getStudentResultsData',
    'saveTeacherComments', 'submitResultsForFinalVetting', 'getStudentSubjectResultHeaders', 'saveStudent', 'saveTeacher'
]);