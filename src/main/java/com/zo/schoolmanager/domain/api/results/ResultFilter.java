package com.zo.schoolmanager.domain.api.results;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by ozo on 23/01/2016.
 */
public class ResultFilter implements RequestObject {

    private String session;
    private String term;
    private String clazz;
    private String subject;
    private String resultCategory;

    public ResultFilter() {}

    public ResultFilter(String session, String term, String clazz, String subject, String resultCategory) {
        this.session = session;
        this.term = term;
        this.clazz = clazz;
        this.subject = subject;
        this.resultCategory = resultCategory;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getResultCategory() {
        return resultCategory;
    }

    public void setResultCategory(String resultCategory) {
        this.resultCategory = resultCategory;
    }

    public boolean hasMissingField() {
        if (StringUtils.isEmpty(session) || StringUtils.isEmpty(term) || StringUtils.isEmpty(clazz) ||
                StringUtils.isEmpty(subject) || StringUtils.isEmpty(resultCategory)) {
            return true;
        }
        return  false;
    }
}
