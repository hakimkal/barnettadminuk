var React = require('react');
var ReactDOM = require('react-dom');
var Form = require("../../shared/form");
var Actions = require("../../actions");
var TeacherStore = require("../../stores/teacher-store");
var Reflux = require('reflux');
var Modal = require('react-bootstrap/lib/Modal');

var firstColumn = [
    {type: "string", title: "First Name", validation: {required: true}},
    {type: "string", title: "Middle Name"},
    {type: "string", title: "Last Name", validation: {required: true}},
    {type: "sex", title: "Sex", validation: {required: true}},
    {type: "select-class", title: "Class Name"},
    {type: "text-area", title: "Address"},
    {type: "string", title: "Phone Number", validation: {required: true}},
    {type: "string", title: "Email"}
];

var NewTeacher = React.createClass({

    getInitialState: function() {
        return {
            success: false
        }
    },

    mixins: [
        Reflux.listenTo(TeacherStore, 'onTeacherSaved')
    ],

    render: function () {
        return <div>
            <Form firstColumn={firstColumn}
                  title={'New Teacher'}
                  onSubmit={this.action}
            />

            <Modal show={this.state.success} onHide={this.closeSuccess}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Teacher'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-info fade in nomargin">
                        <h4>Info</h4>
                        <p className="lead">{'Teacher Saved Successfully!'}</p>
                    </div>
                </Modal.Body>
            </Modal>

            <Modal show={this.state.failure} onHide={this.closeFailure}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {'Save Teacher'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-danger fade in nomargin">
                        <h4>Error</h4>
                        <p className="lead">{'There was a problem saving teacher'}</p>
                    </div>
                </Modal.Body>
            </Modal>

        </div>
    },

    action: function (data) {
        console.log(JSON.stringify(data));
        Actions.saveTeacher(data);
    },

    onTeacherSaved: function () {

        if (TeacherStore.data.status == 'success') {
            this.setState({success: true});
        } else {
            this.setState({failure: true});
        }
    },

    closeSuccess: function () {
        this.setState({success: false});
        window.location.href = '/admin/teachers';
    },

    closeFailure: function () {
        this.setState({failure: false});
    },

});


ReactDOM.render(<NewTeacher />, document.getElementById('new-teacher'));
