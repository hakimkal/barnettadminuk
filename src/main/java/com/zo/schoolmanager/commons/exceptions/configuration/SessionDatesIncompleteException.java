package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 13/04/2016.
 */
public class SessionDatesIncompleteException extends ConfigurationException {

    public SessionDatesIncompleteException(String message) {
        super(message);
    }
}
