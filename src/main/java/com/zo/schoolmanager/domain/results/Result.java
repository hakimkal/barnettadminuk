package com.zo.schoolmanager.domain.results;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by ozo on 16/12/2015.
 */

public abstract class Result {

    private String session;
    private String term;
    private LocalDateTime created = LocalDateTime.now();
    private String clazz;
    private String schoolId;


    public enum ResultStatus {
        CLASS_TEACHER_COMMENTED,
        CLASS_TEACHER_VETTED,
        CLASS_TEACHER_SUBMITTED,
        HOD_VETTED,
        NONE,
        // These two are for student subject results only - just easier to have them all in this abstract class
        SUBJECT_TEACHER_VETTED,
        SUBJECT_TEACHER_SUBMITTED,
    }


    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
}
