
var BioDataInfo = React.createClass({displayName: "BioDataInfo",
   render: function() {
       return (
           React.createElement("div", null, 
                React.createElement("div", {className: "form-group mt10"}, 
                    React.createElement("label", {className: "col-sm-3 control-label"}, React.createElement("span", {className: "text-danger"}, "*"), " Admission Number:"), 
                    React.createElement("div", {className: "col-sm-8"}, 
                        React.createElement("input", {name: "admissionnumber", type: "text", className: "form-control required", placeholder: "Enter Admission Number"})
                    )
                ), 
                React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {className: "col-sm-3 control-label"}, React.createElement("span", {className: "text-danger"}, "*"), " Surname:"), 
                    React.createElement("div", {className: "col-sm-8"}, 
                        React.createElement("input", {name: "lastname", type: "text", className: "form-control required", placeholder: "Enter your Surname"})
                    )
                ), 
                React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {className: "col-sm-3 control-label"}, React.createElement("span", {className: "text-danger"}, "*"), " First Name:"), 
                    React.createElement("div", {className: "col-sm-8"}, 
                        React.createElement("input", {name: "firstname", type: "text", className: "form-control required", placeholder: "Enter your First Name"})
                    )
                ), 
               React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {className: "col-sm-3 control-label"}, React.createElement("span", {className: "text-danger"}, "*"), " Middle Name:"), 
                    React.createElement("div", {className: "col-sm-8"}, 
                        React.createElement("input", {name: "middlename", type: "text", className: "form-control", placeholder: "Enter your Middle Name"})
                    )
               )
           )
       )
   }
});

var StudentForm = React.createClass({displayName: "StudentForm",

   componentDidMount: function() {

       var form = $('#wizard-form');
       form.steps({
           headerTag: 'h3',
           bodyTag: 'div',
           stepsContainerTag: 'div',
           transitionEffect: 'slideLeft',
           onStepChanging: function (event, currentIndex, newIndex) {

               // Allways allow previous action even if the current form is not valid!
               if (currentIndex > newIndex) { return true; }

               // Needed in some cases if the user went back (clean up)
               if (currentIndex < newIndex) {
                   // To remove error styles
                   form.find('.body:eq(' + newIndex + ') label.error').remove();
                   form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
               }

               form.validate().settings.ignore = ':disabled,:hidden';
               return form.valid();
           },
           onFinishing: function (event, currentIndex) {
               form.validate().settings.ignore = ':disabled';
               return form.valid();
           },
           onFinished: function (event, currentIndex) {
               alert('Submitted!');
           }
       }).validate({
           highlight: function(element) {
               $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
           },
           success: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           }
       });

   },

   render: function() {
       return (
           React.createElement("form", {className: "form-horizontal", id: "wizard-form", action: "#"}, 
                React.createElement("h3", null, "Bio-Data Info"), 
                React.createElement(BioDataInfo, null)

           )
       )
   }

});

React.render(
   React.createElement(StudentForm, null), document.getElementById("form-container")
);
var SideSection = React.createClass({displayName: "SideSection",

    render: function() {
        return (
            React.createElement("nav", {className: "sidebar", "data-sidebar-anyclick-close": ""}, 
            React.createElement("ul", {className: "nav"}, 
            React.createElement("li", {className: "active"}, 
            React.createElement("a", {"data-toggle": "collapse", title: "Dashboard", href: "#dashboard", className: "", "aria-expanded": "true"}, 
            React.createElement("em", {className: "icon-home"}), 
            React.createElement("span", {"data-localize": "sidebar.nav.DASHBOARD"}, "My Area")
        ), 
        React.createElement("ul", {className: "nav sidebar-subnav collapse in", id: "dashboard", "aria-expanded": "true", styles: ""}, 
            React.createElement("li", {className: "sidebar-subnav-header"}), 
            React.createElement("li", {className: " active"}, 
            React.createElement("a", {title: "My Items", href: "/my_area"}, 
            React.createElement("span", null, "My Items")
        )
        ), 
        React.createElement("li", {className: " "}, 
            React.createElement("a", {title: "Lost Items", href: "/my_area/lost-items"}, 
            React.createElement("span", null, "Lost Items")
        )
        ), 
        React.createElement("li", {className: " "}, 
            React.createElement("a", {title: "Payments", href: "#"}, 
            React.createElement("span", null, "Payments")
            )
            )
            )
            )
            )
            )
        );
    }
});

React.render(
React.createElement(SideSection, null),
    document.getElementById('sidebar_sec')
);
var StudentListHeader = React.createClass({displayName: "StudentListHeader",

    getInitialState: function() {
        return {data: []}
    },

    loadData: function() {
        $.ajax({
            url: this.props.url,
            success: function(data) {
                this.setState({data: data})
            }.bind(this)
        });
    },

    componentWillMount: function() {
        this.loadData();
    },

    render: function() {

        var rows = this.state.data.map(function(header) {
            return React.createElement("th", null, header)
        });

        return (
            React.createElement("tr", null, 
                rows, 
                React.createElement("th", null, "Actions")
            )

        )
    }
});


var StudentList = React.createClass({displayName: "StudentList",

    getInitialState: function() {
        return {data: []}
    },

    loadData: function() {
        $.ajax({
          url: this.props.url,
            success: function(data) {
                this.setState({data: data})
            }.bind(this)
        });
    },

    componentWillMount: function() {
        this.loadData();
    },

    componentDidUpdate: function(){
        $('#dataTable1').dataTable({
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "bDestroy": true
        });
    },

    render: function() {
        var rows = this.state.data.map(function(r) {
            return (
                React.createElement("tr", null, 
                    React.createElement("td", null, "1"), React.createElement("td", null, r.lastname, "  ", r.firstname, " "), React.createElement("td", null, moment(r.dob).format("DD MMM YYYY")), React.createElement("td", null, r.sex), React.createElement("td", null, "SS1A"), 
                    React.createElement("td", null, 
                        React.createElement("div", {className: "btn-group mr5"}, 
                            React.createElement("button", {type: "button", className: "btn btn-primary"}, "View"), 
                            React.createElement("button", {type: "button", className: "btn btn-primary dropdown-toggle", "data-toggle": "dropdown"}, 
                            React.createElement("span", {className: "caret"}), 
                            React.createElement("span", {className: "sr-only"}, "Toggle Dropdown")
                        ), 
                        React.createElement("ul", {className: "dropdown-menu", role: "menu"}, 
                            React.createElement("li", null, React.createElement("a", {href: "#"}, "Edit")), 
                            React.createElement("li", {className: "divider"}), 
                            React.createElement("li", null, React.createElement("a", {href: "#"}, "Delete"))
                        )
                        )
                    )
                )
            )
        });

        return (
            React.createElement("table", {id: "dataTable1", className: "table table-bordered table-striped-col"}, 
                React.createElement("thead", null, 
                    React.createElement(StudentListHeader, {url: "/api/html/students/list/2"})
                ), 

                React.createElement("tfoot", null, 
                    React.createElement(StudentListHeader, {url: "/api/html/students/list/2"})
                ), 

                React.createElement("tbody", null, 
                    rows
                )
            )
        )
    }

});

React.render(
React.createElement(StudentList, {url: "/api/students/school/2"}),
    document.getElementById('student-list')
);