package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.Session;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ozo on 11/04/2016.
 */
public interface SessionRepository extends MongoRepository<Session, String> {
    List<Session> getSessionByStartsBeforeAndEndsAfterAndSchoolId(LocalDateTime starts, LocalDateTime ends, String schoolId);
    List<Session> getSessionBySchoolId(String schoolId);
}
