package com.zo.schoolmanager.domain.payment;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by Eze on 08/03/2016.
 */

@Document(collection = "paymentRecords")
public class PaymentRecord {

    @Id
    private String id;
    private String studentId;
    private String firstName;
    private String lastName;
    private String contactNumber;
    private String reference;
    private String bankPaidTo;
    private String session;
    private String term;
    private LocalDate datePaid;
    private LocalDateTime dateCreated;
    private String paymentItemId;
    private BigDecimal amountPaid;

    public PaymentRecord (){}

    public PaymentRecord ( String studentId, String firstName, String lastName, String contactNumber, String reference,
                          String bankPaidTo, String session, String term, LocalDate datePaid, String paymentItemId){

        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.reference = reference;
        this.bankPaidTo = bankPaidTo;
        this.session = session;
        this.term = term;
        this.datePaid = datePaid;
        this.dateCreated = LocalDateTime.now();
        this.paymentItemId = paymentItemId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getBankPaidTo() {
        return bankPaidTo;
    }

    public void setBankPaidTo(String bankPaidTo) {
        this.bankPaidTo = bankPaidTo;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public LocalDate getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(LocalDate datePaid) {
        this.datePaid = datePaid;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPaymentItemId() {
        return paymentItemId;
    }

    public void setPaymentItemId(String paymentItemId) {
        this.paymentItemId = paymentItemId;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }
}
