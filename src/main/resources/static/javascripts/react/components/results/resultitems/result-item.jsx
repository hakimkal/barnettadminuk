var React = require('react');
var Tooltip = require('react-bootstrap/lib/Tooltip');
var OverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');

module.exports = React.createClass({

    getInitialState: function () {

        return {
            item: this.props.item,
            savedBefore: this.props.item.saved
        }
    },

    render: function () {
        var message = '';
        if (this.state.item.vetted) {
            message = "You have vetted this result. It can't be changed!";
        } else if (this.state.item.processed) {
            message = "Results have already been compiled by Class Teacher";
        }

        var toolTip = <Tooltip placement="top" className="in" id={this.state.item.key}>
            {message}
        </Tooltip>;

        return (
            <tr>
                <td className="text-left">
                    <div className="col-sm-8">{this.state.item.studentName}</div>
                </td>
                <td className="text-left">
                    <div className="col-sm-5">
                        <input type="text" placeholder="Enter Score" className="form-control"
                               value={ this.state.item.score ? this.state.item.score : ''}
                               onChange={this.updateScore} disabled={this.state.item.vetted || this.state.item.saved ||
                                this.state.item.processed}/>
                    </div>
                </td>

                <td>
                    <div className="btn-demo">
                        <div>
                            <button className="btn btn-warning" disabled={this.state.item.vetted || this.state.item.processed}
                                    onClick={this.edit}>Edit
                            </button>
                            <button className="btn btn-danger" disabled={this.state.item.vetted || this.state.item.processed}
                                    onClick={this.delete}>Delete
                            </button>
                            { (this.state.item.vetted || this.state.item.processed) ?
                                <div>
                                    <OverlayTrigger placement="top" overlay={toolTip}>
                                        <button className="btn btn-danger btn-icon btn-stroke"><i className="fa fa-exclamation"></i>
                                        </button>
                                    </OverlayTrigger>
                                </div>
                                :
                                null
                            }
                        </div>
                    </div>
                </td>
            </tr>
        )
    },

    updateScore: function (e) {
        var newItem = this.state.item;
        newItem.score = e.target.value;
        this.setState({item: newItem});
        this.props.updateScore(this.state.item.studentId, e.target.value);
    },

    edit: function () {
        if (this.state.savedBefore) {
            var newItem = this.state.item;
            newItem.saved = this.state.item.saved ? false : true;
            this.setState({item: newItem});
        }
    },

    delete: function () {

    }

});