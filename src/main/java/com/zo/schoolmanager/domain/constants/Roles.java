package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 18/12/2015.
 */
public enum Roles {
    ADMIN,
    PARENT,
    STUDENT,
    CLASS_TEACHER,
    SUBJECT_TEACHER,
    HOD,
    SUPER_ADMIN
}
