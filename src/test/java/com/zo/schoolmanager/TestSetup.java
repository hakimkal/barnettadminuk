package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 10/11/15.
 */
public class TestSetup {

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;


    private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();

    public Student student() {
        School school = school();
        return new Student("Ja" + RandomStringUtils.random(3, true, false), "Lew" +
                RandomStringUtils.random(2, true, false), Sex.valueOf("Male"), LocalDate.of(1978, 2, 3),
                "Nigerian", "Edo", "22 Akoko-Edo, Lagos", school.getId());
    }

    public List<Student> students(int records) {
        List<Student> students = new ArrayList<>();
        School school = school();
        for (int i = 0; i < records; i++) {
            students.add(new Student("Da" + RandomStringUtils.random(4, true, false), "La" +
                    RandomStringUtils.random(2, true, false), Sex.valueOf("Male"), LocalDate.of(1978, 2, 3),
                    "Nigerian", "Edo", "22 Akoko-Edo, Lagos", school.getId(), "SS3D"));
        }

        return students;
    }

    public Parent mother() {
        return new Parent("Ma" + RandomStringUtils.random(5, true, false) ,
                "Li" + RandomStringUtils.random(4, true, false),
                "Jo" + RandomStringUtils.random(4, true, false), "08093898912", bCryptEncoder.encode("admin123"), "12345678");
    }

    public Parent father() {
        return new Parent("Ok" + RandomStringUtils.random(3, true, false),
                "Ha" + RandomStringUtils.random(3, true, false),
                "Jo" + RandomStringUtils.random(4, true, false), "08098398378", bCryptEncoder.encode("admin124"), "12345678");
    }


    public Parent father1() {
        return new Parent("Ok" + RandomStringUtils.random(3, true, false),
                "Ha" + RandomStringUtils.random(3, true, false),
                "Jo" + RandomStringUtils.random(4, true, false), "08098398378", bCryptEncoder.encode("admin123"), "12345678", "089889392823");
    }

    public School school() {
        School school = new School("Books Academy", "213 Jones Street", "Joe", "Lewis");
        school.setId("schoolId");

        return school;
    }

    @Before
    public void clearDB() {
//        studentRepository.findAll().forEach(studentRepository::delete);
//        teacherRepository.findAll().forEach(teacherRepository::delete);
        //studentSubjectResultRepository.findAll().forEach(studentSubjectResultRepository::delete);
    }
}
