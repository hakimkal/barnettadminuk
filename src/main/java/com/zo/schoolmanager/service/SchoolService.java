package com.zo.schoolmanager.service;

import com.zo.schoolmanager.commons.exceptions.configuration.MultipleResultCategoriesException;
import com.zo.schoolmanager.commons.exceptions.configuration.ResultCategoryNotFoundException;
import com.zo.schoolmanager.commons.exceptions.configuration.ResultTemplateException;
import com.zo.schoolmanager.domain.results.ResultCategory;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.repository.SchoolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Created by ozo on 25/08/2016.
 */
@Service
public class SchoolService {

    @Autowired
    private SchoolRepository schoolRepository;

    /**
     * Method to get the ResultTemplate given a school and class
     *
     * @param schoolId
     * @param clazz
     * @return
     * @throws ResultTemplateException I have included unless = "#result == null" mainly for documentation purposes; although it should work
     */
    @Cacheable(value = "ResultTemplatesCache", unless = "#result == null")
    public ResultTemplate getResultTemplate(String schoolId, String clazz) throws ResultTemplateException {

        School school = getSchool(schoolId);
        List<ResultTemplate> resultTemplates =
                school.getSetting()
                        .getResultConfiguration()
                        .getResultTemplates()
                        .stream().filter(rt -> rt.getClasses().contains(clazz))
                        .collect(Collectors.toList());

        if (resultTemplates.size() > 1) {
            throw new ResultTemplateException("More than one ResultTemplate configured for " + clazz);

        } else if (resultTemplates.size() == 1) {
            return resultTemplates.get(0);

        } else {
            throw new ResultTemplateException("No ResultTemplate configured for " + clazz);
        }
    }

    @Cacheable("SchoolsCache")
    public School getSchool(String schoolId) {
        return schoolRepository.findOne(schoolId);
    }

    /**
     * Initially ResultCategory objects existed as elements on their own and as such had a Repository for getting them from the database
     * However, in the improved design, ResultCategory objects are part of the School object
     * @param schoolId
     * @param clazz
     * @return
     */
    @Cacheable("SchoolResultCategoriesCache")
    public List<ResultCategory> getResultCategories(String schoolId, String clazz) {

        School school = getSchool(schoolId);

        Optional<ResultTemplate> resultTemplate = school.getSetting().getResultConfiguration()
                .getResultTemplates()
                .stream()
                .filter(r -> r.getClasses().contains(clazz))
                .findFirst();

        final List<ResultCategory> resultCategoriesBeingScored = new ArrayList<>();

        // result categories we return are only the ones that results are keyed in for
        resultTemplate.ifPresent(r -> resultCategoriesBeingScored.addAll(r.getResultCategories()
            .stream()
                .filter(rc -> rc.isResultKeyedIn())
                .collect(Collectors.toList())
        ));

        return resultCategoriesBeingScored;

    }

    /**
     * This method is used to get only the result categories that are parent result categories
     * @param schoolId
     * @param clazz
     * @return
     */
    @Cacheable("SchoolParentResultCategoriesCache")
    public List<ResultCategory> getParentResultCategories(String schoolId, String clazz) {

        List<ResultCategory> resultCategories = getAllResultCategories(schoolId, clazz);

        List<ResultCategory> parentResultCategories =
                resultCategories
                        .stream()
                .filter(rc -> rc.getParentCategory().equals(rc.getName()))
                .collect(Collectors.toList());

        return parentResultCategories;
    }

    /**
     * This method is used to get all result categories both parents and children
     * @param schoolId
     * @param clazz
     * @return
     */
    public List<ResultCategory> getAllResultCategories(String schoolId, String clazz) {
        School school = getSchool(schoolId);

        Optional<ResultTemplate> resultTemplate = school.getSetting().getResultConfiguration()
                .getResultTemplates()
                .stream()
                .filter(r -> r.getClasses().contains(clazz))
                .findFirst();

        final List<ResultCategory> allResultCategories = new ArrayList<>();

        resultTemplate.ifPresent(r -> allResultCategories.addAll(r.getResultCategories()
                .stream()
                .collect(Collectors.toList())
        ));

        return allResultCategories;
    }

    /**
     * Method to get a specific result category - specific to a school + class
     * @param schoolId
     * @param clazz
     * @param name
     * @return
     */
    @Cacheable("ResultCategoryCache")
    public ResultCategory getResultCategory(String schoolId, String clazz, String name) {
        List<ResultCategory> resultCategories =
                getAllResultCategories(schoolId, clazz)
                        .stream()
                        .filter(rc -> rc.getName().equals(name)).collect(Collectors.toList());

        if (resultCategories.size() > 1) {
            throw new MultipleResultCategoriesException("Multiple Result Categories exist for " + name);

        } else if (resultCategories.size() == 1) {
            return resultCategories.get(0);

        } else {
            throw new ResultCategoryNotFoundException("Found no Result Category called " + name);
        }
    }

    @CacheEvict({"SchoolsCache", "SchoolResultCategoriesCache", "SchoolParentResultCategoriesCache, ResultTemplatesCache, " +
            "ResultCategoryCache", "ResultTemplatesCache"})
    public School save(School school) {
        return schoolRepository.save(school);
    }

    @CacheEvict(value = {"SchoolsCache", "SchoolResultCategoriesCache", "SchoolParentResultCategoriesCache, " +
            "ResultTemplatesCache", "ResultCategoryCache", "ResultTemplatesCache"},
            allEntries = true)
    public void deleteAll() {
        schoolRepository.deleteAll();
    }
}
