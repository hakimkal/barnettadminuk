package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.payment.PaymentRecord;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 10/11/15.
 */

@Service
public class ParentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    public ParentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Parent> findAll() {
        return studentRepository.findAll().stream()
                .map(s -> s.getParents()).flatMap(p -> p.stream()).collect(Collectors.toList());
    }

    public Parent getParentByUsername(String username) {
        return studentRepository.findOneByParentsUsername(username).getParents().stream()
                .filter(p -> p.getUsername().equals(username)).findFirst().get();
    }

    public boolean authenticate(Parent parent, String username, String password) {
        return parent.getUsername().equals(username) && parent.getPassword().equals(password);
    }

    public Parent findParentById(String userId) {
        Parent parent = (Parent) userRepository.findOne(userId);
        List<Student> children = studentRepository.findByParentId(parent.getId());

        parent.setChildren(children);

        return parent;

    }


}
