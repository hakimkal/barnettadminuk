package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.results.Result;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentResultRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 01/03/2016.
 */

@Service
public class ResultService {

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentResultRepository studentResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    /**
     * This method is very import as it is used to determine if All Subject Results have been vetted.
     * This method is used to check if a Subject Teacher has vetted all their results before submitting them
     * NOTE:
     *      If no results have been entered it fails the vetting process
     *
     * @param subjectId
     * @param className
     * @return
     */
    public boolean areSubjectResultsInClassVetted(String subjectId, String className, String session, String term, String schoolId) {
        boolean vetted = false;

        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository.findBySessionAndTermAndClazzAndSubjectAndSchoolId(session, term, className, subjectId, schoolId);

            List<StudentSubjectResult> studentSubjectResultsVetted = studentSubjectResults.stream()
                    .filter(r -> r.getStatus() == (Result.ResultStatus.SUBJECT_TEACHER_VETTED))
                    .collect(Collectors.toList());

            if (!studentSubjectResultsVetted.isEmpty()) {
                if (studentSubjectResults.size() == studentSubjectResultsVetted.size()) {
                    vetted = true;
                }
            }

        return vetted;
    }

    /**
     * This method is also important as it is used to determine if a class teacher can submit results for final vetting
     *  In summary what it does is check to see if a class teacher has entered all comments on student results;
     *  and if they have, they are allowed to submit results
     * @param className
     * @param session
     * @param term
     * @param schoolId
     * @return
     */
    public boolean doAllStudentsResultsHaveComments(String className, String session, String term, String schoolId) {
        boolean allCommentedOn = false;

        List<Student> students = studentRepository.findByClazzAndSchoolIdAndExemptFromResultProcessing(className, schoolId, false);
        int studentCount = students.size();

        List<StudentResult> studentResults =
                studentResultRepository.findBySchoolIdAndClazzAndSessionAndTerm(schoolId, className, session, term);


        List<StudentResult> studentResultsWithComments = studentResults.stream()
                .filter(r -> !StringUtils.isEmpty(r.getTeacherComments()))
                .collect(Collectors.toList());

        if (!studentResultsWithComments.isEmpty()) {
            if ((studentCount == studentResults.size()) && (studentCount == studentResultsWithComments.size())) {
                allCommentedOn = true;
            }
        }

        return allCommentedOn;
    }
}
