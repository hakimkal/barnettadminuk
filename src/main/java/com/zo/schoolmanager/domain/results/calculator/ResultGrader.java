package com.zo.schoolmanager.domain.results.calculator;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by ozo on 30/07/2016.
 */
public class ResultGrader {

    @Id
    private String id;

    private List<String> grades;
    private List<String> classes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }
}
