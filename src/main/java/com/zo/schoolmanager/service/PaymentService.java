package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.payment.PaymentItem;
import com.zo.schoolmanager.domain.payment.PaymentRecord;
import com.zo.schoolmanager.repository.PaymentItemRepository;
import com.zo.schoolmanager.repository.PaymentRecordRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Eze on 15/03/2016.
 */

@Service
public class PaymentService {

    @Autowired
    private PaymentRecordRepository paymentRecordRepository;

    @Autowired
    private StudentRepository studentRepository;
    private final Logger LOG = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    private PaymentItemRepository paymentItemRepository;


    public List<Student> getStudentsWhoHaveNotPaid(String paymentItemId, String session, String term, String clazz,String schoolId){
        List<Student> studentsWhoHaveNotPaid = new ArrayList<>();
        List<PaymentRecord> paymentRecord = paymentRecordRepository.findByPaymentItemIdAndSessionAndTerm(paymentItemId, session, term);
        final List <Student> allStudentsInAClazz = studentRepository.findByClazzAndSchoolId(clazz,schoolId);

        if (allStudentsInAClazz != null) {
            Set<String> studentWhoHavePaid = paymentRecord.stream()
                    .map(PaymentRecord::getStudentId)
                    .collect(Collectors.toSet());

            allStudentsInAClazz.stream()
                    .forEach(student -> {
                        if (!(studentWhoHavePaid.contains(student.getId()))) {
                            studentsWhoHaveNotPaid.add(student);
                        }
                    });
        }
        else {
            LOG.warn("Clazz not found with clazzid: "+ clazz);
        }

        return studentsWhoHaveNotPaid;
    }


    public List<Student> getStudentsWhoHavePaid(String paymentItemId, String session, String term){
        final List<Student> studentsWhoHavePaid = new ArrayList<>();
        final List<PaymentRecord> paymentRecords = paymentRecordRepository.findByPaymentItemIdAndSessionAndTerm(paymentItemId,session,term);
        Set<String> studentIDs = paymentRecords.stream()
                .map(PaymentRecord::getStudentId)
                .collect(Collectors.toSet());

        final List<Student> allStudents = studentRepository.findAll();
        if(allStudents !=null) {
            allStudents.stream()
                    .forEach(student -> {
                        if (studentIDs.contains(student.getId())) {
                            studentsWhoHavePaid.add(student);
                        }
                    });

        }
        return studentsWhoHavePaid;
    }

    public void addClassToPaymentItem(String paymentId, String className){
        PaymentItem returnedPaymentItem = paymentItemRepository.findOne(paymentId);
        List<String> classes = returnedPaymentItem.getClasses();
        classes.add(className);
        returnedPaymentItem.setClasses(classes);
        paymentItemRepository.save(returnedPaymentItem);
    }

    public boolean hasStudentPaid(String studentId, String paymentItemId, String session, String term){
        final List<PaymentRecord> paymentRecords = paymentRecordRepository.findByPaymentItemIdAndSessionAndTerm(paymentItemId,session,term);
        return  paymentRecords.stream().anyMatch(paymentRecord -> paymentRecord.getStudentId().equalsIgnoreCase(studentId));
    }


    public BigDecimal howMuchIsPaid(String studentId,String session, String term){
        BigDecimal totalPaid = BigDecimal.ZERO;
        List<BigDecimal> listOfPaidAmount = new ArrayList<>();
        List<PaymentRecord> paymentRecords = paymentRecordRepository.findByStudentIdAndSessionAndTerm(studentId,session,term);
        final Set<String> paymentItemIDs = paymentRecords.stream()
                .map(PaymentRecord::getPaymentItemId)
                .collect(Collectors.toSet());
        paymentItemIDs.stream()
                .forEach(s -> {
                    PaymentItem paymentItem = paymentItemRepository.findOne(s);
                    if(paymentItem != null){
                        listOfPaidAmount.add(paymentItem.getAmount());
                    }
        });
        for (BigDecimal bigDecimal : listOfPaidAmount){
            totalPaid = totalPaid.add(bigDecimal);
        }
        return totalPaid;
    }


    public BigDecimal howMuchIsOwed(String studentId, String session, String term, String schoolId){
        BigDecimal totalOwed = BigDecimal.ZERO;
        List<BigDecimal> listOfOwedAmount = new ArrayList<>();

        final List<PaymentItem> paymentItems = paymentItemRepository.findByClassesInAndSchoolId(studentRepository.findOne(studentId).getClazz(), schoolId);
        paymentItems.stream().forEach(paymentItem -> listOfOwedAmount.add(paymentItem.getAmount()));

        for(BigDecimal bigDecimal: listOfOwedAmount){
            totalOwed = totalOwed.add(bigDecimal);
        }
        totalOwed = totalOwed.subtract(howMuchIsPaid(studentId,session,term));

        return totalOwed;
    }

    public List<PaymentItem> getItemsPaidFor(String studentId,String session, String term){
        List<PaymentItem> listOfItemsPaidFor = new ArrayList<>();
        List<PaymentRecord> paymentRecords = paymentRecordRepository.findByStudentIdAndSessionAndTerm(studentId,session,term);
        final Set<String> paymentItemsId = paymentRecords.stream()
                .map(PaymentRecord::getPaymentItemId)
                .collect(Collectors.toSet());

        paymentItemsId.stream().forEach(s -> listOfItemsPaidFor.add(paymentItemRepository.findOne(s)));

        return listOfItemsPaidFor;
    }

    public List<PaymentRecord> getPaymentRecords(String studentId, String session, String term){

        return  paymentRecordRepository.findByStudentIdAndSessionAndTerm(studentId, session,term);

    }

    public List<PaymentRecord> getPaymentRecords(String studentId) {
        return paymentRecordRepository.findByStudentId(studentId);
    }


}
