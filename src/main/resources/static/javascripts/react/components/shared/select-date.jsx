var React = require('react');
var DatePicker = require('react-datepicker');
var moment = require('moment');

module.exports = React.createClass({

    getInitialState: function() {
        var date = moment();
        if (this.props.startDate != undefined && this.props.startDate != '') {
            date = moment(this.props.startDate);
        }
        return {
            startDate: date
        };
    },

    render: function() {
        return (
            <DatePicker placeholder="Click to select date"
                        onChange={this.handleDateChange}
                        selected={this.state.startDate}
                        dateFormat="DD-MM-YYYY"
                        className='form-control'
            />
        )
    },

    handleDateChange: function(date) {
        this.setState({
            startDate: date
        });
    },
});


