var React = require('react');
var Select = require('react-select');
var ClassStore = require('../../stores/class-store');
var Reflux = require('reflux');


module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(ClassStore, 'onUpdateClasses')
    ],

    getInitialState: function() {
        return {
            classes : []
        }
    },


    render: function() {
        var options = this.state.classes.map(function(clazz) {
            return (
                { value: clazz.name, label: clazz.name }
            )
        }.bind(this));

        return (
            <Select
                name="select-multiple-classes"
                options={options}
                multi
                joinValues={true}
                delimeter=","
                onChange={this.logChange}
            />
        )
    },

    onUpdateClasses: function () {
        this.setState({classes: ClassStore.classes});
    },

    // tag is used by the hack - linked to shouldComponentUpdate
    getDefaultProps: function() {
        return {
            tags: []
        }
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        // Using this hack to fix the problem where on re-render the react-select component is clearing out what the user has selected
        return (this.props.tags.length == 0 || nextProps.tags.length !== this.props.tags.length)
    },

    logChange: function(val) {
        // this is used by the hack
        this.props.tags = [];
        this.props.tags.push(val.split(","));
    }

})

