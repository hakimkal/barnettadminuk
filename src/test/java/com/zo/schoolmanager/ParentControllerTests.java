package com.zo.schoolmanager;

import com.jayway.restassured.RestAssured;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.StudentRepository;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;

/**
 * Created by ozo on 05/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@IntegrationTest("server.port:0")
public class ParentControllerTests extends TestSetup {

    @Autowired
    private StudentRepository studentRepository;

    @Value("${local.server.port}")
    int port;

    Parent p1, p2, p3;
    Student s1;

    @Before
    public void setup() {

        p1 = father();
        p2 = mother();
        p3 = father();

        s1 = student();

        s1.setParents(Arrays.asList(p1, p2, p3));

        studentRepository.save(s1);

        RestAssured.port = port;
    }

    @Test
    public void canFetchParents() {

        when().
            get("/parents").
        then().
            body("firstName", hasItems(p1.getFirstName(), p2.getFirstName(), p3.getFirstName()));

        when().
            get("/parents").
        then().
            statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void canFetchSingleParent() {

        Parent firstParent = s1.getParents().get(0);

        when().
            get("/parent/{:id}", s1.getId()).
        then().
            body("firstName", equalTo(firstParent.getFirstName())).
        and().
            body("lastName", equalTo(firstParent.getLastName())).
        and().
            body("primaryPhone", equalTo(firstParent.getPrimaryPhone()));

//        get("/parent/{:id}", s1.getId()).then().assertThat().body("firstName", equalTo("Jane"));
    }

    @After
    public void cleanUp() {
         removeAllParentsRecords();
    }

    private void removeAllParentsRecords() {
        studentRepository.findAll().forEach(s -> studentRepository.delete(s));
    }

}