package com.zo.schoolmanager.controller;

import com.zo.schoolmanager.domain.api.results.ResultFilter;
import com.zo.schoolmanager.domain.api.results.ResultItemDTO;
import com.zo.schoolmanager.domain.api.results.ResultItemSaveRequest;
import com.zo.schoolmanager.service.ResultItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 23/01/2016.
 */

@RestController
public class ResultItemController extends BaseController {

    @Autowired
    private ResultItemService resultItemService;

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/result-items", method = RequestMethod.POST)
    public List<ResultItemDTO> getResultItems(@RequestBody ResultFilter resultFilter) {

        if (resultFilter.hasMissingField()) {
            return new ArrayList<>();
        }

        return resultItemService.getResultItemDTOs(resultFilter, loggedInUser().getSchoolId());
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/result-items-save", method = RequestMethod.POST)
    public List<ResultItemDTO> saveResultItems(@RequestBody ResultItemSaveRequest saveRequest) {

        List<ResultItemDTO> savedResultItems = new ArrayList<>();

        if (saveRequest != null) {
            savedResultItems = resultItemService.saveResultItemDTOs(saveRequest, loggedInUser().getSchoolId());
        }

        return savedResultItems;
    }
}
