package com.zo.schoolmanager.domain.school;

import com.zo.schoolmanager.domain.constants.SchoolType;

/**
 * Created by ozo on 27/11/15.
 */
public class Setting {

    private SchoolType schoolType;
    private boolean enforcesMaxAndMinSubjects;
    private ResultConfiguration resultConfiguration = new ResultConfiguration();
    private AccountConfiguration accountConfiguration = new AccountConfiguration();


    public SchoolType getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(SchoolType schoolType) {
        this.schoolType = schoolType;
    }

    public boolean isEnforcesMaxAndMinSubjects() {
        return enforcesMaxAndMinSubjects;
    }

    public void setEnforcesMaxAndMinSubjects(boolean enforcesMaxAndMinSubjects) {
        this.enforcesMaxAndMinSubjects = enforcesMaxAndMinSubjects;
    }

    public ResultConfiguration getResultConfiguration() {
        return resultConfiguration;
    }

    public void setResultConfiguration(ResultConfiguration resultConfiguration) {
        this.resultConfiguration = resultConfiguration;
    }

    public AccountConfiguration getAccountConfiguration() {
        return accountConfiguration;
    }

    public void setAccountConfiguration(AccountConfiguration accountConfiguration) {
        this.accountConfiguration = accountConfiguration;
    }
}
