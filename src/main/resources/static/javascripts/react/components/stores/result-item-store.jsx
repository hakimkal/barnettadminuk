var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getResultItems: function(filter) {
        return Api.post('/result-items', filter).then(function(data) {
            var response = {
                resultItems: data,
                fromSave: false
            };

            this.response = response;
            this.trigger("updateResultItems", response);
        }.bind(this));
    },


    saveResultItems: function(resultItems) {
        return Api.post('/result-items-save', resultItems).then(function(data) {
            var response = {
                resultItems: data,
                fromSave: true
            };

            this.response = response;
            this.trigger("updateResultItems", response);
        }.bind(this));
    }

});