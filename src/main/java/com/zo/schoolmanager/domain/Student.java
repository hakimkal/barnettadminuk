package com.zo.schoolmanager.domain;

import com.zo.schoolmanager.domain.constants.Sex;
import com.zo.schoolmanager.domain.users.Parent;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 06/11/15.
 */

@Document(collection = "students")
public class Student {

    public Student() {}

    public Student(String firstName, String surname, Sex sex, LocalDate dateOfBirth, String nationality,
                   String stateOfOrigin, String address, String schoolId) {

        this.firstName = firstName;
        this.surname = surname;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
        this.stateOfOrigin = stateOfOrigin;
        this.address = address;
        this.schoolId = schoolId;
    }

    public Student(String firstName, String surname, Sex sex, LocalDate dateOfBirth, String nationality,
                   String stateOfOrigin, String address, String schoolId, String className) {

        this(firstName, surname, sex, dateOfBirth, nationality, stateOfOrigin, address, schoolId);
        this.clazz = className;

    }



    @Id
    private String id;
    private String schoolId;
    private String surname;
    private String admissionNumber;
    private String firstName;

    private Sex sex;

    private String middleName;

    private LocalDate dateOfBirth;

    private String placeOfBirth;
    private String religion;
    private String nationality;
    private String stateOfOrigin;
    private String lga;

    private String address;

    private List<String> parentIds = new ArrayList<>();

    @Transient
    private List<Parent> parents = new ArrayList<Parent>();
    @Transient
    private List<Parent> guardians = new ArrayList<Parent>();

    private boolean fromOtherSchool;
    private String otherSchoolName;
    private String reasonForLeavingSchool;
    private String previousClass;
    private String proposedClass;
    private boolean isBoarder;
    private boolean busServices;
    private boolean lunchForChild;
    private boolean paidSchoolFees;
    private String clazz;

    private LocalDateTime lastUpdated;
    private LocalDateTime created = LocalDateTime.now();

    private boolean deleted = false;
    private String reasonDeleted;
    private boolean exemptFromResultProcessing = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAdmissionNumber() {
        return admissionNumber;
    }

    public void setAdmissionNumber(String admissionNumber) {
        this.admissionNumber = admissionNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getStateOfOrigin() {
        return stateOfOrigin;
    }

    public void setStateOfOrigin(String stateOfOrigin) {
        this.stateOfOrigin = stateOfOrigin;
    }

    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Parent> getParents() {
        return parents;
    }

    public void setParents(List<Parent> parents) {
        this.parents = parents;
    }

    public List<Parent> getGuardians() {
        return guardians;
    }

    public void setGuardians(List<Parent> guardians) {
        this.guardians = guardians;
    }

    public boolean isFromOtherSchool() {
        return fromOtherSchool;
    }

    public void setFromOtherSchool(boolean fromOtherSchool) {
        this.fromOtherSchool = fromOtherSchool;
    }

    public String getOtherSchoolName() {
        return otherSchoolName;
    }

    public void setOtherSchoolName(String otherSchoolName) {
        this.otherSchoolName = otherSchoolName;
    }

    public String getReasonForLeavingSchool() {
        return reasonForLeavingSchool;
    }

    public void setReasonForLeavingSchool(String reasonForLeavingSchool) {
        this.reasonForLeavingSchool = reasonForLeavingSchool;
    }

    public String getPreviousClass() {
        return previousClass;
    }

    public void setPreviousClass(String previousClass) {
        this.previousClass = previousClass;
    }

    public String getProposedClass() {
        return proposedClass;
    }

    public void setProposedClass(String proposedClass) {
        this.proposedClass = proposedClass;
    }

    public boolean isBoarder() {
        return isBoarder;
    }

    public void setIsBoarder(boolean isBoarder) {
        this.isBoarder = isBoarder;
    }

    public boolean isBusServices() {
        return busServices;
    }

    public void setBusServices(boolean busServices) {
        this.busServices = busServices;
    }

    public boolean isLunchForChild() {
        return lunchForChild;
    }

    public void setLunchForChild(boolean lunchForChild) {
        this.lunchForChild = lunchForChild;
    }

    public boolean isPaidSchoolFees() {
        return paidSchoolFees;
    }

    public void setPaidSchoolFees(boolean paidSchoolFees) {
        this.paidSchoolFees = paidSchoolFees;
    }

    public void setBoarder(boolean boarder) {
        isBoarder = boarder;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getReasonDeleted() {
        return reasonDeleted;
    }

    public void setReasonDeleted(String reasonDeleted) {
        this.reasonDeleted = reasonDeleted;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public List<String> getParentIds() {
        return parentIds;
    }

    public void setParentIds(List<String> parentIds) {
        this.parentIds = parentIds;
    }

    public boolean isExemptFromResultProcessing() {
        return exemptFromResultProcessing;
    }

    public void setExemptFromResultProcessing(boolean exemptFromResultProcessing) {
        this.exemptFromResultProcessing = exemptFromResultProcessing;
    }
}

