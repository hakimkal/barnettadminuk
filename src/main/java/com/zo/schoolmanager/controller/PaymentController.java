package com.zo.schoolmanager.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ozo on 14/04/2016.
 */

@Controller
public class PaymentController extends BaseController {

    private static final String PAYMENT_PAGE = "payment";

    @RequestMapping("/payments/new-payment-item")
    @Secured({"ROLE_ADMIN", "ROLE_SUPER_ADMIN"})
    public String newPaymentItem(Model model) {

        model.addAttribute("page", PAYMENT_PAGE);
        model.addAttribute("user", loggedInUser());

        return "admin/payment/new-payment-item";

    }
}
