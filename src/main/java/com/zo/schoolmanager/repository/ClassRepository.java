package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.Clazz;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ozo on 22/01/2016.
 */
public interface ClassRepository extends MongoRepository<Clazz, String> {

    Clazz findOneByNameAndSchoolId(String name, String schoolId);
    List<Clazz> findByNameInAndSchoolId(List<String> names, String schoolId);
    List<Clazz> findBySchoolId(String schoolId);

}
