package com.zo.schoolmanager.service;

import com.zo.schoolmanager.commons.exceptions.configuration.MultipleResultCategoriesException;
import com.zo.schoolmanager.commons.exceptions.configuration.NoResultCategoryException;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.results.ResultFilter;
import com.zo.schoolmanager.domain.api.results.ResultItemDTO;
import com.zo.schoolmanager.domain.api.results.ResultItemSaveRequest;
import com.zo.schoolmanager.domain.results.*;
import com.zo.schoolmanager.domain.results.calculator.ResultCalculator;
import com.zo.schoolmanager.domain.results.calculator.ResultTemplate;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.repository.*;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by ozo on 24/01/2016.
 */

@Service
public class ResultItemService {

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private ClassResultRepository classResultRepository;

    private final Logger LOG = LoggerFactory.getLogger(ResultItemService.class);

    /**
     * This method gets the result items that are displayed on the front end.
     * It displays a list of all students in the class with no scores if no scores have been entered before.
     * Otherwise it gets the list of previously entered results
     *
     * @param filter
     * @return
     */
    public List<ResultItemDTO> getResultItemDTOs(ResultFilter filter, String schoolId) {
        List<ResultItemDTO> resultItemDTOs = new ArrayList<>();

        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository
                        .findBySessionAndTermAndClazzAndSubjectAndSchoolId(filter.getSession(), filter.getTerm(),
                                filter.getClazz(), filter.getSubject(), schoolId);

        // We want to get the result items for the category in filter.getResultCategory
        Predicate<ResultItem> hasResultCategory = ri -> ri.getResultCategory().equals(filter.getResultCategory());

        List<ResultItem> resultItemsForCategory = studentSubjectResults
                .stream()
                .map(ssr -> {
                    ssr.getResultItems().forEach(r -> r.setSubjectResultStatus(ssr.getStatus().toString()));
                    return ssr.getResultItems();
                })
                .flatMap(ri -> ri.stream())
                .filter(hasResultCategory)
                .collect(Collectors.toList());

        // If there are no existing result items for that category then create new result items for that category (note the result items are not saved)
        if (resultItemsForCategory.isEmpty()) {
            List<Student> studentsInClass = studentRepository.findByClazzAndSchoolId(filter.getClazz(), schoolId);

            // Get ResultTemplate for class and check the ResultCategories

            ResultCategory resultCategory = getResultCategory(filter.getResultCategory(), schoolId, filter.getClazz());

            resultItemDTOs.addAll(getResultItemDTOs(studentsInClass, filter.getResultCategory(), filter.getSubject(),
                    filter.getSession(), filter.getTerm(), resultCategory.getMaxScore(), schoolId));

        } else {
            resultItemDTOs.addAll(getResultItemDTOs(studentSubjectResults, filter));
        }

        List<ResultItemDTO> sortedResultItemDTOs =
                resultItemDTOs.stream()
                        .sorted((a, b) -> a.getStudentName().compareTo(b.getStudentName()))
                        .collect(Collectors.toList());

        return sortedResultItemDTOs;
    }


    /**
     * The ResultItemDTOs are used as wrappers for the front end. coming from the front end is simplified.
     * {
     * studentId,
     * score
     * }
     * This simple DTO has to be used as the basis for creating ResultItems which are persisted in the DB.
     *
     * @param saveRequest
     * @return
     */
    public List<ResultItemDTO> saveResultItemDTOs(ResultItemSaveRequest saveRequest, String schoolId) {

        ResultFilter filter = saveRequest.getFilter();

        List<StudentSubjectResult> studentSubjectResults =
                saveRequest.getResultItems()
                        .stream()
                        .map(ri -> {
                            ResultItem resultItem = getResultItem(ri, schoolId, saveRequest.getFilter().getClazz());

                            StudentSubjectResult studentSubjectResult =
                                    studentSubjectResultRepository.findByStudentIdAndSessionAndTermAndSubjectAndSchoolId
                                            (ri.getStudentId(), filter.getSession(), filter.getTerm(), filter.getSubject(), schoolId);

                            if (studentSubjectResult == null) {
                                studentSubjectResult =
                                        new StudentSubjectResult(ri.getStudentId(), filter.getSession(), filter.getTerm(),
                                                filter.getClazz(), filter.getSubject(), schoolId);
                            }

                            studentSubjectResult = replaceOrAddResultItems(studentSubjectResult, resultItem);
                            return studentSubjectResult;

                        }).collect(Collectors.toList());

        studentSubjectResultRepository.save(studentSubjectResults);

        return getResultItemDTOs(studentSubjectResults, filter);
    }

    /**
     * This method takes a list of ResultItems (some of which have sub result categories), and it returns a list of
     *  only parent result items. It does this by doing aggregation on result items that have sub result categories,
     *  summing up their score and creating a new Parent Result Item, assigning the aggregated score to the newly
     *  created Parent Result Item
     * @param nonNormalisedResultItems
     * @param schoolId
     * @param clazz
     * @return
     */
    public List<ResultItem> normaliseResultItems(List<ResultItem> nonNormalisedResultItems, String schoolId, String clazz) {

        // Group ResultItems by Subject > then go ahead and group by Parent Result Category

        Map<String, List<ResultItem>> resultItemMap =
                nonNormalisedResultItems.stream().collect(Collectors.groupingBy(ResultItem::getParentResultCategory));

        List<ResultItem> normalisedResultItems = new ArrayList<>();

        resultItemMap.forEach((k, v) -> {
            if (v.size() > 0) {
                ResultItem resultItem = v.get(0);
                resultItem.setResultCategory(resultItem.getParentResultCategory());
                resultItem.setScore(v.stream().collect(Collectors.summingDouble(ResultItem::getScore)));

                if (v.size() > 1) {
                    // Since it's a sub category result item, get the max score from the Parent ResultCategory
                    resultItem.setMaxScore(schoolService.getResultCategory(schoolId, clazz, resultItem.getResultCategory())
                            .getMaxScore());
                }

                normalisedResultItems.add(resultItem);
            }
        });

        return normalisedResultItems;
    }


    /**
     * Helper method that is used to either add a result item to subjectStudentResult or replace a result item in same object
     * ** This method is used by saveResultItemDTOs **
     *
     * @param studentSubjectResult
     * @param resultItem
     */
    private StudentSubjectResult replaceOrAddResultItems(StudentSubjectResult studentSubjectResult, ResultItem resultItem) {
        List<ResultItem> newResultItems = new ArrayList<>();

        // If the categories are the same update the score
        boolean found = false;
        for (ResultItem ri : studentSubjectResult.getResultItems()) {
            ResultItem resultItemToAdd = ri;
            if (ri.getResultCategory().equals(resultItem.getResultCategory())) {
                resultItemToAdd.setScore(resultItem.getScore());
                resultItemToAdd.setLastUpdated(LocalDateTime.now());
                if (resultItem.getScore() != null) {
                    resultItemToAdd.setStatus(ResultItem.ResultStatus.SCORE_SAVED);
                }

                resultItemToAdd.setSubjectResultStatus(studentSubjectResult.getStatus().toString());

                resultItemToAdd.setId(resultItem.getId());
                found = true;
            }
            newResultItems.add(resultItemToAdd);
        }

        if (!found) {
            newResultItems.add(resultItem);
        }

        studentSubjectResult.setResultItems(newResultItems);

        return studentSubjectResult;

    }

    /**
     * Helper method that is used to create a new ResultItem from the DTO passed from the web frontend
     *
     * @param resultItemDTO
     * @return
     */
    private ResultItem getResultItem(ResultItemDTO resultItemDTO, String schoolId, String className) {

        // We need the ResultCategory order for the front-end
        //ResultCategory resultCategory = resultCategoryRepository.findOneByNameAndSchoolId(resultItemDTO.getResultCategory(), schoolId);
        ResultCategory resultCategory = getResultCategory(resultItemDTO.getResultCategory(), schoolId, className);

        if (!StringUtils.isEmpty(resultItemDTO.getScore())) {
            ResultItem resultItem = new ResultItem(resultItemDTO.getResultCategory(), resultCategory.getParentCategory(), new Double(resultItemDTO.getScore()),
                    ResultItem.ResultStatus.SCORE_SAVED, resultCategory.getMaxScore());
            resultItem.setId(resultItemDTO.getKey());
            resultItem.setOrder(resultCategory.getOrder());
            return resultItem;

        } else {
            ResultItem resultItem = new ResultItem(resultItemDTO.getResultCategory(),
                    resultCategory.getParentCategory(),
                    ResultItem.ResultStatus.NO_SCORE, resultCategory.getMaxScore());
            resultItem.setId(resultItemDTO.getKey());
            resultItem.setOrder(resultCategory.getOrder());
            return resultItem;
        }
    }

    /**
     * Helper method to create new ResultItemDTOs that will be presented the first time a Teacher
     * visits the Result Entry Form
     *
     * @param students
     * @return
     */
    private List<ResultItemDTO> getResultItemDTOs(List<Student> students, String resultCategory, String subject,
                                                  String session, String term, Double maxScore, String schoolId) {

        ClassResult classResult = null;

        if (!students.isEmpty()) {
            String clazz = students.get(0).getClazz();

            classResult =
                    classResultRepository.findOneBySchoolIdAndClazzAndSessionAndTerm(schoolId, clazz, session, term);

        }

        final boolean resultProcessed = classResult != null ? true : false;

        return students.stream().map(s -> {
            ResultItemDTO resultItemDTO = new ResultItemDTO();
            resultItemDTO.setStudentId(s.getId());
            resultItemDTO.setResultCategory(resultCategory);
            resultItemDTO.setStudentName(s.getSurname() + " " + s.getFirstName() +
                    (s.getMiddleName() != null ? " " + s.getMiddleName() : ""));

            String id = ObjectId.get().toString();
            resultItemDTO.setKey(id);

            resultItemDTO.setMaxScore(maxScore);

            // Check the Status of SubjectClassResult to see if this result status is not null (So will have been vetted)
            StudentSubjectResult studentSubjectResult =
                    studentSubjectResultRepository.findByStudentIdAndSessionAndTermAndSubjectAndSchoolId(s.getId(), session,
                            term, subject, schoolId);

            if (studentSubjectResult != null && studentSubjectResult.getStatus() != Result.ResultStatus.NONE) {
                resultItemDTO.setVetted(true);
            }

            resultItemDTO.setProcessed(resultProcessed);


            return resultItemDTO;

        }).collect(Collectors.toList());

    }

    /**
     * Helper method that is used to get Results items for a Specific Subject (Subject already selected in
     * the passed in object) for a Specific Result Category - defined in the filter
     *
     * @param studentSubjectResults
     * @param filter
     * @return
     */
    private List<ResultItemDTO> getResultItemDTOs(List<StudentSubjectResult> studentSubjectResults, ResultFilter filter) {
        List<ResultItemDTO> resultItemDTOs = new ArrayList<>();

        studentSubjectResults.forEach(ssr -> {

            List<ResultItem> resultItems =
                    ssr.getResultItems().stream().filter(ri ->
                            ri.getResultCategory().equals(filter.getResultCategory())).collect(Collectors.toList());

            resultItems.forEach(ri -> {
                ResultItemDTO resultItemDTO = new ResultItemDTO();
                resultItemDTO.setStudentId(ssr.getStudentId());
                if (ri.getScore() != null) {
                    resultItemDTO.setScore(ri.getScore().toString());
                }

                resultItemDTO.setResultCategory(ri.getResultCategory());

                if (ri.getStatus() == ResultItem.ResultStatus.SCORE_SAVED) {
                    resultItemDTO.setSaved(true);
                }

                if (ssr.getStatus() != Result.ResultStatus.NONE) {
                    resultItemDTO.setVetted(true);
                }

                // Set a unique key to aid rendering in react
                resultItemDTO.setKey(ri.getId());

                resultItemDTO.setMaxScore(ri.getMaxScore());

                Student student = studentRepository.findOne(ssr.getStudentId());
                resultItemDTO.setStudentName(student.getSurname() + " " + student.getFirstName() +
                        (student.getMiddleName() != null ? " " + student.getMiddleName() : ""));
                resultItemDTOs.add(resultItemDTO);
            });

        });

        return resultItemDTOs;
    }

    /**
     * This method returns the a ResultCategory based on the name and schoolId provided.
     * Initially ResultCategory objects existed as elements on their own and as such had a Repository for getting them from the database
     * However, in the improved design, ResultCategory objects are part of the School object and as such need to be filtered out.
     * In order to improve performance we are caching the method to get School by id
     * @param name
     * @param schoolId
     * @return
     */
    private ResultCategory getResultCategory(String name, String schoolId, String className) {

        List<ResultCategory> allResultCategories = schoolService.getResultCategories(schoolId, className);

        List<ResultCategory> resultCategories =
                allResultCategories
                        .stream().filter(rc -> rc.getName().equals(name))
                        .collect(Collectors.toList());

        if (resultCategories!= null && resultCategories.size() == 1) {
            return resultCategories.get(0);

        } else if (resultCategories != null && resultCategories.size() > 1) {
            throw new MultipleResultCategoriesException("Multiple Result Categories found for " + name);

        } else {
            throw new NoResultCategoryException("Found no ResultCategory with name " + name);
        }
    }

}
