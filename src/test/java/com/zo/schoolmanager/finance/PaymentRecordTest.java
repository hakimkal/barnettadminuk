package com.zo.schoolmanager.finance;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.school.School;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.payment.PaymentItem;
import com.zo.schoolmanager.domain.payment.PaymentRecord;
import com.zo.schoolmanager.repository.PaymentItemRepository;
import com.zo.schoolmanager.repository.PaymentRecordRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eze on 08/03/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class PaymentRecordTest {

    private PaymentRecord paymentRecord;


    @Autowired
    PaymentItemRepository paymentItemRepository;
    private PaymentItem paymentItem;

    @Autowired
    private PaymentRecordRepository paymentRecordRepository;

    @Test
    public void testCanCreatePaymentRecord(){
        PaymentRecord paymentRecord = new PaymentRecord();
        Assert.assertNotNull(paymentRecord);
    }


    @Test
    public void testCanCreatePaymentRecordWithFields(){
        Student student = new Student();
        PaymentItem paymentItem = new PaymentItem();
        PaymentRecord paymentRecord = new PaymentRecord(student.getId(), "Uju","Chioma","07708934537",
                "419","GTBank","2017/2018","First Term", LocalDate.of(2018,7,2),paymentItem.getId());

        Assert.assertEquals(paymentRecord.getBankPaidTo(),"GTBank");
        Assert.assertEquals(paymentRecord.getContactNumber(),"07708934537");
    }


    @Test
    public void testSavePaymentRecord(){
        Student student = new Student();
        School school = new School();
        List<String> classes = new ArrayList<>();
        classes.add("classJSS1A");
        classes.add("classJSS2B");
        classes.add("classJSS3C");
        List<String>terms = new ArrayList<>();
        terms.add("First Term");
        terms.add("Second Term");
        terms.add("Last Term");
        BigDecimal amount = new BigDecimal("3000.00");
        paymentItem = new PaymentItem("Excurtion", "This is an excurtion to see the crowned Queen of the land",
                LocalDate.of(2016,9,25),school.getId(),classes,amount,"2016/2017",terms);
        PaymentItem savedPaymentItem = paymentItemRepository.save(paymentItem);

        paymentRecord = new PaymentRecord(student.getId(),"Chris","Fisher", "07709563559",
                "911","Access Bank","2016/2017","Second Term",LocalDate.of(2017,6,4),savedPaymentItem.getId());

        PaymentRecord savedPaymentRecord = paymentRecordRepository.save(paymentRecord);
        Assert.assertNotNull(savedPaymentRecord);
        Assert.assertNotNull(savedPaymentRecord.getId());

    }


}
