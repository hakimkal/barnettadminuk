var React = require('react')

module.exports = React.createClass({

    getInitialState: function() {
        return {data: []}
    },

    loadData: function() {
        $.ajax({
            url: this.props.url,
            success: function(data) {
                this.setState({data: data})
            }.bind(this)
        });
    },

    componentWillMount: function() {
        this.loadData();
    },

    render: function() {

        var rows = this.state.data.map(function(header) {
            return <th>{header}</th>
        });

        return (
            <tr>
                {rows}
                <th>Actions</th>
            </tr>

        )
    }
});


