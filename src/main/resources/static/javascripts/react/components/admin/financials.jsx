var React = require('react');
var ReactDom = require('react-dom');
var Tile = require('./tile');

var tiles = [
    {
        title: "view payment items",
        href: "",
        icon: "fa fa-binoculars",
        page: "col-xs-4 col-sm-5 col-md-4 page-user"
    },
    {
        title: "add payment item",
        href: "",
        icon: "fa fa-plus",
        page: "col-xs-4 col-sm-5 col-md-4 page-statistics"
    },
    {
        title: "edit payment item",
        href: "",
        icon: "fa fa-edit",
        page: "col-xs-4 col-sm-4 col-md-4 page-support"
    }
];


var Financial = React.createClass({


    render: function () {
        var arrayOfTiles = [];
        tiles.map(function (value) {
            if(value != undefined){
                arrayOfTiles.push(<Tile title = {value.title} icon = {value.icon} href = {value.href} page={value.page}/>);
            }
        });

        return (
            <div className="row panel-quick-page">
                {arrayOfTiles}
            </div>
        )
    }

});

ReactDom.render(<Financial/>, document.getElementById('financials'));