var React = require('react');
var ReactDOM = require('react-dom');
var Actions = require('../actions');
var Reflux = require('reflux');
var StudentStore = require('../stores/student-store');
var ClassResultStore = require('../stores/class-result-store');
var ResultHelperStore = require('../stores/result-helper-store');
var ResultView = require('../shared/result-view');
var Modal = require('react-bootstrap/lib/Modal');

var ViewStudentResult = React.createClass({

    mixins: [
        Reflux.listenTo(ResultHelperStore, "onUpdateStudentResultHeaders"),
        Reflux.listenTo(StudentStore, "onUpdateStudentResultsData"),
        Reflux.listenTo(ClassResultStore, "onSaveTeacherComments")
    ],

    getInitialState: function () {
        return {
            results: [],
            headers: [],
            student: {'surname': '', 'firstName': ''},
            teacherComments: '',
            comments: true,
            allResultsEntered: false,
            teacherCommentsSaved: false,
            success: false,
            failure: false,
            disabled: false
        }
    },

    render: function () {

        var headers = this.state.headers.map(function (rc) {
            return (
                <th className="text-left" key={rc.key}>{rc.shortName}</th>
            )
        }.bind(this));

        var body = this.state.results.map(function (result) {
            return <ResultView result={result} key={result[0].key}/>
        }.bind(this));

        return (
            <div className="col-md-12">
                <div className="panel">
                    <div className="panel-heading">
                        <h3> {this.state.student.surname} {this.state.student.firstName}'s Result</h3>
                        { this.state.allResultsEntered === false ?
                            <h4 className="panel-title-red">The results are incomplete!</h4>
                            :
                            null
                        }
                    </div>

                    <div className="panel-body">

                        <div className="form-group col-md-9">
                            <div className="table-responsive">
                                <table className="table table-bordered table-success nomargin">
                                    <thead>
                                    <tr>
                                        {headers}
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {body}
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="panel">
                    <div className="panel-heading">
                        <h4>Teacher's Comments</h4>
                    </div>

                    <div className="panel-body">

                        <div className="form-group col-md-9">
                            <div className="table-responsive">
                                { this.state.comments ?
                                    <div className="form-group col-md-9">
                                        <div>
                                            <textarea className="form-control" ref="teacherComments" rows="4"
                                                      placeholder="Teacher's Comments"
                                                      disabled={ this.state.teacherCommentsSaved == true}
                                                      value={this.state.teacherComments} />
                                        </div>
                                    </div>
                                    :

                                    null
                                }
                                <div className="form-group col-md-9">
                                    <button className="btn btn-primary btn-quirk" onClick={this.saveComment} disabled={this.state.disabled}>Save
                                        Comment
                                    </button>
                                    &nbsp;
                                    <button className="btn btn-warning btn-quirk" onClick={this.toggleEdit} disabled={this.state.disabled}>Edit
                                        Comment
                                    </button>
                                    &nbsp;
                                    <a href="/results/view-class-results" className="btn btn-success btn-quirk">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    {
                        /* Todo - refactor this logic used across the system into mixin*/
                        <Modal show={this.state.success} onHide={this.close}>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    {'Comments'}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="alert alert-info fade in nomargin">
                                    <h4>Info</h4>
                                    <p className="lead">{'Your Comments have been saved!'}</p>
                                </div>
                            </Modal.Body>
                        </Modal>

                    }


                    {
                        /* Todo - refactor this logic used across the system into mixin*/
                        <Modal show={this.state.failure} onHide={this.close}>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    {'Comments'}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="alert alert-info fade in nomargin">
                                    <h4>Info</h4>
                                    <p className="lead">{'There was a problem saving your comments'}</p>
                                </div>
                            </Modal.Body>
                        </Modal>

                    }

                </div>
            </div>
        )
    },

    componentWillMount: function () {

        Actions.getStudentResultsData(document.getElementById("studentId").value,
            document.getElementById("session").value,
            document.getElementById("term").value, document.getElementById("clazz").value)

    },

    onUpdateStudentResultsData: function () {
        this.setState({
            student: StudentStore.data.student,
            headers: StudentStore.data.headers,
            teacherComments: StudentStore.data.teacherComments,
            results: StudentStore.data.results,
            teacherCommentsSaved: StudentStore.data.teacherComments != '' && StudentStore.data.teacherComments != undefined,
            disabled: this.shouldBeDisabled(StudentStore.data.resultStatus)
        });
    },

    saveComment: function () {

        // Save only if result is not disabled and the comment box is enabled
        if (!this.state.disabled) {
            if (!this.state.teacherCommentsSaved){
                Actions.saveTeacherComments({
                    'studentId': document.getElementById("studentId").value,
                    'session': document.getElementById("session").value,
                    'term': document.getElementById("term").value,
                    'teacherComments': ReactDOM.findDOMNode(this.refs.teacherComments).value
                });
            }
        }
    },

    onSaveTeacherComments: function () {
        var success = ClassResultStore.response.status == 'success';
        if (success) {
            this.setState({success: true, teacherCommentsSaved:true});
        } else {
            this.setState({failure: true});
        }

    },

    toggleEdit: function() {
        this.setState({teacherCommentsSaved: !this.state.teacherCommentsSaved});
    },

    close: function () {
        this.setState({success: false, failure: false});
    },

    shouldBeDisabled: function(status) {
        switch (status) {
            case undefined:
                return false;
            case '':
                return false;
            case 'NONE':
                return false;
            case 'CLASS_TEACHER_COMMENTED':
                return false;
            default:
                return true;
        }
    }


});

ReactDOM.render(<ViewStudentResult />, document.getElementById("view-student-result"));