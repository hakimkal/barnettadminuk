package com.zo.schoolmanager.api.result;

import com.zo.schoolmanager.controller.BaseController;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.api.ApiResponse;
import com.zo.schoolmanager.domain.api.results.VetSubjectDTO;
import com.zo.schoolmanager.domain.results.ClassResult;
import com.zo.schoolmanager.domain.results.Result;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.domain.results.ClassSubjectResult;
import com.zo.schoolmanager.repository.ClassResultRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import com.zo.schoolmanager.repository.SubjectClassResultRepository;
import com.zo.schoolmanager.service.ResultService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 02/03/2016.
 */
@RestController
public class ResultApiController extends BaseController {

    @Autowired
    private SubjectClassResultRepository subjectClassResultRepository;

    @Autowired
    private ClassResultRepository classResultRepository;

    @Autowired
    private ResultService resultService;

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/submit-to-class-teacher", method = RequestMethod.GET)
    public ApiResponse submitResultsToClassTeacher(@RequestParam String subjectId, @RequestParam String className,
                                                   @RequestParam String session, @RequestParam String term, Model model) {

        ApiResponse success = success();
        ApiResponse failure = failure();

        model.addAttribute("user", loggedInUser());

        boolean submitted = false;

        // Check if the teacher has submitted the results before
        boolean submittedBefore =
                subjectClassResultRepository.findOneBySubjectIdAndClassNameAndSessionAndTerm(subjectId, className, session, term)
                        .filter(r -> r.getStatus() == Result.ResultStatus.SUBJECT_TEACHER_SUBMITTED)
                        .map(r -> true)
                        .orElse(false);

        if (submittedBefore) {
            success.setMessage("Results have previously been submitted to the Class Teacher");
        } else  {

            if (resultService.areSubjectResultsInClassVetted(subjectId, className, session, term, loggedInUser().getSchoolId())) {

                // Create new SubjectClassResult
                ClassSubjectResult classSubjectResult = new ClassSubjectResult(subjectId, className, session, term,
                        loggedInUser().getSchoolId());
                subjectClassResultRepository.save(classSubjectResult);
                submitted = true;
            } else {
                failure.setMessage("All Results have not been vetted! " +
                        "You must vet all results before you can submit them");
            }
        }

        if (submittedBefore || submitted) {
            return success;
        } else {
            return failure;
        }
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/submit-for-final-vetting", method = RequestMethod.GET)
    public ApiResponse submitResultsForFinalVetting(@RequestParam String className, @RequestParam String session,
                                                    @RequestParam String term, Model model) {
        ApiResponse success = success();
        ApiResponse failure = failure();

        model.addAttribute("user", loggedInUser());

        boolean submittedBefore = false;
        boolean saved = false;

        // Check if the teacher has submitted the results before
        ClassResult classResult = classResultRepository.findOneBySchoolIdAndClazzAndSessionAndTerm(loggedInUser().getSchoolId(), className, session, term);

        if (classResult != null) {
            if (classResult.getStatus() == Result.ResultStatus.CLASS_TEACHER_SUBMITTED ||
                    classResult.getStatus() == Result.ResultStatus.HOD_VETTED) {
                submittedBefore = true;
            }
        }

        if (submittedBefore) {
            success.setMessage("Results have previously been submitted");
        } else  {

            if (resultService.doAllStudentsResultsHaveComments(className, session, term, loggedInUser().getSchoolId())) {
                classResult = new ClassResult(className, session, term, Result.ResultStatus.CLASS_TEACHER_SUBMITTED,
                        loggedInUser().getSchoolId());
                classResultRepository.save(classResult);
                saved = true;
            } else {
                failure.setMessage("You have not entered comments for all the Students in your Class. " +
                        "Please do so prior to submitting results");
            }
        }

        if (submittedBefore || saved) {
            return success;
        } else {
            return failure;
        }
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/students-subject-vetting-helpers", method = RequestMethod.GET)
    public ApiResponse getStudentsSubjectVettingHelpers(@RequestParam String subjectId, @RequestParam String className,
                                                        @RequestParam String session, @RequestParam String term) {

        ApiResponse success = success();

        success.setResults(getSortedVetSubjectDTOs(session, term, className, subjectId, loggedInUser().getSchoolId()));

        return success;
    }

    @Secured({"ROLE_SUBJECT_TEACHER", "ROLE_SUPER_ADMIN"})
    @RequestMapping(value = "/results/vet-subject", method = RequestMethod.GET)
    public ApiResponse vetSubjectResult(@RequestParam String resultId,
                                        @RequestParam String subjectId, @RequestParam String className,
                                        @RequestParam String session, @RequestParam String term) {

        ApiResponse success = success();
        ApiResponse failure = failure();

        StudentSubjectResult studentSubjectResult =  studentSubjectResultRepository.findOne(resultId);

        if (studentSubjectResult != null && studentSubjectResult.getStatus() == Result.ResultStatus.NONE) {
            studentSubjectResult.setStatus(Result.ResultStatus.SUBJECT_TEACHER_VETTED);
            studentSubjectResultRepository.save(studentSubjectResult);

            success.setResults(getSortedVetSubjectDTOs(session, term, className, subjectId, loggedInUser().getSchoolId()));

            return success;
        }

        return failure;
    }

    private List getSortedVetSubjectDTOs(String session, String term, String className, String subjectId, String schoolId) {

        List<VetSubjectDTO> vetSubjectDTOs = new ArrayList<>();

        List<StudentSubjectResult> studentSubjectResults =
                studentSubjectResultRepository.findBySessionAndTermAndClazzAndSubjectAndSchoolId(session, term, className, subjectId, schoolId);

        // Set the full name used on the web front end
        studentSubjectResults.forEach( r ->  {
            Student student = studentRepository.findOne(r.getStudentId());
            VetSubjectDTO vetSubjectDTO = new VetSubjectDTO();
            vetSubjectDTO.setStudentName(student.getSurname() + " " + student.getFirstName() +
                    (!StringUtils.isEmpty(student.getMiddleName()) ? " " + student.getMiddleName() : ""));
            vetSubjectDTO.setId(r.getId());
            vetSubjectDTO.setStatus(r.getStatus());
            vetSubjectDTOs.add(vetSubjectDTO);
        });

        List<VetSubjectDTO> sortedVetSubjectDTOs =
                vetSubjectDTOs.stream()
                        .sorted((a, b) -> a.getStudentName().compareTo(b.getStudentName()))
                        .collect(Collectors.toList());

        return sortedVetSubjectDTOs;
    }
}
