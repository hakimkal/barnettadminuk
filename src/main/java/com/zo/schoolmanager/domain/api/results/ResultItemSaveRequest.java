package com.zo.schoolmanager.domain.api.results;

import com.zo.schoolmanager.domain.api.common.RequestBody;

import java.util.List;

/**
 * Created by ozo on 23/01/2016.
 */
public class ResultItemSaveRequest implements RequestBody, RequestObject {
    private List<ResultItemDTO> resultItems;
    private ResultFilter filter;

    public List<ResultItemDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ResultItemDTO> resultItems) {
        this.resultItems = resultItems;
    }

    public ResultFilter getFilter() {
        return filter;
    }

    public void setFilter(ResultFilter filter) {
        this.filter = filter;
    }
}
