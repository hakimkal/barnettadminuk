var React = require('react');
var Modal = require('react-bootstrap/lib/Modal');

module.exports = React.createClass({

    getInitialState: function() {
        return {
            open: this.props.open == 'true' ? true : false,
            message: this.props.message,
            title: this.props.title
        }
    },

    render: function() {
        return (
            <Modal show={this.state.open} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.state.title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="alert alert-danger fade in nomargin">
                        <h4>Error</h4>
                        <p className="lead">{this.state.message}</p>
                    </div>
                </Modal.Body>
            </Modal>
        )
    },

    close: function() {
        this.setState({open: false});
    }
});