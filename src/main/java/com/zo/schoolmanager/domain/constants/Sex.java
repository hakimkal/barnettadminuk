package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 26/11/15.
 */
public enum Sex {
    Male,
    Female,
    None
}
