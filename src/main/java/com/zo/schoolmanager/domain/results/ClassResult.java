package com.zo.schoolmanager.domain.results;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ozo on 17/12/2015.
 */

@Document(collection = "class_results")
public class ClassResult extends Result {

    @Id
    private String id;
    private ResultStatus status;
    @Transient
    private List<StudentResult> studentResults;

    public ClassResult() {}

    public ClassResult(String clazz, String session, String term, ResultStatus status, String schoolId) {
        this.setClazz(clazz);
        this.setSession(session);
        this.setTerm(term);
        this.status = status;
        this.setSchoolId(schoolId);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public List<StudentResult> getStudentResults() {
        return studentResults;
    }

    public void setStudentResults(List<StudentResult> studentResults) {
        this.studentResults = studentResults;
    }
}
