package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.TestSetup;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.results.ResultItem;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.domain.results.StudentSubjectResult;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.StudentResultRepository;
import com.zo.schoolmanager.repository.StudentSubjectResultRepository;
import com.zo.schoolmanager.service.StudentResultService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ozo on 17/12/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class ResultTests extends TestSetup {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentSubjectResultRepository studentSubjectResultRepository;

    @Autowired
    private StudentResultRepository studentResultRepository;

    @Autowired
    private StudentResultService resultService;


    @Test
    public void testCanGetStudentSubjectResults() {

        String clazz = "SS2A";

        Student student = student();
        student.setClazz(clazz);
        studentRepository.save(student);
        StudentSubjectResult chemistrySubjectResult = createStudentSubjectResult(student.getId(), "Chemistry");
        StudentSubjectResult physicsSubjectResult = createStudentSubjectResult(student.getId(), "Physics");
        StudentSubjectResult frenchSubjectResult = createStudentSubjectResult(student.getId(), "French");

        ResultItem resultItem1 = new ResultItem("Continuous Assessment 1", "Continuous Assessment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem2 = new ResultItem("Continuous Assessment 2", "Continuous Assessment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem3 = new ResultItem("Continuous Assessment 3", "Continuous Assessment 3", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem4 = new ResultItem("Assignment 1", "Assignment 1", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);
        ResultItem resultItem5 = new ResultItem("Assignment 2", "Assignment 2", 10d, ResultItem.ResultStatus.SCORE_SAVED, 10d);

        List firstResults = Arrays.asList(resultItem1, resultItem2, resultItem3, resultItem4, resultItem5);

        chemistrySubjectResult.getResultItems().addAll(firstResults);
        physicsSubjectResult.getResultItems().addAll(firstResults);
        frenchSubjectResult.getResultItems().addAll(firstResults);

        studentSubjectResultRepository.save(Arrays.asList(chemistrySubjectResult, physicsSubjectResult, frenchSubjectResult));

        StudentResult studentResult = new StudentResult(student.getId(), "2015/2016", "First Term", "SS1A", "schoolId");
        studentResultRepository.save(studentResult);

        StudentResult savedStudentResult = resultService.findStudentResults(student.getId(),
                "2015/2016", "First Term", student.getSchoolId());

        Assert.assertEquals(3, savedStudentResult.getStudentSubjectResults().size());

    }

    private StudentSubjectResult createStudentSubjectResult(String studentId, String subject) {
        StudentSubjectResult studentSubjectResult = new StudentSubjectResult(studentId, subject, "2015/2016", "First Term",
                "SS1A", "schoolId");

        return studentSubjectResultRepository.save(studentSubjectResult);
    }
}
