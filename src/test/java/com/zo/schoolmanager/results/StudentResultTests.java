package com.zo.schoolmanager.results;

import com.zo.schoolmanager.BarnettApplication;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.repository.StudentResultRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by ozo on 17/12/2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
public class StudentResultTests {

    @Autowired
    private StudentResultRepository studentResultRepository;

    @Test
    public void testCanCreateStudentResult() {
        StudentResult studentFirstTermResult = new StudentResult();
        Assert.assertNotNull(studentFirstTermResult);
    }

    @Test
    public void testCanSaveStudentResult() {
        StudentResult studentResult = new StudentResult();
        studentResultRepository.save(studentResult);
    }

}
