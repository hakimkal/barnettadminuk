package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 18/12/2015.
 */
public enum UserType {
    TEACHER,
    PARENT,
    ADMIN,
    PRINCIPAL,
    EX_STUDENT,
    SUPER_ADMIN
}
