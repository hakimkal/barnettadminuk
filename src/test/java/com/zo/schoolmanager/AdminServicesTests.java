package com.zo.schoolmanager;

import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.repository.StudentRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ozo on 05/11/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BarnettApplication.class)
@WebAppConfiguration
public class AdminServicesTests extends TestSetup {


    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void testCanSaveParent() {

        Parent parent = mother();
        Student student = student();
        student.setParents(Arrays.asList(parent));

        studentRepository.save(student);

        Parent savedParent = student.getParents().get(0);

        Assert.assertNotNull(savedParent);
        Assert.assertEquals(savedParent.getUsername(), parent.getUsername());
    }


    @Test
    public void testGetListOfParents() {

        Student student = student();
        student.setParents(Arrays.asList(father(), mother()));

        Student retrievedStudent = studentRepository.save(student);

        List<Parent> parentsFromRepo = retrievedStudent.getParents();

        Assert.assertNotNull(parentsFromRepo);
        Assert.assertTrue(parentsFromRepo.size() == 2);
    }

    @Test
    public void testCanSaveStudent() {
        Student student = student();
        String generatedFirstName = student.getFirstName();
        studentRepository.save(student);

        Assert.assertNotNull(student);
        Assert.assertNotNull(student.getId());
        Assert.assertEquals(student.getFirstName(), generatedFirstName);
    }

    @Test
    public void testCanConvertMongoSavedDateToLocalDate() {

        Student student = student();
        studentRepository.save(student);

        Student retrievedStudent = studentRepository.findOne(student.getId());

        Assert.assertEquals(LocalDate.of(1978, 2, 3), retrievedStudent.getDateOfBirth());
    }

    @Test
    public void canGetParentsStudentsByParent() {

        Student student = student();

        Parent father = father();
        Parent mother = mother();

        student.setParents(Arrays.asList(father, mother));

        studentRepository.save(student);

        List<Student> students = studentRepository.findByParentsFirstName(father.getFirstName());

        Assert.assertEquals(students.size(), 1);
    }

    @Test
    public void canGetParentByUsernameFromStudent() {

        Student student = student();
        Parent father = father();
        Parent mother = mother();

        student.setParents(Arrays.asList(father, mother));

        studentRepository.save(student);

        Student retrievedStudent = studentRepository.findOneByParentsUsername(father.getUsername());

        Parent parent = retrievedStudent.getParents().stream()
                .filter(p -> p.getUsername().equals(father.getUsername())).findFirst().orElse(null);

        Assert.assertNotNull(parent);
        Assert.assertEquals(parent.getUsername(), father.getUsername());
        Assert.assertEquals(parent.getFirstName(), father.getFirstName());
        Assert.assertEquals(parent.getLastName(), father.getLastName());
}

    @Test
    public void canUpdateParentOfStudent() {
        Student student = student();
        Parent father1 = father();
        Parent mother1 = mother();

        Parent father2 = father();
        Parent mother2 = mother();

        student.setParents(Arrays.asList(father1, mother1));
        Student studentOnFirstSave = studentRepository.save(student);

        student.setParents(Arrays.asList(father2, mother2));
        Student studentOnSecondSave = studentRepository.save(student);


        Parent father = studentOnSecondSave.getParents().stream()
                .filter(p -> p.getUsername().equals(father2.getUsername())).findFirst().orElse(null);


        Parent mother = studentOnSecondSave.getParents().stream()
                .filter(p -> p.getUsername().equals(mother2.getUsername())).findFirst().orElse(null);

        Assert.assertEquals(studentOnFirstSave.getId(), studentOnSecondSave.getId());
        Assert.assertEquals(father.getUsername(), father2.getUsername());
        Assert.assertEquals(mother.getLastName(), mother2.getLastName());
        Assert.assertNotEquals(father1.getUsername(), father.getUsername());

    }


    @After
    public void dropCollections() {
        removeAllStudents();
    }


    private void removeAllStudents() {
        studentRepository.delete(studentRepository.findAll());
    }

}
