var Api = require('../util/api');
var Reflux = require('reflux');
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getTerms: function() {
        return Api.get('/sessions/terms').then(function(data) {
            this.terms = data;
            this.trigger('updateTerms', data);

        }.bind(this));
    }

});

