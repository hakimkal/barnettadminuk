package com.zo.schoolmanager.repository;

import com.zo.schoolmanager.domain.results.ResultCategory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ozo on 16/12/2015.
 */
public interface ResultCategoryRepository extends MongoRepository<ResultCategory, String> {
    List<ResultCategory> findResultCategoriesBySchoolId(String schoolId);
    ResultCategory findOneByNameAndSchoolId(String name, String schoolId);
}
