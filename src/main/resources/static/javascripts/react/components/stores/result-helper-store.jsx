var Api = require('../util/api')
var Reflux = require('reflux')
var Actions = require('../actions');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getResultViewHeaders: function(type, clazz) {
        return Api.get('/helpers/student-subject-result-headers?resultFor=' + type + '&clazz=' + clazz).then(function(data) {
            this.headers = data.results;
            if (type === 'Class') {
                this.trigger("updateStudentSubjectResultHeaders");
            } else {
                this.trigger("updateStudentResultHeaders");
            }

        }.bind(this));
    }

});

