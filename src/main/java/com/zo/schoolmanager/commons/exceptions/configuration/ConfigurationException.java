package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 06/09/2016.
 */
public abstract class ConfigurationException extends RuntimeException {

    private static final long serialVersionUID = -1L;

    public ConfigurationException(String message) {
        super(message);
    }


}
