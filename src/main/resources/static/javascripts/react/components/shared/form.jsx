var React = require('react');
var ReactDOM = require('react-dom');
var NumberField = require('react-number-field');
var SelectDate = require('./select-date');
var SelectClass = require('../results/dropdowns/select-class');
var Actions = require('../actions');
var Sex = require('./sex');
var Country = require('./country');
var Validate = require("validate.js");
var SelectSession = require('../results/dropdowns/select-session');
var SelectMultipleTerms = require('../results/dropdowns/select-multiple-terms');
var SelectMultipleClasses = require('../results/dropdowns/select-multiple-classes');


module.exports = React.createClass({

    getInitialState: function () {

        var hasFirstColumn = true;
        var hasSecondColumn = true;

        if (this.props.firstColumn == undefined || this.props.firstColumn.length < 1) {
            hasFirstColumn = false;
        }

        if (this.props.secondColumn == undefined || this.props.secondColumn.length < 1) {
            hasSecondColumn = false;
        }

        return {
            hasFirstColumn: hasFirstColumn,
            hasSecondColumn: hasSecondColumn,
            constraints: null,
            firstColumn: this.props.firstColumn,
            secondColumn: this.props.secondColumn
        }
    },

    render: function () {

        var firstColumn;
        var secondColumn;


        if (!this.state.hasFirstColumn && !this.state.hasSecondColumn) {
            return (
                <div></div>
            )
        }

        if (this.state.hasFirstColumn) {
            firstColumn =
                this.state.firstColumn.map(function (property) {
                    return (<div className={'form-group ' + (property.error != undefined ? 'has-error' : '')}
                                 key={property.title}>
                        <label className="col-sm-3 control-label">{property.title} <span
                            className="text-danger">{ property.required ? '*' : null}</span></label>
                        <div className="col-sm-8">
                            { this.getComponent(property) }
                        </div>
                    </div>)
                }.bind(this));
        }

        if (this.state.hasSecondColumn) {
            secondColumn =
                this.state.secondColumn.map(function (property) {
                    return (<div className={'form-group ' + (property.error != undefined ? 'has-error' : '')}
                                 key={property.title}>
                        <label className="col-sm-3 control-label">{property.title}<span
                            className="text-danger"> { property.required ? '*' : null}</span></label>
                        <div className="col-sm-8">
                            { this.getComponent(property) }
                        </div>
                    </div>)
                }.bind(this));
        }

        return (
            <div>
                <div className="panel">
                    <h3>{this.props.title}</h3>

                    <hr />

                    <div classNameName="panel">
                        <div className="panel-body">
                            <form id="basicForm" action="form-validation.html" className="form-horizontal">
                                {
                                    this.state.hasFirstColumn && this.state.hasSecondColumn ?
                                        <div className="col-sm-6">
                                            {firstColumn}
                                        </div>

                                        :

                                        <div className="col-sm-8">
                                            {firstColumn}
                                        </div>
                                }

                                {
                                    this.state.hasSecondColumn ?
                                        <div className="col-sm-6">
                                            {secondColumn}
                                        </div>
                                        :
                                        null
                                }
                            </form>
                        </div>
                    </div>
                </div>

                <hr className="col-sm-9 col-sm-offset-1"/>
                <div className="row">
                    <div className="col-sm-9 col-sm-offset-3">
                        <button className="btn btn-success btn-quirk btn-wide mr5" onClick={this.onSubmit}>Save</button>
                        <button className="btn btn-quirk btn-wide btn-default">Cancel</button>
                    </div>
                </div>

            </div>
        )
    },

    componentWillMount: function () {
        Actions.getAllClasses();
        Actions.getTerms();
        Actions.getSessions();

    },

    onSubmit: function () {

        var data = this.getValuesFromFormElements();

        var errorOutput = Validate(data, this.state.constraints);

        this.updateStyling(errorOutput);

        if (errorOutput == undefined) {
            this.props.onSubmit(data);
        }
    },

    updateStyling: function (errorOutput) {


        if (this.state.hasFirstColumn) {
            this.applyErrorStyling(this.state.firstColumn, errorOutput, 'firstColumn');
        }

        if (this.state.hasSecondColumn) {
            this.applyErrorStyling(this.state.secondColumn, errorOutput, 'secondColumn');
        }

    },

    applyErrorStyling: function (columnData, errorOutput, columnName) {
        if (errorOutput !== undefined) {
            var currentColumn = columnData;
            var updatedColumn = columnData;

            for (var i = 0; i < currentColumn.length; i++) {
                var key = this.toCamel(currentColumn[i].title);

                var propertyToApplyError = currentColumn[i];

                if (errorOutput[key] !== undefined) {
                    propertyToApplyError['error'] = errorOutput[key];
                } else {
                    delete propertyToApplyError['error'];
                }

                updatedColumn[i] = propertyToApplyError;
            }

            var newStateObject = {};
            newStateObject[columnName] = updatedColumn;
            this.setState(newStateObject);

        } else {
            // Need to remove all errors

            var currentColumn = columnData;
            var updatedColumn = columnData;

            for (var i = 0; i < currentColumn.length; i++) {

                var propertyToApplyError = currentColumn[i];

                delete propertyToApplyError['error'];
                updatedColumn[i] = propertyToApplyError;
            }

            var newStateObject = {};
            newStateObject[columnName] = updatedColumn;
            this.setState(newStateObject);
        }
    },

    getComponent: function (property) {

        if (property.validation !== undefined) {
            this.addValidation(this.toCamel(property.title), property.validation);
        }

        var errorElement = null;
        if (property.error !== undefined) {
            errorElement =
                <label className="error">{this.getErrorMessages(this.toCamel(property.title),
                    property.error)}
                </label>
        }

        switch (property.type) {
            case 'string':
                return <div>
                    <input type="text" className="form-control" ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'number':
                return <div>
                    <NumberField defaultValue={''} className="form-control" ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'date':
                return <div>
                    <SelectDate ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'select-class':
                return <div>
                    <SelectClass ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'text-area':
                return <div>
                    <textarea rows="5" className="form-control" ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'sex':
                return <div>
                    <Sex ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>

            case 'country':
                return <div>
                    <Country ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>
            case 'multiple-terms':
                return <div>
                    <SelectMultipleTerms ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>
            case 'multiple-classes':
                return <div>
                    <SelectMultipleClasses />
                    { errorElement }
                </div>
            case 'session':
                return <div>
                    <SelectSession ref={this.toCamel(property.title)}/>
                    { errorElement }
                </div>
            case 'amount':
                return <div>
                    <div className="input-group">
                        <span className="input-group-addon">N</span>
                        <input type="text" className="form-control" ref={this.toCamel(property.title)}/>
                        <span className="input-group-addon">.00</span>
                    </div>
                </div>
        }

    },

    getValuesFromFormElements: function () {

        var data = {};

        // Get the values entered for the First Column Properties
        if (this.state.hasFirstColumn) {
            this.state.firstColumn.map(function (p) {

                if (p.type == 'date') {
                    // Date - It is wrapped in external DIV so need to get the first child which is an input
                    data[this.toCamel(p.title)] = ReactDOM.findDOMNode(this.refs[this.toCamel(p.title)]).children[0].value;
                } else if (p.type == 'multiple-sessions') {

                    data[this.toCamel(p.title)] = document.getElementsByName('select-multiple-sessions')[0].value

                } else if (p.type == 'multiple-terms') {
                    data[this.toCamel(p.title)] = document.getElementsByName('select-multiple-terms')[0].value

                }  else if (p.type == 'multiple-classes') {
                    data[this.toCamel(p.title)] = document.getElementsByName('select-multiple-classes')[0].value

                } else {
                    // Input, TextArea, Select, Number?
                    data[this.toCamel(p.title)] = ReactDOM.findDOMNode(this.refs[this.toCamel(p.title)]).value;
                }

            }.bind(this));
        }

        // Get the values entered for the Second Column Properties
        if (this.state.hasSecondColumn) {
            this.state.secondColumn.map(function (p) {

                if (p.type == 'date') {
                    // Date - It is wrapped in external DIV so need to get the first child which is an input
                    data[this.toCamel(p.title)] = ReactDOM.findDOMNode(this.refs[this.toCamel(p.title)]).children[0].value;
                } else {
                    // Input, TextArea, Select, Number?
                    data[this.toCamel(p.title)] = ReactDOM.findDOMNode(this.refs[this.toCamel(p.title)]).value;
                }

            }.bind(this));
        }

        return data;
    },

    toCamel: function (string) {
        return string.replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
            return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
        }).replace(/\s+/g, '');
    },

    addValidation: function (title, validation) {

        if (validation.required !== undefined && validation.required) {
            var val = {
                presence: true
            };

            // first time constraints is null
            if (this.state.constraints == null) {
                this.state.constraints = {};
            }

            this.state.constraints[title] = val;

        }
    },

    getErrorMessages: function (title, errors) {
        return errors.join(", ");
    }

})