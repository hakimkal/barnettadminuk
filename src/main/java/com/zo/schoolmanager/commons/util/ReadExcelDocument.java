package com.zo.schoolmanager.commons.util;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.users.Parent;
import com.zo.schoolmanager.repository.StudentRepository;
import com.zo.schoolmanager.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ReadExcelDocument {

    private static String phoneNo = "";
    private static String surname = "";
    private static String firstName= "";
    private static String middleName= "";

    private static Map<Student, Parent> studentAndParentObjects = new HashMap<>();

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();

    public Map<Student, Parent> readExcelDoc(String file) {

        try (FileInputStream fIP = new FileInputStream(new File(file))) {

            XSSFWorkbook workbook = new XSSFWorkbook(fIP);
            workbook.forEach(XSSFSheet -> {
                Iterator<Row> rowIterator = XSSFSheet.rowIterator();
                Iterable<Row> iterable = ()-> rowIterator;
                Stream<Row> stream = StreamSupport.stream(iterable.spliterator(),false);
                stream.filter(row -> row.getRowNum() != 0)
                        .forEach(row -> {
                                row.forEach(cell-> {

                            if (cell.getColumnIndex()==1){
                                 phoneNo = cell.getStringCellValue();
                            }
                            else if (cell.getColumnIndex()==2){
                                surname = cell.getStringCellValue();
                            }
                            else if (cell.getColumnIndex()==3){
                                firstName = cell.getStringCellValue();
                            }
                            else if (cell.getColumnIndex()==4){
                                middleName = cell.getStringCellValue();
                            }
                                });
                            saveStudentsAndParents(surname, firstName, middleName, phoneNo, XSSFSheet.getSheetName());
                        }
                        );
            });
        }
        catch(Exception ex)

        {
            System.out.println("Error: ");
            ex.printStackTrace();
        }
        return getStudentAndParentCollection();
    }




    public void saveStudentsAndParents(String surname, String firstName, String middleName,
                                                      String phoneNo, String clazz) {

        Student student = new Student();
        Parent parent = new Parent();

        try{
            if ( (surname != null && !surname.isEmpty()) && (firstName != null && !firstName.isEmpty()) &&
                    (middleName != null && !middleName.isEmpty()) && (phoneNo != null && !phoneNo.isEmpty())) {

                student.setFirstName(firstName);
                student.setMiddleName(middleName);
                student.setSurname(surname);
                student.setClazz(clazz);

                parent.setPhone2(phoneNo);

                parent.setLastName(surname);



            }
            else if ((surname != null && !surname.isEmpty()) && (firstName != null && !firstName.isEmpty()) &&
                    (middleName != null && !middleName.isEmpty()) ){

                student.setFirstName(firstName);
                student.setMiddleName(middleName);
                student.setSurname(surname);
                student.setClazz(clazz);
                parent.setLastName(surname);

            }
            else if (surname != "" && firstName != "" && phoneNo != ""){
                student.setFirstName(firstName);
                student.setSurname(surname);
                student.setClazz(clazz);

                parent.setPhone2(phoneNo);
                parent.setLastName(surname);

            }
            else if((surname != null && !surname.isEmpty()) && (firstName != null && !firstName.isEmpty())){
                student.setFirstName(firstName);
                student.setSurname(surname);
                student.setClazz(clazz);
                parent.setLastName(surname);

            }


            studentRepository.save(student);

            parent.setPassword(bCryptEncoder.encode(RandomStringUtils.random(4, true, false)));
            userRepository.save(parent);


            studentAndParentObjects.put(student, parent);
        }catch (Exception ex){
            ex.printStackTrace();
        }


    }




    private static Map<Student, Parent> getStudentAndParentCollection() {
        return studentAndParentObjects;
    }
}
