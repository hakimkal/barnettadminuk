package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Student;
import com.zo.schoolmanager.domain.results.ClassResult;
import com.zo.schoolmanager.domain.results.StudentResult;
import com.zo.schoolmanager.repository.ClassResultRepository;
import com.zo.schoolmanager.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by ozo on 24/01/2016.
 */
public class ClassResultService {

    @Autowired
    private ClassResultRepository classResultRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentResultService studentResultService;

    private final Logger LOG = LoggerFactory.getLogger(ClassResultService.class);


}
