package com.zo.schoolmanager.domain.constants;

/**
 * Created by ozo on 27/11/15.
 */
public enum SchoolType {
    NURSERY,
    PRIMARY,
    SECONDARY,
    NURSERY_AND_PRIMARY,
    NURSERY_AND_SECONDARY,
    PRIMARY_AND_SECONDARY,
    NURSERY_PRIMARY_AND_SECONDARY
}
