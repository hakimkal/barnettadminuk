package com.zo.schoolmanager.commons.exceptions.configuration;

/**
 * Created by ozo on 06/09/2016.
 */
public class MultipleResultCategoriesException extends ConfigurationException {
    public MultipleResultCategoriesException(String message) {
        super(message);
    }
}
