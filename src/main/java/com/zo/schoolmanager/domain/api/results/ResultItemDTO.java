package com.zo.schoolmanager.domain.api.results;

import com.zo.schoolmanager.domain.api.ResponseObject;

/**
 * Created by ozo on 23/01/2016.
 */
public class ResultItemDTO implements ResponseObject {

    private String studentId;
    private String score;
    private String resultCategory;
    private String studentName;
    private String status;
    private boolean saved = false;
    private String key;
    private boolean vetted = false;
    private boolean processed = false;
    private Integer order;
    private Double maxScore;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getResultCategory() {
        return resultCategory;
    }

    public void setResultCategory(String resultCategory) {
        this.resultCategory = resultCategory;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isVetted() {
        return vetted;
    }

    public void setVetted(boolean vetted) {
        this.vetted = vetted;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }
}
