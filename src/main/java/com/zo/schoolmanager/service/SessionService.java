package com.zo.schoolmanager.service;

import com.zo.schoolmanager.domain.Session;
import com.zo.schoolmanager.domain.Term;
import com.zo.schoolmanager.commons.exceptions.configuration.MultipleTermsException;
import com.zo.schoolmanager.commons.exceptions.configuration.SessionDatesIncompleteException;
import com.zo.schoolmanager.commons.exceptions.configuration.MultipleSessionsException;
import com.zo.schoolmanager.commons.exceptions.configuration.NoSchoolSetException;
import com.zo.schoolmanager.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ozo on 11/04/2016.
 */

@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    /**
     * This method saves a school. In the process it sets the start and end dates of the session based on the dates
     * entered for the terms. If the terms are incomplete, it picks the start and end dates based on the terms available.
     * So if only one term, it will use the start and end dates of the terms set
     *
     * @param session
     * @return
     */
    public Session save(Session session) {

        if (session.getSchoolId() == null) {
            throw new NoSchoolSetException("Session must have a School ID");
        }

        List<Term> terms = session.getTerms();
        if (terms != null) {

            // Get the start of earliest date in the terms
            LocalDateTime start = terms
                    .stream()
                    .reduce((t1, t2) -> t1.getStarts().isBefore(t2.getStarts()) ? t1 : t2).get().getStarts();

            // Get the start of latest date in the terms
            LocalDateTime end = terms
                    .stream()
                    .reduce((t1, t2) -> t1.getEnds().isAfter(t2.getEnds()) ? t1 : t2).get().getEnds();

            // Set the start and end dates based on the dates gotten above
            if (start != null && end != null) {
                session.setStarts(start);
                session.setEnds(end);
            } else {
                throw new SessionDatesIncompleteException("Dates set for Terms are incomplete");
            }

        }

        return sessionRepository.save(session);
    }

    /**
     * This method gets the current session based on the current time
     *
     * @param schoolId
     * @return
     */
    public Session currentSession(String schoolId) {
        LocalDateTime dateTime = LocalDateTime.now();

        // There is only one session in this list. An exception is thrown in getSessions if it is more than one
        List<Session> sessions = getSessions(schoolId, dateTime);
        sessions.forEach(s -> s.setCurrentSession(true));
        return sessions.get(0);
    }

    /**
     * This method is used to get the session for the specified date
     * This method is useful as it can be used as an override for the default get current session (So we can set the
     * context for a specific user to use a different session - for seeing results etc - i.e. an admin)
     *
     * @param schoolId
     * @param dateTime
     * @return
     */
    public Session getSession(String schoolId, LocalDateTime dateTime) {
        List<Session> sessions = getSessions(schoolId, dateTime);
        return sessions.get(0);
    }

    /**
     * This method is used for getting all the sessions in the school. This method is used most for admins
     * so they have more control over the system in areas driven by Session/Term
     *
     * @param schoolId
     * @return
     */
    public List<Session> getSessions(String schoolId) {
        List<Session> sessions = sessionRepository.getSessionBySchoolId(schoolId);

        Session currentSession = currentSession(schoolId);

        sessions.forEach(s -> {
            if (s.getName().equals(currentSession.getName())) {
                s.setCurrentSession(true);

                s.getTerms().forEach(t ->
                    currentSession.getTerms().forEach(ct -> {
                        if (ct.getName().equals(t.getName())) {
                            t.setCurrent(true);
                        }
                    }));
            }
        });


        return sessions;
    }

    public Term currentTerm(String schoolId) {
        Term currentTerm = null;

        Session currentSession = currentSession(schoolId);
        if (currentSession != null) {
            List<Term> terms = currentSession.getTerms().stream()
                    .filter(t -> t.isCurrent())
                    .collect(Collectors.toList());

            if (terms.size() == 1) {
                currentTerm = terms.get(0);
            }
        }

        return currentTerm;

    }

    /**
     * This method is very helpful. It's used to get a session
     *
     * @param schoolId
     * @param dateTime
     * @return
     */
    private List<Session> getSessions(String schoolId, LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");

        List<Session> sessions =
                sessionRepository.getSessionByStartsBeforeAndEndsAfterAndSchoolId(dateTime, dateTime, schoolId);

        if (sessions.size() > 1) {
            String sessionNames = "";
            for (Session s : sessions) {
                sessionNames = sessionNames + s.getName() + " ";
            }
            throw new MultipleSessionsException("Multiple Sessions(" + sessionNames.trim()
                    + ") for date: " + formatter.format(dateTime));
        }

        // Set the current term in the session list
        sessions.forEach(s -> {
            List<Term> terms =

            s.getTerms().stream()
                    .filter(t -> t.getStarts().isBefore(dateTime) && t.getEnds().isAfter(dateTime))
                    .collect(Collectors.toList());

            // Throw Exception if there are multiple current terms
            if (terms.size() > 1) {
                String termNames = "";
                for (Term t : terms) {
                    termNames = termNames + t.getName() + " ";
                }
                throw new MultipleTermsException("Multiple Terms(" + termNames.trim()
                        + ") for date: " + formatter.format(dateTime));
            }

            s.getTerms().forEach(t -> {
                if (t.getStarts().isBefore(dateTime) && t.getEnds().isAfter(dateTime)) {
                    t.setCurrent(true);
                }
            });


        });


        return sessions;

    }
}
