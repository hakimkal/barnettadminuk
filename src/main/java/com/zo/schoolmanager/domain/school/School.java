package com.zo.schoolmanager.domain.school;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozo on 26/11/15.
 */
@Document(collection = "schools")
public class School {

    @Id
    private String id;
    private String name = "";
    private String address = "";
    private String principalFirstName = "";
    private String principalLastName = "";
    private List<String> sections = new ArrayList<>();
    private List<String> classes = new ArrayList<>();

    private Setting setting = new Setting();

    public School() {}

    public School(String name, String address, String principalFirstName, String principalLastName) {
        this.name = name;
        this.address = address;
        this.principalFirstName = principalFirstName;
        this.principalLastName = principalLastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrincipalFirstName() {
        return principalFirstName;
    }

    public void setPrincipalFirstName(String principalFirstName) {
        this.principalFirstName = principalFirstName;
    }

    public String getPrincipalLastName() {
        return principalLastName;
    }

    public void setPrincipalLastName(String principalLastName) {
        this.principalLastName = principalLastName;
    }

    public List<String> getSections() {
        return sections;
    }

    public void setSections(List<String> sections) {
        this.sections = sections;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }
}
